
drop table ReportGender
drop table report 
drop table Region
drop table Country

CREATE TABLE Country(
	[Id] int IDENTITY(1, 1) PRIMARY KEY,
	[Name] nvarchar(500) null,
	[Population] int null
);

CREATE TABLE Region(
	[Id] int IDENTITY(1, 1) PRIMARY KEY,
	[CountryId] int null FOREIGN KEY REFERENCES Country (Id),
	[Name] nvarchar(500) null,
	[Population] int null
);

CREATE TABLE Report (	
	[Id] int not null,
    [Date] datetime not null,
	[CountryId] int not null FOREIGN KEY REFERENCES Country (Id),
	[RegionId] int null FOREIGN KEY REFERENCES Region (Id),
	[Cases] int not null,
	[Deaths] int not null,
	[Recovered] int not null,
	PRIMARY KEY NONCLUSTERED ([Id],[CountryId])
);

CREATE CLUSTERED INDEX IX_Date_Report   
    ON dbo.Report ([Date]);   

CREATE TABLE ReportGender(
	[ReportId] int not null,
	[CountryId] int not null,
	[Male] int not null,
	[Female] int not null,
	[Undefined] int not null,
	CONSTRAINT FK_REPORT FOREIGN KEY ([ReportId],[CountryId]) REFERENCES Report ([Id],[CountryId])
);

GO  

