
INSERT Country(Name,Population)
VALUES
('Afghanistan',38928346),
('Albania',2877797),
('Algeria',43851044),
('American Samoa',55191),
('Andorra',77265),
('Angola',32866272),
('Anguilla',15003),
('Antigua and Barbuda',97929),
('Argentina',45195774),
('Armenia',2963243),
('Aruba',106766),
('Australia',25499884),
('Austria',9006398),
('Azerbaijan',10139177),
('Bahamas',393244),
('Bahrain',1701575),
('Bangladesh',164689383),
('Barbados',287375),
('Belarus',9449323),
('Belgium',11589623),
('Belize',397628),
('Benin',12123200),
('Bermuda',62278),
('Bhutan',771608),
('Bolivia',11673021),
('Bosnia and Herzegovina',3280819),
('Botswana',2351627),
('Brazil',212559417),
('British Virgin Islands',30231),
('Brunei',437479),
('Bulgaria',6948445),
('Burkina Faso',20903273),
('Burundi',11890784),
('Cabo Verde',555987),
('Cambodia',16718965),
('Cameroon',26545863),
('Canada',37742154),
('Caribbean Netherlands',26223),
('CAR',null),
('Cayman Islands',65722),
('Central African Republic',4829767),
('Chad',16425864),
('Channel Islands',173863),
('Chile',19116201),
('China',1439323776),
('Colombia',50882891),
('Comoros',869601),
('Congo',5518087),
('Cook Islands',17564),
('Costa Rica',5094118),
('Croatia',4105267),
('Cuba',11326616),
('Cura�ao',164093),
('Cyprus',1207359),
('Czechia',10708981),
('C�te dIvoire',26378274),
('Denmark',5792202),
('Diamond Princess',null),
('Djibouti',988000),
('Dominica',71986),
('empty',null),
('Dominican Republic',10847910),
('DRC (DR Congo)',89561403),
('Ecuador',17643054),
('Egypt',102334404),
('El Salvador',6486205),
('Equatorial Guinea',1402985),
('Eritrea',3546421),
('Estonia',1326535),
('Eswatini',1160164),
('Ethiopia',114963588),
('Faeroe Islands',48863),
('Falkland Islands',3480),
('Fiji',896445),
('Finland',5540720),
('France',65273511),
('French Guiana',298682),
('French Polynesia',280908),
('Gabon',2225734),
('Gambia',2416668),
('Georgia',3989167),
('Germany',83783942),
('Ghana',31072940),
('Gibraltar',33691),
('Greece',10423054),
('Greenland',56770),
('Grenada',112523),
('Guadeloupe',400124),
('Guam',168775),
('Guatemala',17915568),
('Guinea',13132795),
('Guinea-Bissau',1968001),
('Guyana',786552),
('Haiti',11402528),
('Holy See',801),
('Honduras',9904607),
('Hong Kong',7496981),
('Hungary',9660351),
('Iceland',341243),
('India',1380004385),
('Indonesia',273523615),
('Iran',83992949),
('Iraq',40222493),
('Ireland',4937786),
('Isle of Man',85033),
('Israel',8655535),
('Italy',60461826),
('Ivory Coast',null),
('Jamaica',2961167),
('Japan',126476461),
('Jordan',10203134),
('Kazakhstan',18776707),
('Kenya',53771296),
('Kiribati',119449),
('Kuwait',4270571),
('Kyrgyzstan',6524195),
('Laos',7275560),
('Latvia',1886198),
('Lebanon',6825445),
('Lesotho',2142249),
('Liberia',6871292),
('Liechtenstein',38128),
('Lithuania',2722289),
('Luxembourg',625978),
('Macao',649335),
('Madagascar',27691018),
('Malawi',19129952),
('Malaysia',32365999),
('Maldives',540544),
('Mali',20250833),
('Malta',441543),
('Marshall Islands',59190),
('Martinique',375265),
('Mauritania',4649658),
('Mauritius',1271768),
('Mayotte',272815),
('Mexico',128932753),
('Micronesia',115023),
('Moldova',4033963),
('Monaco',39242),
('Mongolia',3278290),
('Montenegro',628066),
('Montserrat',4992),
('Morocco',36910560),
('Mozambique',31255435),
('Myanmar',54409800),
('Namibia',2540905),
('Nauru',10824),
('Nepal',29136808),
('Netherlands',17134872),
('New Caledonia',285498),
('New Zealand',4822233),
('Nicaragua',6624554),
('Niger',24206644),
('Nigeria',206139589),
('Niue',1626),
('North Korea',25778816),
('North Macedonia',2083374),
('Northern Mariana Islands',57559),
('Norway',5421241),
('Oman',5106626),
('Pakistan',220892340),
('Palau',18094),
('Palestine',5101414),
('Panama',4314767),
('Papua New Guinea',8947024),
('Paraguay',7132538),
('Peru',32971854),
('Philippines',109581078),
('Poland',37846611),
('Portugal',10196709),
('Puerto Rico',2860853),
('Qatar',2881053),
('Romania',19237691),
('Russia',145934462),
('Rwanda',12952218),
('R�union',895312),
('Saint Barthelemy',9877),
('Saint Helena',6077),
('Saint Kitts & Nevis',53199),
('S. Korea',51269185),
('Saint Lucia',183627),
('Saint Martin',38666),
('Saint Pierre & Miquelon',5794),
('Samoa',198414),
('San Marino',33931),
('Sao Tome & Principe',219159),
('Saudi Arabia',34813871),
('Senegal',16743927),
('Serbia',8737371),
('Seychelles',98347),
('Sierra Leone',7976983),
('Singapore',5850342),
('Sint Maarten',42876),
('Slovakia',5459642),
('Slovenia',2078938),
('Solomon Islands',686884),
('Somalia',15893222),
('South Africa',59308690),
('Spain',46754778),
('Sri Lanka',21413249),
('St. Barth',9877),
('St. Vincent Grenadines',110940),
('Sudan',43849260),
('Suriname',586632),
('Sweden',10099265),
('Switzerland',8654622),
('Taiwan',23816775),
('Tajikistan',9537645),
('Tanzania',59734218),
('Thailand',69799978),
('Timor-Leste',1318445),
('Togo',8278724),
('Tokelau',1357),
('Tonga',105695),
('Trinidad and Tobago',1399488),
('Tunisia',11818619),
('Turkey',84339067),
('Turkmenistan',6031200),
('Turks and Caicos',38717),
('Tuvalu',11792),
('U.S. Virgin Islands',104425),
('UAE (United Arab Emirates)',9890402),
('Uganda',45741007),
('UK (United Kingdom)',67886011),
('Ukraine',43733762),
('Uruguay',3473730),
('USA',331002651),
('Uzbekistan',33469203),
('Vatican City',801),
('Vanuatu',307145),
('Venezuela',28435940),
('Vietnam',97338579),
('Wallis & Futuna',11239),
('Western Sahara',597339),
('Yemen',29825964),
('Zambia',18383955),
('Zimbabwe',16862924)

DELETE FROM report

INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200121',45 ,null,278 ,3  ,0  ,1),
('20200121',110,null,1   ,0  ,0  ,1),
('20200121',181,null,1   ,0  ,0  ,1),
('20200121',211,null,2   ,0  ,0  ,1),

('20200122',45 ,null,32  ,0  ,0  ,2),
('20200122',110,null,0   ,0  ,0  ,2),
('20200122',181,null,0   ,0  ,0  ,2),
('20200122',211,null,0   ,0  ,0  ,2),

('20200123',45 ,null,261 ,12 ,0  ,3),
('20200123',110,null,0   ,0  ,0  ,3),
('20200123',181,null,0   ,0  ,0  ,3),
('20200123',211,null,2   ,0  ,0  ,3),
('20200123',228,null,1   ,0  ,0  ,3),

('20200124',45 ,null,259 ,10 ,0  ,4),
('20200124',110,null,0   ,0  ,0  ,4),
('20200124',181,null,1   ,0  ,0  ,4),
('20200124',211,null,0   ,0  ,0  ,4),
('20200124',228,null,0   ,0  ,0  ,4),
('20200124',233,null,2   ,0  ,0  ,4),
('20200124',193,null,1   ,0  ,0  ,4)

select Country.Name, SUM(report.cases), SUM(report.deaths) from
Report inner join Country on Country.Id = Report.CountryId 
group by Country.Name 


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200125',45 ,null,467 ,16 ,0  ,5),
('20200125',110,null,2   ,0  ,0  ,5),
('20200125',181,null,0   ,0  ,0  ,5),
('20200125',211,null,0   ,0  ,0  ,5),
('20200125',228,null,1   ,0  ,0  ,5),
('20200125',233,null,0   ,0  ,0  ,5),
('20200125',193,null,2   ,0  ,0  ,5),
('20200125',12 ,null,3   ,0  ,0  ,5),
('20200125',149,null,1   ,0  ,0  ,5),
('20200125',76 ,null,3   ,0  ,0  ,5)



INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200126',45 ,null,688 ,15 ,0  ,6),
('20200126',110,null,0   ,0  ,0  ,6),
('20200126',181,null,0   ,0  ,0  ,6),
('20200126',211,null,1   ,0  ,0  ,6),
('20200126',228,null,0   ,0  ,0  ,6),
('20200126',233,null,0   ,0  ,0  ,6),
('20200126',193,null,1   ,0  ,0  ,6),
('20200126',12 ,null,1   ,0  ,0  ,6),
('20200126',149,null,0   ,0  ,0  ,6),
('20200126',76 ,null,0   ,0  ,0  ,6),
('20200126',128 ,null,3   ,0  ,0  ,6)


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200127',45 ,null,776 ,24 ,0  ,7),
('20200127',110,null,1   ,0  ,0  ,7),
('20200127',181,null,2   ,0  ,0  ,7),
('20200127',211,null,0   ,0  ,0  ,7),
('20200127',228,null,3   ,0  ,0  ,7),
('20200127',233,null,0   ,0  ,0  ,7),
('20200127',193,null,0   ,0  ,0  ,7),
('20200127',12 ,null,0   ,0  ,0  ,7),
('20200127',149,null,0   ,0  ,0  ,7),
('20200127',76 ,null,0   ,0  ,0  ,7),
('20200127',128 ,null,1   ,0  ,0  ,7),
('20200127',37 ,null,1   ,0  ,0  ,7)


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200128',45 ,null,1776,26 ,0  ,8),
('20200128',110,null,2   ,0  ,0  ,8),
('20200128',181,null,0   ,0  ,0  ,8),
('20200128',211,null,9   ,0  ,0  ,8),
('20200128',228,null,0   ,0  ,0  ,8),
('20200128',233,null,0   ,0  ,0  ,8),
('20200128',193,null,3   ,0  ,0  ,8),
('20200128',12 ,null,1   ,0  ,0  ,8),
('20200128',149,null,0   ,0  ,0  ,8),
('20200128',76 ,null,0   ,0  ,0  ,8),
('20200128',128,null,0   ,0  ,0  ,8),
('20200128',37 ,null,1   ,0  ,0  ,8),
('20200128',35 ,null,1   ,0  ,0  ,8),
('20200128',82 ,null,1   ,0  ,0  ,8),
('20200128',201 ,null,1   ,0  ,0  ,8)

INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200129',45 ,null,1460,26 ,0  ,9),
('20200129',110,null,1   ,0  ,0  ,9),
('20200129',181,null,0   ,0  ,0  ,9),
('20200129',211,null,0   ,0  ,0  ,9),
('20200129',228,null,0   ,0  ,0  ,9),
('20200129',233,null,0   ,0  ,0  ,9),
('20200129',193,null,0   ,0  ,0  ,9),
('20200129',12 ,null,2   ,0  ,0  ,9),
('20200129',149,null,0   ,0  ,0  ,9),
('20200129',76 ,null,1   ,0  ,0  ,9),
('20200129',128,null,0   ,0  ,0  ,9),
('20200129',37 ,null,1   ,0  ,0  ,9),
('20200129',35 ,null,0   ,0  ,0  ,9),
('20200129',82 ,null,3   ,0  ,0  ,9),
('20200129',201,null,0   ,0  ,0  ,9),
('20200129',223,null,4   ,0  ,0  ,9)




INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200130',45 ,null,1739,38 ,0  ,10),
('20200130',110,null,4   ,0  ,0  ,10),
('20200130',181,null,0   ,0  ,0  ,10),
('20200130',211,null,0   ,0  ,0  ,10),
('20200130',228,null,0   ,0  ,0  ,10),
('20200130',233,null,0   ,0  ,0  ,10),
('20200130',193,null,3   ,0  ,0  ,10),
('20200130',12 ,null,0   ,0  ,0  ,10),
('20200130',149,null,0   ,0  ,0  ,10),
('20200130',76 ,null,1   ,0  ,0  ,10),
('20200130',128,null,3   ,0  ,0  ,10),
('20200130',37 ,null,0   ,0  ,0  ,10),
('20200130',35 ,null,0   ,0  ,0  ,10),
('20200130',82 ,null,0   ,0  ,0  ,10),
('20200130',201,null,0   ,0  ,0  ,10),
('20200130',223,null,0   ,0  ,0  ,10),
('20200130',169,null,1   ,0  ,0  ,10),
('20200130',100,null,1   ,0  ,0  ,10),
('20200130',75 ,null,1   ,0  ,0  ,10)


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200131',45 ,null,1984,43 ,0  ,11),
('20200131',110,null,3   ,0  ,0  ,11),
('20200131',181,null,7   ,0  ,0  ,11),
('20200131',211,null,0   ,0  ,0  ,11),
('20200131',228,null,1   ,0  ,0  ,11),
('20200131',233,null,3   ,0  ,0  ,11),
('20200131',193,null,3   ,0  ,0  ,11),
('20200131',12 ,null,2   ,0  ,0  ,11),
('20200131',149,null,0   ,0  ,0  ,11),
('20200131',76 ,null,1   ,0  ,0  ,11),
('20200131',128,null,1   ,0  ,0  ,11),
('20200131',37 ,null,0   ,0  ,0  ,11),
('20200131',35 ,null,0   ,0  ,0  ,11),
('20200131',82 ,null,1   ,0  ,0  ,11),
('20200131',201,null,0   ,0  ,0  ,11),
('20200131',223,null,0   ,0  ,0  ,11),
('20200131',169,null,0   ,0  ,0  ,11),
('20200131',100,null,0   ,0  ,0  ,11),
('20200131',75 ,null,0   ,0  ,0  ,11),
('20200131',107,null,2   ,0  ,0  ,11)


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200201',45 ,null,2101,46 ,0  ,12),
('20200201',110,null,3   ,0  ,0  ,12),
('20200201',181,null,1   ,0  ,0  ,12),
('20200201',211,null,5   ,0  ,0  ,12),
('20200201',228,null,1   ,0  ,0  ,12),
('20200201',233,null,1   ,0  ,0  ,12),
('20200201',193,null,3   ,0  ,0  ,12),
('20200201',12 ,null,3   ,0  ,0  ,12),
('20200201',149,null,0   ,0  ,0  ,12),
('20200201',76 ,null,0   ,0  ,0  ,12),
('20200201',128,null,0   ,0  ,0  ,12),
('20200201',37 ,null,1   ,0  ,0  ,12),
('20200201',35 ,null,0   ,0  ,0  ,12),
('20200201',82 ,null,2   ,0  ,0  ,12),
('20200201',201,null,0   ,0  ,0  ,12),
('20200201',223,null,0   ,0  ,0  ,12),
('20200201',169,null,0   ,0  ,0  ,12),
('20200201',100,null,0   ,0  ,0  ,12),
('20200201',75 ,null,0   ,0  ,0  ,12),
('20200201',107,null,0   ,0  ,0  ,12),
('20200201',175,null,2   ,0  ,0  ,12),
('20200201',200,null,1   ,0  ,0  ,12),
('20200201',206,null,1   ,0  ,0  ,12),
('20200201',225,null,2   ,0  ,0  ,12)



INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200202',45 ,null,2590,45 ,0  ,13),
('20200202',110,null,3   ,0  ,0  ,13),
('20200202',181,null,3   ,0  ,0  ,13),
('20200202',211,null,0   ,0  ,0  ,13),
('20200202',228,null,1   ,0  ,0  ,13),
('20200202',233,null,1   ,0  ,0  ,13),
('20200202',193,null,2   ,0  ,0  ,13),
('20200202',12 ,null,0   ,0  ,0  ,13),
('20200202',149,null,0   ,0  ,0  ,13),
('20200202',76 ,null,0   ,0  ,0  ,13),
('20200202',128,null,0   ,0  ,0  ,13),
('20200202',37 ,null,0   ,0  ,0  ,13),
('20200202',35 ,null,0   ,0  ,0  ,13),
('20200202',82 ,null,1   ,0  ,0  ,13),
('20200202',201,null,0   ,0  ,0  ,13),
('20200202',223,null,1   ,0  ,0  ,13),
('20200202',169,null,1   ,1  ,0  ,13),
('20200202',100,null,1   ,0  ,0  ,13),
('20200202',75 ,null,0   ,0  ,0  ,13),
('20200202',107,null,0   ,0  ,0  ,13),
('20200202',175,null,0   ,0  ,0  ,13),
('20200202',200,null,0   ,0  ,0  ,13),
('20200202',206,null,0   ,0  ,0  ,13),
('20200202',225,null,0   ,0  ,0  ,13)



INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200203',45 ,null,2827,57 ,0  ,14),
('20200203',110,null,0   ,0  ,0  ,14),
('20200203',181,null,0   ,0  ,0  ,14),
('20200203',211,null,0   ,0  ,0  ,14),
('20200203',228,null,3   ,0  ,0  ,14),
('20200203',233,null,1   ,0  ,0  ,14),
('20200203',193,null,0   ,0  ,0  ,14),
('20200203',12 ,null,0   ,0  ,0  ,14),
('20200203',149,null,0   ,0  ,0  ,14),
('20200203',76 ,null,0   ,0  ,0  ,14),
('20200203',128,null,0   ,0  ,0  ,14),
('20200203',37 ,null,0   ,0  ,0  ,14),
('20200203',35 ,null,0   ,0  ,0  ,14),
('20200203',82 ,null,2   ,0  ,0  ,14),
('20200203',201,null,0   ,0  ,0  ,14),
('20200203',223,null,0   ,0  ,0  ,14),
('20200203',169,null,0   ,0  ,0  ,14),
('20200203',100,null,1   ,0  ,0  ,14),
('20200203',75 ,null,0   ,0  ,0  ,14),
('20200203',107,null,0   ,0  ,0  ,14),
('20200203',175,null,0   ,0  ,0  ,14),
('20200203',200,null,0   ,0  ,0  ,14),
('20200203',206,null,0   ,0  ,0  ,14),
('20200203',225,null,0   ,0  ,0  ,14)




INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200204',45 ,null,3233,64 ,0  ,15),
('20200204',110,null,0   ,0  ,0  ,15),
('20200204',181,null,1   ,0  ,0  ,15),
('20200204',211,null,0   ,0  ,0  ,15),
('20200204',228,null,0   ,0  ,0  ,15),
('20200204',233,null,1   ,0  ,0  ,15),
('20200204',193,null,0   ,0  ,0  ,15),
('20200204',12 ,null,0   ,0  ,0  ,15),
('20200204',149,null,0   ,0  ,0  ,15),
('20200204',76 ,null,0   ,0  ,0  ,15),
('20200204',128,null,2   ,0  ,0  ,15),
('20200204',37 ,null,0   ,0  ,0  ,15),
('20200204',35 ,null,0   ,0  ,0  ,15),
('20200204',82 ,null,2   ,0  ,0  ,15),
('20200204',201,null,0   ,0  ,0  ,15),
('20200204',223,null,0   ,0  ,0  ,15),
('20200204',169,null,0   ,0  ,0  ,15),
('20200204',100,null,0   ,0  ,0  ,15),
('20200204',75 ,null,0   ,0  ,0  ,15),
('20200204',107,null,0   ,0  ,0  ,15),
('20200204',175,null,0   ,0  ,0  ,15),
('20200204',200,null,0   ,0  ,0  ,15),
('20200204',206,null,0   ,0  ,0  ,15),
('20200204',225,null,0   ,0  ,0  ,15)


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200205',45 ,null,3892,66 ,0  ,16),
('20200205',110,null,3   ,0  ,0  ,16),
('20200205',181,null,2   ,0  ,0  ,16),
('20200205',211,null,6   ,0  ,0  ,16),
('20200205',228,null,0   ,0  ,0  ,16),
('20200205',233,null,1   ,0  ,0  ,16),
('20200205',193,null,6   ,0  ,0  ,16),
('20200205',12 ,null,1   ,0  ,0  ,16),
('20200205',149,null,0   ,0  ,0  ,16),
('20200205',76 ,null,0   ,0  ,0  ,16),
('20200205',128,null,0   ,0  ,0  ,16),
('20200205',37 ,null,1   ,0  ,0  ,16),
('20200205',35 ,null,0   ,0  ,0  ,16),
('20200205',82 ,null,0   ,0  ,0  ,16),
('20200205',201,null,0   ,0  ,0  ,16),
('20200205',223,null,0   ,0  ,0  ,16),
('20200205',169,null,1   ,0  ,0  ,16),
('20200205',100,null,0   ,0  ,0  ,16),
('20200205',75 ,null,0   ,0  ,0  ,16),
('20200205',107,null,0   ,0  ,0  ,16),
('20200205',175,null,0   ,0  ,0  ,16),
('20200205',200,null,0   ,0  ,0  ,16),
('20200205',206,null,0   ,0  ,0  ,16),
('20200205',225,null,0   ,0  ,0  ,16),
('20200205',20 ,null,1   ,0  ,0  ,16)



INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200206',45 ,null,3697,73 ,0  ,17),
('20200206',110,null,2   ,0  ,0  ,17),
('20200206',181,null,5   ,0  ,0  ,17),
('20200206',211,null,0   ,0  ,0  ,17),
('20200206',228,null,1   ,0  ,0  ,17),
('20200206',233,null,0   ,0  ,0  ,17),
('20200206',193,null,4   ,0  ,0  ,17),
('20200206',12 ,null,1   ,0  ,0  ,17),
('20200206',149,null,0   ,0  ,0  ,17),
('20200206',76 ,null,0   ,0  ,0  ,17),
('20200206',128,null,2   ,0  ,0  ,17),
('20200206',37 ,null,0   ,0  ,0  ,17),
('20200206',35 ,null,0   ,0  ,0  ,17),
('20200206',82 ,null,0   ,0  ,0  ,17),
('20200206',201,null,0   ,0  ,0  ,17),
('20200206',223,null,0   ,0  ,0  ,17),
('20200206',169,null,0   ,0  ,0  ,17),
('20200206',100,null,0   ,0  ,0  ,17),
('20200206',75 ,null,0   ,0  ,0  ,17),
('20200206',107,null,0   ,0  ,0  ,17),
('20200206',175,null,0   ,0  ,0  ,17),
('20200206',200,null,0   ,0  ,0  ,17),
('20200206',206,null,0   ,0  ,0  ,17),
('20200206',225,null,0   ,0  ,0  ,17),
('20200206',20 ,null,0   ,0  ,0  ,17),
('20200206',58 ,null,20   ,0  ,0  ,17)



INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200207',45 ,null,3151,73 ,0  ,18),--China
('20200207',193,null,2   ,0  ,0  ,18),--Singapore
('20200207',110,null,0   ,0  ,0  ,18),--Japan
('20200207',181,null,1   ,0  ,0  ,18),--Republic of korea
('20200207',12 ,null,1   ,0  ,0  ,18),--Australia
('20200207',128,null,2   ,0  ,0  ,18),--Malaysia
('20200207',233,null,2   ,0  ,0  ,18),--Vietnam
('20200207',169,null,0   ,0  ,0  ,18),--Philippines
('20200207',35 ,null,0   ,0  ,0  ,18),--Cambodia
('20200207',211,null,0   ,0  ,0  ,18),--Thailand
('20200207',100,null,0   ,0  ,0  ,18),--India
('20200207',149,null,0   ,0  ,0  ,18),--Nepal
('20200207',201,null,0   ,0  ,0  ,18),--Sri Lanka
('20200207',228,null,0   ,0  ,0  ,18),--USA
('20200207',37 ,null,2   ,0  ,0  ,18),--Canada
('20200207',82 ,null,1   ,0  ,0  ,18),--Germany
('20200207',76 ,null,0   ,0  ,0  ,18),--France
('20200207',107,null,1   ,0  ,0  ,18),--Italy
('20200207',225,null,1   ,0  ,0  ,18),--UK (United Kingdom)
('20200207',175,null,0   ,0  ,0  ,18),--Russia
('20200207',20 ,null,0   ,0  ,0  ,18),--Belgium
('20200207',75 ,null,0   ,0  ,0  ,18),--Finland
('20200207',200,null,0   ,0  ,0  ,18),--Spain
('20200207',206,null,0   ,0  ,0  ,18),--Sweden
('20200207',223,null,0   ,0  ,0  ,18),--UAE (United Arab Emirates)
('20200207',58 ,null,41   ,0  ,0  ,18)--Diamond Princess


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200208',45 ,null,3387,73 ,0  ,19),--China
('20200208',193,null,3   ,0  ,0  ,19),--Singapore
('20200208',110,null,0   ,0  ,0  ,19),--Japan
('20200208',181,null,0   ,0  ,0  ,19),--Republic of korea
('20200208',12 ,null,0   ,0  ,0  ,19),--Australia
('20200208',128,null,1   ,0  ,0  ,19),--Malaysia
('20200208',233,null,1   ,0  ,0  ,19),--Vietnam
('20200208',169,null,0   ,0  ,0  ,19),--Philippines
('20200208',35 ,null,0   ,0  ,0  ,19),--Cambodia
('20200208',211,null,7   ,0  ,0  ,19),--Thailand
('20200208',100,null,0   ,0  ,0  ,19),--India
('20200208',149,null,0   ,0  ,0  ,19),--Nepal
('20200208',201,null,0   ,0  ,0  ,19),--Sri Lanka
('20200208',228,null,0   ,0  ,0  ,19),--USA
('20200208',37 ,null,0   ,0  ,0  ,19),--Canada
('20200208',82 ,null,1   ,0  ,0  ,19),--Germany
('20200208',76 ,null,0   ,0  ,0  ,19),--France
('20200208',107,null,0   ,0  ,0  ,19),--Italy
('20200208',225,null,0   ,0  ,0  ,19),--UK (United Kingdom)
('20200208',175,null,0   ,0  ,0  ,19),--Russia
('20200208',20 ,null,0   ,0  ,0  ,19),--Belgium
('20200208',75 ,null,0   ,0  ,0  ,19),--Finland
('20200208',200,null,0   ,0  ,0  ,19),--Spain
('20200208',206,null,0   ,0  ,0  ,19),--Sweden
('20200208',223,null,2   ,0  ,0  ,19),--UAE (United Arab Emirates)
('20200208',58 ,null,3   ,0  ,0  ,19)--Diamond Princess



INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200209',45 ,null,2653,89 ,0  ,20),--China
('20200209',193,null,7   ,0  ,0  ,20),--Singapore
('20200209',110,null,1   ,0  ,0  ,20),--Japan
('20200209',181,null,3   ,0  ,0  ,20),--Republic of korea
('20200209',12 ,null,0   ,0  ,0  ,20),--Australia
('20200209',128,null,2   ,0  ,0  ,20),--Malaysia
('20200209',233,null,1   ,0  ,0  ,20),--Vietnam
('20200209',169,null,0   ,0  ,0  ,20),--Philippines
('20200209',35 ,null,0   ,0  ,0  ,20),--Cambodia
('20200209',211,null,0   ,0  ,0  ,20),--Thailand
('20200209',100,null,0   ,0  ,0  ,20),--India
('20200209',149,null,0   ,0  ,0  ,20),--Nepal
('20200209',201,null,0   ,0  ,0  ,20),--Sri Lanka
('20200209',228,null,0   ,0  ,0  ,20),--USA
('20200209',37 ,null,0   ,0  ,0  ,20),--Canada
('20200209',82 ,null,0   ,0  ,0  ,20),--Germany
('20200209',76 ,null,5   ,0  ,0  ,20),--France
('20200209',107,null,0   ,0  ,0  ,20),--Italy
('20200209',225,null,0   ,0  ,0  ,20),--UK (United Kingdom)
('20200209',175,null,0   ,0  ,0  ,20),--Russia
('20200209',20 ,null,0   ,0  ,0  ,20),--Belgium
('20200209',75 ,null,0   ,0  ,0  ,20),--Finland
('20200209',200,null,0   ,0  ,0  ,20),--Spain
('20200209',206,null,0   ,0  ,0  ,20),--Sweden
('20200209',223,null,0   ,0  ,0  ,20),--UAE (United Arab Emirates)
('20200209',58 ,null,0   ,0  ,0  ,20)--Diamond Princess


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200210',45 ,null,2984,110,0  ,21),--China
('20200210',193,null,3   ,0  ,0  ,21),--Singapore
('20200210',181,null,0   ,0  ,0  ,21),--Republic of korea
('20200210',110,null,0   ,0  ,0  ,21),--Japan
('20200210',128,null,1   ,0  ,0  ,21),--Malaysia
('20200210',12 ,null,0   ,0  ,0  ,21),--Australia
('20200210',233,null,0   ,0  ,0  ,21),--Vietnam
('20200210',169,null,0   ,0  ,0  ,21),--Philippines
('20200210',35 ,null,0   ,0  ,0  ,21),--Cambodia
('20200210',211,null,0   ,0  ,0  ,21),--Thailand
('20200210',100,null,0   ,0  ,0  ,21),--India
('20200210',149,null,0   ,0  ,0  ,21),--Nepal
('20200210',201,null,0   ,0  ,0  ,21),--Sri Lanka
('20200210',228,null,0   ,0  ,0  ,21),--USA
('20200210',37 ,null,0   ,0  ,0  ,21),--Canada
('20200210',82 ,null,0   ,0  ,0  ,21),--Germany
('20200210',76 ,null,0   ,0  ,0  ,21),--France
('20200210',107,null,0   ,0  ,0  ,21),--Italy
('20200210',225,null,1   ,0  ,0  ,21),--UK (United Kingdom)
('20200210',175,null,0   ,0  ,0  ,21),--Russia
('20200210',20 ,null,0   ,0  ,0  ,21),--Belgium
('20200210',75 ,null,0   ,0  ,0  ,21),--Finland
('20200210',200,null,1   ,0  ,0  ,21),--Spain
('20200210',206,null,0   ,0  ,0  ,21),--Sweden
('20200210',223,null,0   ,0  ,0  ,21),--UAE (United Arab Emirates)
('20200210',58 ,null,6   ,0  ,0  ,21)--Diamond Princess

INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200211',45 ,null,2473,108,0  ,22),--China
('20200211',193,null,2   ,0  ,0  ,22),--Singapore
('20200211',181,null,1   ,0  ,0  ,22),--Republic of korea
('20200211',110,null,0   ,0  ,0  ,22),--Japan
('20200211',128,null,0   ,0  ,0  ,22),--Malaysia
('20200211',12 ,null,0   ,0  ,0  ,22),--Australia
('20200211',233,null,1   ,0  ,0  ,22),--Vietnam
('20200211',169,null,0   ,0  ,0  ,22),--Philippines
('20200211',35 ,null,0   ,0  ,0  ,22),--Cambodia
('20200211',211,null,1   ,0  ,0  ,22),--Thailand
('20200211',100,null,0   ,0  ,0  ,22),--India
('20200211',149,null,0   ,0  ,0  ,22),--Nepal
('20200211',201,null,0   ,0  ,0  ,22),--Sri Lanka
('20200211',228,null,1   ,0  ,0  ,22),--USA
('20200211',37 ,null,0   ,0  ,0  ,22),--Canada
('20200211',82 ,null,0   ,0  ,0  ,22),--Germany
('20200211',76 ,null,0   ,0  ,0  ,22),--France
('20200211',107,null,0   ,0  ,0  ,22),--Italy
('20200211',225,null,4   ,0  ,0  ,22),--UK (United Kingdom)
('20200211',175,null,0   ,0  ,0  ,22),--Russia
('20200211',20 ,null,0   ,0  ,0  ,22),--Belgium
('20200211',75 ,null,0   ,0  ,0  ,22),--Finland
('20200211',200,null,0   ,0  ,0  ,22),--Spain
('20200211',206,null,0   ,0  ,0  ,22),--Sweden
('20200211',223,null,1   ,0  ,0  ,22),--UAE (United Arab Emirates)
('20200211',58 ,null,65   ,0  ,0  ,22)--Diamond Princess


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200212',45 ,null,2022,97 ,0  ,23),--China
('20200212',193,null,2   ,0  ,0  ,23),--Singapore
('20200212',110,null,2   ,0  ,0  ,23),--Japan
('20200212',181,null,0   ,0  ,0  ,23),--Republic of korea
('20200212',128,null,0   ,0  ,0  ,23),--Malaysia
('20200212',12 ,null,0   ,0  ,0  ,23),--Australia
('20200212',233,null,0   ,0  ,0  ,23),--Vietnam
('20200212',169,null,0   ,0  ,0  ,23),--Philippines
('20200212',35 ,null,0   ,0  ,0  ,23),--Cambodia
('20200212',211,null,0   ,0  ,0  ,23),--Thailand
('20200212',100,null,0   ,0  ,0  ,23),--India
('20200212',149,null,0   ,0  ,0  ,23),--Nepal
('20200212',201,null,0   ,0  ,0  ,23),--Sri Lanka
('20200212',228,null,0   ,0  ,0  ,23),--USA
('20200212',37 ,null,0   ,0  ,0  ,23),--Canada
('20200212',82 ,null,2   ,0  ,0  ,23),--Germany
('20200212',76 ,null,0   ,0  ,0  ,23),--France
('20200212',107,null,0   ,0  ,0  ,23),--Italy
('20200212',175,null,0   ,0  ,0  ,23),--Russia
('20200212',225,null,0   ,0  ,0  ,23),--UK (United Kingdom)
('20200212',20 ,null,0   ,0  ,0  ,23),--Belgium
('20200212',75 ,null,0   ,0  ,0  ,23),--Finland
('20200212',200,null,0   ,0  ,0  ,23),--Spain
('20200212',206,null,0   ,0  ,0  ,23),--Sweden
('20200212',223,null,0   ,0  ,0  ,23),--UAE (United Arab Emirates)
('20200212',58 ,null,40  ,0  ,0  ,23)--Diamond Princess


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200213',45 ,null,1820,254  ,0  ,24),--China
('20200213',193,null,3   ,0  ,0  ,24),--Singapore
('20200213',110,null,0   ,0  ,0  ,24),--Japan
('20200213',181,null,0   ,0  ,0  ,24),--Republic of korea
('20200213',128,null,0   ,0  ,0  ,24),--Malaysia
('20200213',12 ,null,0   ,0  ,0  ,24),--Australia
('20200213',233,null,1   ,0  ,0  ,24),--Vietnam
('20200213',169,null,0   ,0  ,0  ,24),--Philippines
('20200213',35 ,null,0   ,0  ,0  ,24),--Cambodia
('20200213',211,null,0   ,0  ,0  ,24),--Thailand
('20200213',100,null,0   ,0  ,0  ,24),--India
('20200213',149,null,0   ,0  ,0  ,24),--Nepal
('20200213',201,null,0   ,0  ,0  ,24),--Sri Lanka
('20200213',228,null,1   ,0  ,0  ,24),--USA
('20200213',37 ,null,0   ,0  ,0  ,24),--Canada
('20200213',82 ,null,0   ,0  ,0  ,24),--Germany
('20200213',76 ,null,0   ,0  ,0  ,24),--France
('20200213',107,null,0   ,0  ,0  ,24),--Italy
('20200213',175,null,0   ,0  ,0  ,24),--Russia
('20200213',225,null,1   ,0  ,0  ,24),--UK (United Kingdom)
('20200213',20 ,null,0   ,0  ,0  ,24),--Belgium
('20200213',75 ,null,0   ,0  ,0  ,24),--Finland
('20200213',200,null,0   ,0  ,0  ,24),--Spain
('20200213',206,null,0   ,0  ,0  ,24),--Sweden
('20200213',223,null,0   ,0  ,0  ,24),--UAE (United Arab Emirates)
('20200213',58 ,null,0  ,0  ,0  ,24)--Diamond Princess




INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200214',45 ,null,1998,121,0  ,25),--China
('20200214',193,null,8   ,0  ,0  ,25),--Singapore
('20200214',110,null,4   ,1  ,0  ,25),--Japan
('20200214',181,null,0   ,0  ,0  ,25),--Republic of korea
('20200214',128,null,1   ,0  ,0  ,25),--Malaysia
('20200214',12 ,null,0   ,0  ,0  ,25),--Australia
('20200214',233,null,0   ,0  ,0  ,25),--Vietnam
('20200214',169,null,0   ,0  ,0  ,25),--Philippines
('20200214',35 ,null,0   ,0  ,0  ,25),--Cambodia
('20200214',211,null,0   ,0  ,0  ,25),--Thailand
('20200214',100,null,0   ,0  ,0  ,25),--India
('20200214',149,null,0   ,0  ,0  ,25),--Nepal
('20200214',201,null,0   ,0  ,0  ,25),--Sri Lanka
('20200214',228,null,1   ,0  ,0  ,25),--USA
('20200214',37 ,null,0   ,0  ,0  ,25),--Canada
('20200214',82 ,null,0   ,0  ,0  ,25),--Germany
('20200214',76 ,null,0   ,0  ,0  ,25),--France
('20200214',107,null,0   ,0  ,0  ,25),--Italy
('20200214',175,null,0   ,0  ,0  ,25),--Russia
('20200214',225,null,0   ,0  ,0  ,25),--UK (United Kingdom)
('20200214',20 ,null,0   ,0  ,0  ,25),--Belgium
('20200214',75 ,null,0   ,0  ,0  ,25),--Finland
('20200214',200,null,0   ,0  ,0  ,25),--Spain
('20200214',206,null,0   ,0  ,0  ,25),--Sweden
('20200214',223,null,0   ,0  ,0  ,25),--UAE (United Arab Emirates)
('20200214',58 ,null,44  ,0  ,0  ,25)--Diamond Princess



INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200215',45 ,null,1506,121,0  ,26),--China
('20200215',193,null,9   ,0  ,0  ,26),--Singapore
('20200215',110,null,8   ,1  ,0  ,26),--Japan
('20200215',181,null,0   ,0  ,0  ,26),--Republic of korea
('20200215',128,null,2   ,0  ,0  ,26),--Malaysia
('20200215',12 ,null,0   ,0  ,0  ,26),--Australia
('20200215',233,null,0   ,0  ,0  ,26),--Vietnam
('20200215',169,null,0   ,0  ,0  ,26),--Philippines
('20200215',35 ,null,0   ,0  ,0  ,26),--Cambodia
('20200215',211,null,1   ,0  ,0  ,26),--Thailand
('20200215',100,null,0   ,0  ,0  ,26),--India
('20200215',149,null,0   ,0  ,0  ,26),--Nepal
('20200215',201,null,0   ,0  ,0  ,26),--Sri Lanka
('20200215',228,null,0   ,0  ,0  ,26),--USA
('20200215',37 ,null,0   ,0  ,0  ,26),--Canada
('20200215',82 ,null,0   ,0  ,0  ,26),--Germany
('20200215',76 ,null,0   ,0  ,0  ,26),--France
('20200215',107,null,0   ,0  ,0  ,26),--Italy
('20200215',175,null,0   ,0  ,0  ,26),--Russia
('20200215',225,null,0   ,0  ,0  ,26),--UK (United Kingdom)
('20200215',20 ,null,0   ,0  ,0  ,26),--Belgium
('20200215',75 ,null,0   ,0  ,0  ,26),--Finland
('20200215',200,null,0   ,0  ,0  ,26),--Spain
('20200215',206,null,0   ,0  ,0  ,26),--Sweden
('20200215',223,null,0   ,0  ,0  ,26),--UAE (United Arab Emirates)
('20200215',65 ,null,1   ,0  ,0  ,26),--Egypt
('20200215',58 ,null,0  ,0  ,0  ,26)--Diamond Princess


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200216',45 ,null,1120,56 ,0  ,27),--China
('20200216',193,null,5   ,0  ,0  ,27),--Singapore
('20200216',110,null,12  ,0  ,0  ,27),--Japan
('20200216',181,null,1   ,0  ,0  ,27),--Republic of korea
('20200216',128,null,1   ,0  ,0  ,27),--Malaysia
('20200216',12 ,null,0   ,0  ,0  ,27),--Australia
('20200216',233,null,0   ,0  ,0  ,27),--Vietnam
('20200216',169,null,0   ,0  ,0  ,27),--Philippines
('20200216',35 ,null,0   ,0  ,0  ,27),--Cambodia
('20200216',211,null,0   ,0  ,0  ,27),--Thailand
('20200216',100,null,0   ,0  ,0  ,27),--India
('20200216',149,null,0   ,0  ,0  ,27),--Nepal
('20200216',201,null,0   ,0  ,0  ,27),--Sri Lanka
('20200216',228,null,0   ,0  ,0  ,27),--USA
('20200216',37 ,null,0   ,0  ,0  ,27),--Canada
('20200216',82 ,null,0   ,0  ,0  ,27),--Germany
('20200216',76 ,null,1   ,1  ,0  ,27),--France
('20200216',107,null,0   ,0  ,0  ,27),--Italy
('20200216',175,null,0   ,0  ,0  ,27),--Russia
('20200216',225,null,0   ,0  ,0  ,27),--UK (United Kingdom)
('20200216',20 ,null,0   ,0  ,0  ,27),--Belgium
('20200216',75 ,null,0   ,0  ,0  ,27),--Finland
('20200216',200,null,0   ,0  ,0  ,27),--Spain
('20200216',206,null,0   ,0  ,0  ,27),--Sweden
('20200216',223,null,0   ,0  ,0  ,27),--UAE (United Arab Emirates)
('20200216',65 ,null,1   ,0  ,0  ,27),--Egypt
('20200216',58 ,null,136 ,0  ,0  ,27)--Diamond Princess


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200217',45 ,null,19461,106,0 ,28),--China
('20200217',193,null,3   ,0  ,0  ,28),--Singapore
('20200217',110,null,6  ,0  ,0  ,28),--Japan
('20200217',181,null,1   ,0  ,0  ,28),--Republic of korea
('20200217',128,null,0   ,0  ,0  ,28),--Malaysia
('20200217',12 ,null,0   ,0  ,0  ,28),--Australia
('20200217',233,null,0   ,0  ,0  ,28),--Vietnam
('20200217',169,null,0   ,0  ,0  ,28),--Philippines
('20200217',35 ,null,0   ,0  ,0  ,28),--Cambodia
('20200217',211,null,1   ,0  ,0  ,28),--Thailand
('20200217',100,null,0   ,0  ,0  ,28),--India
('20200217',149,null,0   ,0  ,0  ,28),--Nepal
('20200217',201,null,0   ,0  ,0  ,28),--Sri Lanka
('20200217',228,null,0   ,0  ,0  ,28),--USA
('20200217',37 ,null,0   ,0  ,0  ,28),--Canada
('20200217',82 ,null,0   ,0  ,0  ,28),--Germany
('20200217',76 ,null,0   ,0  ,0  ,28),--France
('20200217',107,null,0   ,0  ,0  ,28),--Italy
('20200217',175,null,0   ,0  ,0  ,28),--Russia
('20200217',225,null,0   ,0  ,0  ,28),--UK (United Kingdom)
('20200217',20 ,null,0   ,0  ,0  ,28),--Belgium
('20200217',75 ,null,0   ,0  ,0  ,28),--Finland
('20200217',200,null,0   ,0  ,0  ,28),--Spain
('20200217',206,null,0   ,0  ,0  ,28),--Sweden
('20200217',223,null,1   ,0  ,0  ,28),--UAE (United Arab Emirates)
('20200217',65 ,null,0   ,0  ,0  ,28),--Egypt
('20200217',58 ,null,99  ,0  ,0  ,28)--Diamond Princess


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200218',45 ,null,1893,98 ,0 ,29),--China
('20200218',193,null,2   ,0  ,0  ,29),--Singapore
('20200218',110,null,6   ,0  ,0  ,29),--Japan
('20200218',181,null,1   ,0  ,0  ,29),--Republic of korea
('20200218',128,null,0   ,0  ,0  ,29),--Malaysia
('20200218',12 ,null,0   ,0  ,0  ,29),--Australia
('20200218',233,null,0   ,0  ,0  ,29),--Vietnam
('20200218',169,null,0   ,0  ,0  ,29),--Philippines
('20200218',35 ,null,0   ,0  ,0  ,29),--Cambodia
('20200218',211,null,0   ,0  ,0  ,29),--Thailand
('20200218',100,null,0   ,0  ,0  ,29),--India
('20200218',149,null,0   ,0  ,0  ,29),--Nepal
('20200218',201,null,0   ,0  ,0  ,29),--Sri Lanka
('20200218',228,null,0   ,0  ,0  ,29),--USA
('20200218',37 ,null,1   ,0  ,0  ,29),--Canada
('20200218',82 ,null,0   ,0  ,0  ,29),--Germany
('20200218',76 ,null,0   ,0  ,0  ,29),--France
('20200218',107,null,0   ,0  ,0  ,29),--Italy
('20200218',175,null,0   ,0  ,0  ,29),--Russia
('20200218',225,null,0   ,0  ,0  ,29),--UK (United Kingdom)
('20200218',20 ,null,0   ,0  ,0  ,29),--Belgium
('20200218',75 ,null,0   ,0  ,0  ,29),--Finland
('20200218',200,null,0   ,0  ,0  ,29),--Spain
('20200218',206,null,0   ,0  ,0  ,29),--Sweden
('20200218',223,null,0   ,0  ,0  ,29),--UAE (United Arab Emirates)
('20200218',65 ,null,0   ,0  ,0  ,29),--Egypt
('20200218',58 ,null,0  ,0  ,0  ,29)--Diamond Princess



INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200219',45 ,null,1752,136 ,0  ,30),--China
('20200219',193,null,4   ,0  ,0  ,30),--Singapore
('20200219',110,null,8   ,0  ,0  ,30),--Japan
('20200219',181,null,20   ,0  ,0  ,30),--Republic of korea
('20200219',128,null,0   ,0  ,0  ,30),--Malaysia
('20200219',12 ,null,0   ,0  ,0  ,30),--Australia
('20200219',233,null,0   ,0  ,0  ,30),--Vietnam
('20200219',169,null,0   ,0  ,0  ,30),--Philippines
('20200219',35 ,null,0   ,0  ,0  ,30),--Cambodia
('20200219',211,null,0   ,0  ,0  ,30),--Thailand
('20200219',100,null,0   ,0  ,0  ,30),--India
('20200219',149,null,0   ,0  ,0  ,30),--Nepal
('20200219',201,null,0   ,0  ,0  ,30),--Sri Lanka
('20200219',228,null,0   ,0  ,0  ,30),--USA
('20200219',37 ,null,0   ,0  ,0  ,30),--Canada
('20200219',82 ,null,0   ,0  ,0  ,30),--Germany
('20200219',76 ,null,0   ,0  ,0  ,30),--France
('20200219',107,null,0   ,0  ,0  ,30),--Italy
('20200219',175,null,0   ,0  ,0  ,30),--Russia
('20200219',225,null,0   ,0  ,0  ,30),--UK (United Kingdom)
('20200219',20 ,null,0   ,0  ,0  ,30),--Belgium
('20200219',75 ,null,0   ,0  ,0  ,30),--Finland
('20200219',200,null,0   ,0  ,0  ,30),--Spain
('20200219',206,null,0   ,0  ,0  ,30),--Sweden
('20200219',223,null,0   ,0  ,0  ,30),--UAE (United Arab Emirates)
('20200219',65 ,null,0   ,0  ,0  ,30),--Egypt
('20200219',58 ,null,88  ,0  ,0  ,30)--Diamond Princess



INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200220',45 ,null,395 ,115,0  ,31),--China
('20200220',181,null,53   ,1  ,0  ,31),--Republic of korea
('20200220',110,null,12   ,0  ,0  ,31),--Japan
('20200220',193,null,3   ,0  ,0  ,31),--Singapore
('20200220',128,null,0   ,0  ,0  ,31),--Malaysia
('20200220',233,null,0   ,0  ,0  ,31),--Vietnam
('20200220',12 ,null,0   ,0  ,0  ,31),--Australia
('20200220',169,null,0   ,0  ,0  ,31),--Philippines
('20200220',35 ,null,0   ,0  ,0  ,31),--Cambodia
('20200220',211,null,0   ,0  ,0  ,31),--Thailand
('20200220',100,null,0   ,0  ,0  ,31),--India
('20200220',149,null,0   ,0  ,0  ,31),--Nepal
('20200220',201,null,0   ,0  ,0  ,31),--Sri Lanka
('20200220',228,null,0   ,0  ,0  ,31),--USA
('20200220',37 ,null,0   ,0  ,0  ,31),--Canada
('20200220',82 ,null,0   ,0  ,0  ,31),--Germany
('20200220',76 ,null,0   ,0  ,0  ,31),--France
('20200220',107,null,0   ,0  ,0  ,31),--Italy
('20200220',175,null,0   ,0  ,0  ,31),--Russia
('20200220',225,null,0   ,0  ,0  ,31),--UK (United Kingdom)
('20200220',20 ,null,0   ,0  ,0  ,31),--Belgium
('20200220',75 ,null,0   ,0  ,0  ,31),--Finland
('20200220',200,null,0   ,0  ,0  ,31),--Spain
('20200220',206,null,0   ,0  ,0  ,31),--Sweden
('20200220',223,null,0   ,0  ,0  ,31),--UAE (United Arab Emirates)
('20200220',102,null,2   ,2  ,0  ,31),--Iran
('20200220',65 ,null,0   ,0  ,0  ,31),--Egypt
('20200220',58 ,null,79  ,2  ,0  ,31)--Diamond Princess


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200221',45 ,null,894 ,118,0  ,32),--China
('20200221',181,null,100 ,0  ,0  ,32),--Republic of korea
('20200221',110,null,8   ,0  ,0  ,32),--Japan
('20200221',193,null,1   ,0  ,0  ,32),--Singapore
('20200221',128,null,0   ,0  ,0  ,32),--Malaysia
('20200221',233,null,0   ,0  ,0  ,32),--Vietnam
('20200221',12 ,null,2   ,0  ,0  ,32),--Australia
('20200221',169,null,0   ,0  ,0  ,32),--Philippines
('20200221',35 ,null,0   ,0  ,0  ,32),--Cambodia
('20200221',211,null,0   ,0  ,0  ,32),--Thailand
('20200221',100,null,0   ,0  ,0  ,32),--India
('20200221',149,null,0   ,0  ,0  ,32),--Nepal
('20200221',201,null,0   ,0  ,0  ,32),--Sri Lanka
('20200221',228,null,0   ,0  ,0  ,32),--USA
('20200221',37 ,null,0   ,0  ,0  ,32),--Canada
('20200221',82 ,null,0   ,0  ,0  ,32),--Germany
('20200221',76 ,null,0   ,0  ,0  ,32),--France
('20200221',107,null,0   ,0  ,0  ,32),--Italy
('20200221',175,null,0   ,0  ,0  ,32),--Russia
('20200221',225,null,0   ,0  ,0  ,32),--UK (United Kingdom)
('20200221',20 ,null,0   ,0  ,0  ,32),--Belgium
('20200221',75 ,null,0   ,0  ,0  ,32),--Finland
('20200221',200,null,0   ,0  ,0  ,32),--Spain
('20200221',206,null,0   ,0  ,0  ,32),--Sweden
('20200221',223,null,0   ,0  ,0  ,32),--UAE (United Arab Emirates)
('20200221',102,null,3   ,0  ,0  ,32),--Iran
('20200221',65 ,null,0   ,0  ,0  ,32),--Egypt
('20200221',58 ,null,13  ,0  ,0  ,32)--Diamond Princess


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200222',45 ,null,823 ,109,0  ,33),--China
('20200222',181,null,142 ,0  ,0  ,33),--Republic of korea
('20200222',110,null,12  ,0  ,0  ,33),--Japan
('20200222',193,null,1   ,0  ,0  ,33),--Singapore
('20200222',128,null,0   ,0  ,0  ,33),--Malaysia
('20200222',233,null,0   ,0  ,0  ,33),--Vietnam
('20200222',12 ,null,4   ,0  ,0  ,33),--Australia
('20200222',169,null,0   ,0  ,0  ,33),--Philippines
('20200222',35 ,null,0   ,0  ,0  ,33),--Cambodia
('20200222',211,null,0   ,0  ,0  ,33),--Thailand
('20200222',100,null,0   ,0  ,0  ,33),--India
('20200222',149,null,0   ,0  ,0  ,33),--Nepal
('20200222',201,null,0   ,0  ,0  ,33),--Sri Lanka
('20200222',228,null,20  ,0  ,0  ,33),--USA
('20200222',37 ,null,0   ,0  ,0  ,33),--Canada
('20200222',82 ,null,0   ,0  ,0  ,33),--Germany
('20200222',76 ,null,0   ,0  ,0  ,33),--France
('20200222',107,null,6   ,0  ,0  ,33),--Italy
('20200222',175,null,0   ,0  ,0  ,33),--Russia
('20200222',225,null,0   ,0  ,0  ,33),--UK (United Kingdom)
('20200222',200,null,0   ,0  ,0  ,33),--Spain
('20200222',20 ,null,0   ,0  ,0  ,33),--Belgium
('20200222',75 ,null,0   ,0  ,0  ,33),--Finland
('20200222',106,null,1   ,0  ,0  ,33),--Israel
('20200222',206,null,0   ,0  ,0  ,33),--Sweden
('20200222',223,null,2   ,0  ,0  ,33),--UAE (United Arab Emirates)
('20200222',102,null,13  ,2  ,0  ,33),--Iran
('20200222',65 ,null,0   ,0  ,0  ,33),--Egypt
('20200222',119 ,null,1  ,0  ,0  ,33),--lebanon
('20200222',58 ,null,0  ,0  ,0  ,33)--Diamond Princess



INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200223',45 ,null,650 ,97 ,0  ,34),--China
('20200223',181,null,256 ,3  ,0  ,34),--Republic of korea
('20200223',110,null,27  ,0  ,0  ,34),--Japan
('20200223',193,null,3   ,0  ,0  ,34),--Singapore
('20200223',12 ,null,1   ,0  ,0  ,34),--Australia
('20200223',128,null,0   ,0  ,0  ,34),--Malaysia
('20200223',233,null,0   ,0  ,0  ,34),--Vietnam
('20200223',169,null,0   ,0  ,0  ,34),--Philippines
('20200223',35 ,null,0   ,0  ,0  ,34),--Cambodia
('20200223',211,null,0   ,0  ,0  ,34),--Thailand
('20200223',100,null,0   ,0  ,0  ,34),--India
('20200223',149,null,0   ,0  ,0  ,34),--Nepal
('20200223',201,null,0   ,0  ,0  ,34),--Sri Lanka
('20200223',228,null,0   ,0  ,0  ,34),--USA
('20200223',37 ,null,1   ,0  ,0  ,34),--Canada
('20200223',82 ,null,0   ,0  ,0  ,34),--Germany
('20200223',76 ,null,0   ,0  ,0  ,34),--France
('20200223',107,null,67   ,2  ,0  ,34),--Italy
('20200223',175,null,0   ,0  ,0  ,34),--Russia
('20200223',225,null,0   ,0  ,0  ,34),--UK (United Kingdom)
('20200223',200,null,0   ,0  ,0  ,34),--Spain
('20200223',20 ,null,0   ,0  ,0  ,34),--Belgium
('20200223',75 ,null,0   ,0  ,0  ,34),--Finland
('20200223',106,null,0   ,0  ,0  ,34),--Israel
('20200223',206,null,0   ,0  ,0  ,34),--Sweden
('20200223',223,null,2   ,0  ,0  ,34),--UAE (United Arab Emirates)
('20200223',102,null,10  ,1  ,0  ,34),--Iran
('20200223',65 ,null,0   ,0  ,0  ,34),--Egypt
('20200223',119,null,0   ,0  ,0  ,34),--lebanon
('20200223',58 ,null,0   ,0  ,0  ,34)--Diamond Princess


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200224',45 ,null,220 ,150,0  ,35),--China
('20200224',181,null,161 ,2  ,0  ,35),--Republic of korea
('20200224',110,null,12  ,0  ,0  ,35),--Japan
('20200224',193,null,0   ,0  ,0  ,35),--Singapore
('20200224',12 ,null,0   ,0  ,0  ,35),--Australia
('20200224',128,null,0   ,0  ,0  ,35),--Malaysia
('20200224',233,null,0   ,0  ,0  ,35),--Vietnam
('20200224',169,null,0   ,0  ,0  ,35),--Philippines
('20200224',35 ,null,0   ,0  ,0  ,35),--Cambodia
('20200224',211,null,0   ,0  ,0  ,35),--Thailand
('20200224',100,null,0   ,0  ,0  ,35),--India
('20200224',149,null,0   ,0  ,0  ,35),--Nepal
('20200224',201,null,0   ,0  ,0  ,35),--Sri Lanka
('20200224',228,null,0   ,0  ,0  ,35),--USA
('20200224',37 ,null,0   ,0  ,0  ,35),--Canada
('20200224',107,null,48  ,0  ,0  ,35),--Italy
('20200224',82 ,null,0   ,0  ,0  ,35),--Germany
('20200224',76 ,null,0   ,0  ,0  ,35),--France
('20200224',175,null,0   ,0  ,0  ,35),--Russia
('20200224',225,null,0   ,0  ,0  ,35),--UK (United Kingdom)
('20200224',200,null,0   ,0  ,0  ,35),--Spain
('20200224',20 ,null,0   ,0  ,0  ,35),--Belgium
('20200224',75 ,null,0   ,0  ,0  ,35),--Finland
('20200224',106,null,0   ,0  ,0  ,35),--Israel
('20200224',206,null,0   ,0  ,0  ,35),--Sweden
('20200224',102,null,15  ,3  ,0  ,35),--Iran
('20200224',223,null,0   ,0  ,0  ,35),--UAE (United Arab Emirates)
('20200224',115,null,3   ,0  ,0  ,35),--Kuwait
('20200224',65 ,null,0   ,0  ,0  ,35),--Egypt
('20200224',119,null,0   ,0  ,0  ,35),--lebanon
('20200224',58 ,null,61   ,1  ,0  ,35)--Diamond Princess



INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200225',45 ,null,518 ,71 ,0  ,36),--China
('20200225',181,null,214 ,3  ,0  ,36),--Republic of korea
('20200225',110,null,13  ,0  ,0  ,36),--Japan
('20200225',193,null,1   ,0  ,0  ,36),--Singapore
('20200225',12 ,null,0   ,0  ,0  ,36),--Australia
('20200225',128,null,0   ,0  ,0  ,36),--Malaysia
('20200225',233,null,0   ,0  ,0  ,36),--Vietnam
('20200225',169,null,0   ,0  ,0  ,36),--Philippines
('20200225',35 ,null,0   ,0  ,0  ,36),--Cambodia
('20200225',211,null,2   ,0  ,0  ,36),--Thailand
('20200225',100,null,0   ,0  ,0  ,36),--India
('20200225',149,null,0   ,0  ,0  ,36),--Nepal
('20200225',201,null,0   ,0  ,0  ,36),--Sri Lanka
('20200225',228,null,18  ,0  ,0  ,36),--USA
('20200225',37 ,null,1   ,0  ,0  ,36),--Canada
('20200225',107,null,105 ,4  ,0  ,36),--Italy
('20200225',82 ,null,0   ,0  ,0  ,36),--Germany
('20200225',76 ,null,0   ,0  ,0  ,36),--France
('20200225',175,null,0   ,0  ,0  ,36),--Russia
('20200225',225,null,0   ,0  ,0  ,36),--UK (United Kingdom)
('20200225',106,null,1   ,0  ,0  ,36),--Israel
('20200225',200,null,0   ,0  ,0  ,36),--Spain
('20200225',20 ,null,0   ,0  ,0  ,36),--Belgium
('20200225',75 ,null,0   ,0  ,0  ,36),--Finland
('20200225',206,null,0   ,0  ,0  ,36),--Sweden
('20200225',102,null,18  ,4  ,0  ,36),--Iran
('20200225',223,null,0   ,0  ,0  ,36),--UAE (United Arab Emirates)
('20200225',115,null,5   ,0  ,0  ,36),--Kuwait
('20200225',1  ,null,1   ,0  ,0  ,36),--Afghanistan
('20200225',16 ,null,8   ,0  ,0  ,36),--Bahrain
('20200225',103,null,1   ,0  ,0  ,36),--Iraq
('20200225',161,null,2   ,0  ,0  ,36),--Oman
('20200225',65 ,null,0   ,0  ,0  ,36),--Egypt
('20200225',119,null,0   ,0  ,0  ,36),--lebanon
('20200225',58 ,null,0  ,0  ,0  ,36)--Diamond Princess



INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200226',45 ,null,411 ,52 ,0  ,37),--China
('20200226',181,null,284 ,2  ,0  ,37),--Republic of korea
('20200226',110,null,7   ,0  ,0  ,37),--Japan
('20200226',193,null,1   ,0  ,0  ,37),--Singapore
('20200226',12 ,null,1   ,0  ,0  ,37),--Australia
('20200226',128,null,0   ,0  ,0  ,37),--Malaysia
('20200226',233,null,0   ,0  ,0  ,37),--Vietnam
('20200226',169,null,0   ,0  ,0  ,37),--Philippines
('20200226',35 ,null,0   ,0  ,0  ,37),--Cambodia
('20200226',211,null,3   ,0  ,0  ,37),--Thailand
('20200226',100,null,0   ,0  ,0  ,37),--India
('20200226',149,null,0   ,0  ,0  ,37),--Nepal
('20200226',201,null,0   ,0  ,0  ,37),--Sri Lanka
('20200226',228,null,0   ,0  ,0  ,37),--USA
('20200226',37 ,null,0   ,0  ,0  ,37),--Canada
('20200226',107,null,93  ,5  ,0  ,37),--Italy
('20200226',82 ,null,2   ,0  ,0  ,37),--Germany
('20200226',76 ,null,0   ,0  ,0  ,37),--France
('20200226',175,null,0   ,0  ,0  ,37),--Russia
('20200226',225,null,0   ,0  ,0  ,37),--UK (United Kingdom)
('20200226',13 ,null,2   ,0  ,0  ,37),--Austria
('20200226',51 ,null,2   ,0  ,0  ,37),--Croatia
('20200226',106,null,0   ,0  ,0  ,37),--Israel
('20200226',200,null,0   ,0  ,0  ,37),--Spain
('20200226',20 ,null,0   ,0  ,0  ,37),--Belgium
('20200226',75 ,null,0   ,0  ,0  ,37),--Finland
('20200226',206,null,0   ,0  ,0  ,37),--Sweden
('20200226',207,null,1   ,0  ,0  ,37),--Switzerland
('20200226',102,null,34  ,3  ,0  ,37),--Iran
('20200226',16 ,null,18   ,0  ,0  ,37),--Bahrain
('20200226',223,null,0   ,0  ,0  ,37),--UAE (United Arab Emirates)
('20200226',115,null,4   ,0  ,0  ,37),--Kuwait
('20200226',103,null,4   ,0  ,0  ,37),--Iraq
('20200226',161,null,2   ,0  ,0  ,37),--Oman
('20200226',1  ,null,0   ,0  ,0  ,37),--Afghanistan
('20200226',65 ,null,0   ,0  ,0  ,37),--Egypt
('20200226',119,null,0   ,0  ,0  ,37),--lebanon
('20200226',3  ,null,1   ,0  ,0  ,37),--Algeria
('20200226',58 ,null,0   ,0  ,0  ,37)--Diamond Princess



INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200227',45 ,null,439 ,29 ,0  ,38),--China
('20200227',181,null,505 ,1  ,0  ,38),--Republic of korea
('20200227',110,null,23  ,2  ,0  ,38),--Japan
('20200227',193,null,2   ,0  ,0  ,38),--Singapore
('20200227',12 ,null,0   ,0  ,0  ,38),--Australia
('20200227',128,null,0   ,0  ,0  ,38),--Malaysia
('20200227',233,null,0   ,0  ,0  ,38),--Vietnam
('20200227',169,null,0   ,0  ,0  ,38),--Philippines
('20200227',35 ,null,0   ,0  ,0  ,38),--Cambodia
('20200227',211,null,0   ,0  ,0  ,38),--Thailand
('20200227',100,null,0   ,0  ,0  ,38),--India
('20200227',149,null,0   ,0  ,0  ,38),--Nepal
('20200227',201,null,0   ,0  ,0  ,38),--Sri Lanka
('20200227',228,null,6   ,0  ,0  ,38),--USA
('20200227',37 ,null,1   ,0  ,0  ,38),--Canada
('20200227',28 ,null,1   ,0  ,0  ,38),--Brasil
('20200227',107,null,78  ,1  ,0  ,38),--Italy
('20200227',82 ,null,3   ,0  ,0  ,38),--Germany
('20200227',76 ,null,6   ,1  ,0  ,38),--France
('20200227',175,null,0   ,0  ,0  ,38),--Russia
('20200227',225,null,0   ,0  ,0  ,38),--UK (United Kingdom)
('20200227',13 ,null,0   ,0  ,0  ,38),--Austria
('20200227',51 ,null,1   ,0  ,0  ,38),--Croatia
('20200227',106,null,0   ,0  ,0  ,38),--Israel
('20200227',200,null,10  ,0  ,0  ,38),--Spain
('20200227',20 ,null,0   ,0  ,0  ,38),--Belgium
('20200227',75 ,null,0   ,0  ,0  ,38),--Finland
('20200227',206,null,1   ,0  ,0  ,38),--Sweden
('20200227',207,null,0   ,0  ,0  ,38),--Switzerland
('20200227',57 ,null,1   ,0  ,0  ,38),--Denmark
('20200227',69 ,null,1   ,0  ,0  ,38),--Estonia
('20200227',81 ,null,1   ,0  ,0  ,38),--Georgia
('20200227',85 ,null,1   ,0  ,0  ,38),--Greece
('20200227',158,null,1   ,0  ,0  ,38),--North Macedonia
('20200227',160,null,1   ,0  ,0  ,38),--Norway
('20200227',174,null,1   ,0  ,0  ,38),--Romania
('20200227',102,null,46  ,7  ,0  ,38),--Iran
('20200227',115,null,31  ,0  ,0  ,38),--Kuwait
('20200227',16 ,null,7   ,0  ,0  ,38),--Bahrain
('20200227',223,null,0   ,0  ,0  ,38),--UAE (United Arab Emirates)
('20200227',103,null,1   ,0  ,0  ,38),--Iraq
('20200227',161,null,0   ,0  ,0  ,38),--Oman
('20200227',1  ,null,0   ,0  ,0  ,38),--Afghanistan
('20200227',65 ,null,0   ,0  ,0  ,38),--Egypt
('20200227',119,null,1   ,0  ,0  ,38),--lebanon
('20200227',162,null,2   ,0  ,0  ,38),--Pakistan
('20200227',3  ,null,0   ,0  ,0  ,38),--Algeria
('20200227',58 ,null,14   ,1  ,0  ,38)--Diamond Princess


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200228',45 ,null,331 ,44 ,0  ,39),--China
('20200228',181,null,571 ,1  ,0  ,39),--Republic of korea
('20200228',110,null,24  ,0  ,0  ,39),--Japan
('20200228',193,null,3   ,0  ,0  ,39),--Singapore
('20200228',128,null,2   ,0  ,0  ,39),--Malaysia
('20200228',12 ,null,0   ,0  ,0  ,39),--Australia
('20200228',233,null,0   ,0  ,0  ,39),--Vietnam
('20200228',169,null,0   ,0  ,0  ,39),--Philippines
('20200228',35 ,null,0   ,0  ,0  ,39),--Cambodia
('20200228',152,null,1   ,0  ,0  ,39),--New Zealand

('20200228',107,null,250 ,5  ,0  ,39),--Italy
('20200228',76 ,null,20  ,0  ,0  ,39),--France
('20200228',82 ,null,5   ,0  ,0  ,39),--Germany
('20200228',200,null,13  ,0  ,0  ,39),--Spain
('20200228',225,null,3   ,0  ,0  ,39),--UK (United Kingdom)
('20200228',206,null,5   ,0  ,0  ,39),--Sweden
('20200228',207,null,5   ,0  ,0  ,39),--Switzerland
('20200228',13 ,null,2   ,0  ,0  ,39),--Austria
('20200228',160,null,3   ,0  ,0  ,39),--Norway
('20200228',85 ,null,2   ,0  ,0  ,39),--Greece
('20200228',106,null,1   ,0  ,0  ,39),--Israel
('20200228',51 ,null,0   ,0  ,0  ,39),--Croatia
('20200228',75 ,null,0   ,0  ,0  ,39),--Finland
('20200228',175,null,0   ,0  ,0  ,39),--Russia
('20200228',19 ,null,1   ,0  ,0  ,39),--Belarus
('20200228',123,null,1   ,0  ,0  ,39),--Lithuania
('20200228',150,null,1   ,0  ,0  ,39),--Netherlands
('20200228',158,null,0   ,0  ,0  ,39),--North Macedonia
('20200228',174,null,0   ,0  ,0  ,39),--Romania
('20200228',20 ,null,0   ,0  ,0  ,39),--Belgium
('20200228',57 ,null,0   ,0  ,0  ,39),--Denmark
('20200228',69 ,null,0   ,0  ,0  ,39),--Estonia
('20200228',81 ,null,0   ,0  ,0  ,39),--Georgia

('20200228',211,null,0   ,0  ,0  ,39),--Thailand
('20200228',100,null,0   ,0  ,0  ,39),--India
('20200228',149,null,0   ,0  ,0  ,39),--Nepal
('20200228',201,null,0   ,0  ,0  ,39),--Sri Lanka

('20200228',102,null,104 ,4  ,0  ,39),--Iran
('20200228',115,null,0   ,0  ,0  ,39),--Kuwait
('20200228',16 ,null,0   ,0  ,0  ,39),--Bahrain
('20200228',223,null,6   ,0  ,0  ,39),--UAE (United Arab Emirates)
('20200228',103,null,1   ,0  ,0  ,39),--Iraq
('20200228',161,null,2   ,0  ,0  ,39),--Oman
('20200228',119,null,0   ,0  ,0  ,39),--lebanon
('20200228',162,null,0   ,0  ,0  ,39),--Pakistan
('20200228',1  ,null,0   ,0  ,0  ,39),--Afghanistan
('20200228',65 ,null,0   ,0  ,0  ,39),--Egypt

('20200228',228,null,0   ,0  ,0  ,39),--USA
('20200228',37 ,null,0   ,0  ,0  ,39),--Canada
('20200228',28 ,null,0   ,0  ,0  ,39),--Brasil

('20200228',3  ,null,0   ,0  ,0  ,39),--Algeria
('20200228',155,null,1   ,0  ,0  ,39),--Nigeria

('20200228',58 ,null,0  ,0  ,0  ,39)--Diamond Princess




INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200229',45 ,null,433 ,47 ,0  ,40),--China
('20200229',181,null,813 ,4  ,0  ,40),--Republic of korea
('20200229',110,null,20  ,1  ,0  ,40),--Japan
('20200229',193,null,2   ,0  ,0  ,40),--Singapore
('20200229',12 ,null,1   ,0  ,0  ,40),--Australia
('20200229',128,null,0   ,0  ,0  ,40),--Malaysia
('20200229',233,null,0   ,0  ,0  ,40),--Vietnam
('20200229',169,null,0   ,0  ,0  ,40),--Philippines
('20200229',35 ,null,0   ,0  ,0  ,40),--Cambodia
('20200229',152,null,0   ,0  ,0  ,40),--New Zealand

('20200229',107,null,238 ,4  ,0  ,40),--Italy
('20200229',82 ,null,31  ,0  ,0  ,40),--Germany
('20200229',76 ,null,19  ,0  ,0  ,40),--France
('20200229',200,null,7   ,0  ,0  ,40),--Spain
('20200229',225,null,4   ,0  ,0  ,40),--UK (United Kingdom)
('20200229',206,null,5   ,0  ,0  ,40),--Sweden
('20200229',207,null,4   ,0  ,0  ,40),--Switzerland
('20200229',160,null,2   ,0  ,0  ,40),--Norway
('20200229',51 ,null,2   ,0  ,0  ,40),--Croatia
('20200229',106,null,2   ,0  ,0  ,40),--Israel
('20200229',13 ,null,1   ,0  ,0  ,40),--Austria
('20200229',174,null,2   ,0  ,0  ,40),--Romania
('20200229',85 ,null,0   ,0  ,0  ,40),--Greece
('20200229',57 ,null,1   ,0  ,0  ,40),--Denmark
('20200229',81 ,null,1   ,0  ,0  ,40),--Georgia
('20200229',150,null,1   ,0  ,0  ,40),--Netherlands
('20200229',75 ,null,0   ,0  ,0  ,40),--Finland
('20200229',175,null,0   ,0  ,0  ,40),--Russia
('20200229',186,null,1   ,0  ,0  ,40),--San Marino
('20200229',158,null,0   ,0  ,0  ,40),--North Macedonia
('20200229',69 ,null,0   ,0  ,0  ,40),--Estonia
('20200229',123,null,0   ,0  ,0  ,40),--Lithuania
('20200229',19 ,null,0   ,0  ,0  ,40),--Belarus
('20200229',20 ,null,0   ,0  ,0  ,40),--Belgium

('20200229',211,null,2   ,0  ,0  ,40),--Thailand
('20200229',100,null,0   ,0  ,0  ,40),--India
('20200229',149,null,0   ,0  ,0  ,40),--Nepal
('20200229',201,null,0   ,0  ,0  ,40),--Sri Lanka

('20200229',102,null,143 ,8  ,0  ,40),--Iran
('20200229',115,null,2   ,0  ,0  ,40),--Kuwait
('20200229',16 ,null,5   ,0  ,0  ,40),--Bahrain
('20200229',223,null,0   ,0  ,0  ,40),--UAE (United Arab Emirates)
('20200229',103,null,1   ,0  ,0  ,40),--Iraq
('20200229',161,null,0   ,0  ,0  ,40),--Oman
('20200229',119,null,0   ,0  ,0  ,40),--lebanon
('20200229',162,null,0   ,0  ,0  ,40),--Pakistan
('20200229',1  ,null,0   ,0  ,0  ,40),--Afghanistan
('20200229',65 ,null,0   ,0  ,0  ,40),--Egypt

('20200229',228,null,3   ,0  ,0  ,40),--USA
('20200229',37 ,null,3   ,0  ,0  ,40),--Canada
('20200229',137,null,2   ,0  ,0  ,40),--Mexico
('20200229',28 ,null,0   ,0  ,0  ,40),--Brasil

('20200229',3  ,null,0   ,0  ,0  ,40),--Algeria
('20200229',155,null,0   ,0  ,0  ,40),--Nigeria

('20200229',58 ,null,0  ,2  ,0  ,40)--Diamond Princess


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200301',45 ,null,579 ,35 ,0  ,41),--China
('20200301',181,null,586 ,1  ,0  ,41),--Republic of korea
('20200301',110,null,9   ,0  ,0  ,41),--Japan
('20200301',193,null,4   ,0  ,0  ,41),--Singapore
('20200301',12 ,null,1   ,0  ,0  ,41),--Australia
('20200301',128,null,0   ,0  ,0  ,41),--Malaysia
('20200301',233,null,0   ,0  ,0  ,41),--Vietnam
('20200301',169,null,0   ,0  ,0  ,41),--Philippines
('20200301',35 ,null,0   ,0  ,0  ,41),--Cambodia
('20200301',152,null,0   ,0  ,0  ,41),--New Zealand

('20200301',107,null,240 ,8  ,0  ,41),--Italy
('20200301',76 ,null,43  ,0  ,0  ,41),--France
('20200301',82 ,null,0  , 0  ,0  ,41),--Germany
('20200301',200,null,13  ,0  ,0  ,41),--Spain
('20200301',225,null,3   ,0  ,0  ,41),--UK (United Kingdom)
('20200301',206,null,1   ,0  ,0  ,41),--Sweden
('20200301',207,null,8   ,0  ,0  ,41),--Switzerland
('20200301',160,null,9   ,0  ,0  ,41),--Norway
('20200301',51 ,null,2   ,0  ,0  ,41),--Croatia
('20200301',106,null,2   ,0  ,0  ,41),--Israel
('20200301',13 ,null,5   ,0  ,0  ,41),--Austria
('20200301',174,null,0   ,0  ,0  ,41),--Romania
('20200301',85 ,null,0   ,0  ,0  ,41),--Greece
('20200301',57 ,null,1   ,0  ,0  ,41),--Denmark
('20200301',81 ,null,1   ,0  ,0  ,41),--Georgia
('20200301',150,null,5   ,0  ,0  ,41),--Netherlands
('20200301',75 ,null,0   ,0  ,0  ,41),--Finland
('20200301',175,null,0   ,0  ,0  ,41),--Russia
('20200301',186,null,0   ,0  ,0  ,41),--San Marino
('20200301',158,null,0   ,0  ,0  ,41),--North Macedonia
('20200301',69 ,null,0   ,0  ,0  ,41),--Estonia
('20200301',123,null,0   ,0  ,0  ,41),--Lithuania
('20200301',19 ,null,0   ,0  ,0  ,41),--Belarus
('20200301',20 ,null,0   ,0  ,0  ,41),--Belgium
('20200301',104,null,1   ,0  ,0  ,41),--Ireland
('20200301',140,null,0   ,0  ,0  ,41),--Monaco

('20200301',211,null,0   ,0  ,0  ,41),--Thailand
('20200301',100,null,0   ,0  ,0  ,41),--India
('20200301',149,null,0   ,0  ,0  ,41),--Nepal
('20200301',201,null,0   ,0  ,0  ,41),--Sri Lanka

('20200301',102,null,205 ,9  ,0  ,41),--Iran
('20200301',115,null,0   ,0  ,0  ,41),--Kuwait
('20200301',16 ,null,2   ,0  ,0  ,41),--Bahrain
('20200301',223,null,0   ,0  ,0  ,41),--UAE (United Arab Emirates)
('20200301',103,null,5   ,0  ,0  ,41),--Iraq
('20200301',161,null,0   ,0  ,0  ,41),--Oman
('20200301',119,null,0   ,0  ,0  ,41),--lebanon
('20200301',162,null,2   ,0  ,0  ,41),--Pakistan
('20200301',1  ,null,0   ,0  ,0  ,41),--Afghanistan
('20200301',65 ,null,0   ,0  ,0  ,41),--Egypt
('20200301',173,null,1   ,0  ,0  ,41),--Qatar
('20200301',14 ,null,3   ,0  ,0  ,41),--Azerbaijan

('20200301',228,null,0   ,0  ,0  ,41),--USA
('20200301',37 ,null,5   ,0  ,0  ,41),--Canada
('20200301',137,null,0   ,0  ,0  ,41),--Mexico
('20200301',28 ,null,1   ,0  ,0  ,41),--Brasil
('20200301',64 ,null,1   ,0  ,0  ,41),--Ecuador

('20200301',3  ,null,0   ,0  ,0  ,41),--Algeria
('20200301',155,null,0   ,0  ,0  ,41),--Nigeria

('20200301',58 ,null,0   ,0  ,0  ,41)--Diamond Princess




INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200302',45 ,null,206 ,42 ,0  ,42),--China
('20200302',181,null,476 ,4  ,0  ,42),--Republic of korea
('20200302',110,null,15  ,1  ,0  ,42),--Japan
('20200302',193,null,4   ,0  ,0  ,42),--Singapore
('20200302',12 ,null,2   ,1  ,0  ,42),--Australia
('20200302',128,null,0   ,0  ,0  ,42),--Malaysia
('20200302',233,null,0   ,0  ,0  ,42),--Vietnam
('20200302',169,null,0   ,0  ,0  ,42),--Philippines
('20200302',35 ,null,0   ,0  ,0  ,42),--Cambodia
('20200302',152,null,0   ,0  ,0  ,42),--New Zealand

('20200302',107,null,561 ,6  ,0  ,42),--Italy
('20200302',82 ,null,72  ,0  ,0  ,42),--Germany
('20200302',76 ,null,0   ,0  ,0  ,42),--France
('20200302',200,null,0   ,0  ,0  ,42),--Spain
('20200302',225,null,13  ,0  ,0  ,42),--UK (United Kingdom)
('20200302',207,null,6   ,0  ,0  ,42),--Switzerland
('20200302',160,null,4   ,0  ,0  ,42),--Norway
('20200302',206,null,1   ,0  ,0  ,42),--Sweden
('20200302',150,null,6   ,0  ,0  ,42),--Netherlands
('20200302',13 ,null,0   ,0  ,0  ,42),--Austria
('20200302',51 ,null,0   ,0  ,0  ,42),--Croatia
('20200302',85 ,null,4   ,0  ,0  ,42),--Greece
('20200302',106,null,0   ,0  ,0  ,42),--Israel
('20200302',75 ,null,4   ,0  ,0  ,42),--Finland
('20200302',57 ,null,1   ,0  ,0  ,42),--Denmark
('20200302',14 ,null,0   ,0  ,0  ,42),--Azerbaijan
('20200302',55 ,null,3   ,0  ,0  ,42),--Czechia
('20200302',81 ,null,0   ,0  ,0  ,42),--Georgia
('20200302',174,null,0   ,0  ,0  ,42),--Romania
('20200302',99 ,null,2   ,0  ,0  ,42),--Iceland
('20200302',175,null,0   ,0  ,0  ,42),--Russia
('20200302',10 ,null,1   ,0  ,0  ,42),--Armenia
('20200302',19 ,null,0   ,0  ,0  ,42),--Belarus
('20200302',20 ,null,0   ,0  ,0  ,42),--Belgium
('20200302',69 ,null,0   ,0  ,0  ,42),--Estonia
('20200302',104,null,0   ,0  ,0  ,42),--Ireland
('20200302',123,null,0   ,0  ,0  ,42),--Lithuania
('20200302',124,null,1   ,0  ,0  ,42),--Lithuania
('20200302',158,null,0   ,0  ,0  ,42),--North Macedonia
('20200302',186,null,0   ,0  ,0  ,42),--San Marino
('20200302',140,null,0   ,0  ,0  ,42),--Monaco

('20200302',211,null,0   ,1  ,0  ,42),--Thailand
('20200302',100,null,0   ,0  ,0  ,42),--India
('20200302',101,null,2   ,0  ,0  ,42),--Indonesia
('20200302',149,null,0   ,0  ,0  ,42),--Nepal
('20200302',201,null,0   ,0  ,0  ,42),--Sri Lanka

('20200302',102,null,385 ,11  ,0  ,42),--Iran
('20200302',115,null,11  ,0  ,0  ,42),--Kuwait
('20200302',16 ,null,7   ,0  ,0  ,42),--Bahrain
('20200302',223,null,2   ,0  ,0  ,42),--UAE (United Arab Emirates)
('20200302',103,null,6   ,0  ,0  ,42),--Iraq
('20200302',119,null,1   ,0  ,0  ,42),--lebanon
('20200302',161,null,0   ,0  ,0  ,42),--Oman
('20200302',162,null,0   ,0  ,0  ,42),--Pakistan
('20200302',173,null,2   ,0  ,0  ,42),--Qatar
('20200302',65 ,null,1   ,0  ,0  ,42),--Egypt
('20200302',1  ,null,0   ,0  ,0  ,42),--Afghanistan

('20200302',228,null,2   ,0  ,0  ,42),--USA
('20200302',37 ,null,0   ,0  ,0  ,42),--Canada
('20200302',137,null,3   ,0  ,0  ,42),--Mexico
('20200302',28 ,null,0   ,0  ,0  ,42),--Brasil
('20200302',62 ,null,1   ,0  ,0  ,42),--Dominican Republic
('20200302',64 ,null,0   ,0  ,0  ,42),--Ecuador


('20200302',3  ,null,0   ,0  ,0  ,42),--Algeria
('20200302',155,null,0   ,0  ,0  ,42),--Nigeria

('20200302',58 ,null,1   ,0  ,0  ,42)--Diamond Princess


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200303',45 ,null,130 ,31 ,0  ,43),--China
('20200303',181,null,600 ,6  ,0  ,43),--Republic of korea
('20200303',110,null,14  ,0  ,0  ,43),--Japan
('20200303',193,null,2   ,0  ,0  ,43),--Singapore
('20200303',12 ,null,6   ,0  ,0  ,43),--Australia
('20200303',128,null,5   ,0  ,0  ,43),--Malaysia
('20200303',233,null,0   ,0  ,0  ,43),--Vietnam
('20200303',169,null,0   ,0  ,0  ,43),--Philippines
('20200303',152,null,1   ,0  ,0  ,43),--New Zealand
('20200303',35 ,null,0   ,0  ,0  ,43),--Cambodia

('20200303',107,null,347 ,17 ,0  ,43),--Italy
('20200303',76 ,null,91  ,1  ,0  ,43),--France
('20200303',82 ,null,28  ,0  ,0  ,43),--Germany
('20200303',200,null,69  ,0  ,0  ,43),--Spain
('20200303',225,null,3   ,0  ,0  ,43),--UK (United Kingdom)
('20200303',207,null,6   ,0  ,0  ,43),--Switzerland
('20200303',160,null,6   ,0  ,0  ,43),--Norway
('20200303',13 ,null,8   ,0  ,0  ,43),--Austria
('20200303',150,null,5   ,0  ,0  ,43),--Netherlands
('20200303',206,null,1   ,0  ,0  ,43),--Sweden
('20200303',106,null,3   ,0  ,0  ,43),--Israel
('20200303',51 ,null,2   ,0  ,0  ,43),--Croatia
('20200303',99 ,null,7   ,0  ,0  ,43),--Iceland
('20200303',186,null,7   ,0  ,0  ,43),--San Marino
('20200303',20 ,null,7   ,0  ,0  ,43),--Belgium
('20200303',75 ,null,1   ,0  ,0  ,43),--Finland
('20200303',85 ,null,0   ,0  ,0  ,43),--Greece
('20200303',57 ,null,1   ,0  ,0  ,43),--Denmark
('20200303',14 ,null,0   ,0  ,0  ,43),--Azerbaijan
('20200303',55 ,null,0   ,0  ,0  ,43),--Czechia
('20200303',81 ,null,0   ,0  ,0  ,43),--Georgia
('20200303',174,null,0   ,0  ,0  ,43),--Romania
('20200303',175,null,1   ,0  ,0  ,43),--Russia
('20200303',171,null,2   ,0  ,0  ,43),--Portugal
('20200303',5  ,null,1   ,0  ,0  ,43),--Andorra
('20200303',10 ,null,0   ,0  ,0  ,43),--Armenia
('20200303',19 ,null,0   ,0  ,0  ,43),--Belarus
('20200303',69 ,null,0   ,0  ,0  ,43),--Estonia
('20200303',104,null,0   ,0  ,0  ,43),--Ireland
('20200303',118,null,1   ,0  ,0  ,43),--Latvia
('20200303',123,null,0   ,0  ,0  ,43),--Lithuania
('20200303',124,null,0   ,0  ,0  ,43),--Luxembourg
('20200303',140,null,0   ,0  ,0  ,43),--Monaco
('20200303',158,null,0   ,0  ,0  ,43),--North Macedonia

('20200303',211,null,1   ,0  ,0  ,43),--Thailand
('20200303',100,null,2   ,0  ,0  ,43),--India
('20200303',101,null,0   ,0  ,0  ,43),--Indonesia
('20200303',149,null,0   ,0  ,0  ,43),--Nepal
('20200303',201,null,0   ,0  ,0  ,43),--Sri Lanka

('20200303',102,null,523 ,12 ,0  ,43),--Iran
('20200303',115,null,0   ,0  ,0  ,43),--Kuwait
('20200303',16 ,null,2   ,0  ,0  ,43),--Bahrain
('20200303',103,null,7   ,0  ,0  ,43),--Iraq
('20200303',223,null,0   ,0  ,0  ,43),--UAE (United Arab Emirates)
('20200303',119,null,3   ,0  ,0  ,43),--lebanon
('20200303',173,null,4   ,0  ,0  ,43),--Qatar
('20200303',161,null,0   ,0  ,0  ,43),--Oman
('20200303',162,null,1   ,0  ,0  ,43),--Pakistan
('20200303',65 ,null,0   ,0  ,0  ,43),--Egypt
('20200303',1  ,null,0   ,0  ,0  ,43),--Afghanistan
('20200303',111,null,1   ,0  ,0  ,43),--Jordan
('20200303',144,null,1   ,0  ,0  ,43),--Morocco
('20200303',188,null,1   ,0  ,0  ,43),--Saudi Arabia
('20200303',217,null,1   ,0  ,0  ,43),--Tunisia

('20200303',228,null,2   ,2  ,0  ,43),--USA
('20200303',37 ,null,8   ,0  ,0  ,43),--Canada
('20200303',64 ,null,5   ,0  ,0  ,43),--Ecuador
('20200303',137,null,0   ,0  ,0  ,43),--Mexico
('20200303',28 ,null,0   ,0  ,0  ,43),--Brasil
('20200303',62 ,null,0   ,0  ,0  ,43),--Dominican Republic


('20200303',3  ,null,4   ,0  ,0  ,43),--Algeria
('20200303',155,null,0   ,0  ,0  ,43),--Nigeria
('20200303',189,null,1   ,0  ,0  ,43),--Senegal 

('20200303',58 ,null,0   ,0  ,0  ,43)--Diamond Princess



INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200304',45 ,null,120 ,38 ,0  ,44),--China
('20200304',181,null,516 ,4  ,0  ,44),--Republic of korea
('20200304',110,null,16  ,0  ,0  ,44),--Japan
('20200304',193,null,2   ,0  ,0  ,44),--Singapore
('20200304',128,null,21  ,0  ,0  ,44),--Malaysia
('20200304',12 ,null,10  ,0  ,0  ,44),--Australia
('20200304',233,null,0   ,0  ,0  ,44),--Vietnam
('20200304',169,null,0   ,0  ,0  ,44),--Philippines
('20200304',152,null,0   ,0  ,0  ,44),--New Zealand
('20200304',35 ,null,0   ,0  ,0  ,44),--Cambodia

('20200304',107,null,466 ,28 ,0  ,44),--Italy
('20200304',76 ,null,21  ,1  ,0  ,44),--France
('20200304',82 ,null,39  ,0  ,0  ,44),--Germany
('20200304',200,null,37  ,0  ,0  ,44),--Spain
('20200304',225,null,12  ,0  ,0  ,44),--UK (United Kingdom)
('20200304',207,null,7   ,0  ,0  ,44),--Switzerland
('20200304',160,null,7   ,0  ,0  ,44),--Norway
('20200304',150,null,10  ,0  ,0  ,44),--Netherlands
('20200304',13 ,null,6   ,0  ,0  ,44),--Austria
('20200304',206,null,9   ,0  ,0  ,44),--Sweden
('20200304',99 ,null,7   ,0  ,0  ,44),--Iceland
('20200304',106,null,2   ,0  ,0  ,44),--Israel
('20200304',51 ,null,0   ,0  ,0  ,44),--Croatia
('20200304',20 ,null,0   ,0  ,0  ,44),--Belgium
('20200304',57 ,null,3   ,0  ,0  ,44),--Denmark
('20200304',186,null,0   ,0  ,0  ,44),--San Marino
('20200304',75 ,null,0   ,0  ,0  ,44),--Finland
('20200304',85 ,null,0   ,0  ,0  ,44),--Greece
('20200304',55 ,null,2   ,0  ,0  ,44),--Czechia
('20200304',174,null,1   ,0  ,0  ,44),--Romania
('20200304',14 ,null,0   ,0  ,0  ,44),--Azerbaijan
('20200304',81 ,null,0   ,0  ,0  ,44),--Georgia
('20200304',175,null,0   ,0  ,0  ,44),--Russia
('20200304',69 ,null,1   ,0  ,0  ,44),--Estonia
('20200304',104,null,1   ,0  ,0  ,44),--Ireland
('20200304',171,null,0   ,0  ,0  ,44),--Portugal
('20200304',5  ,null,0   ,0  ,0  ,44),--Andorra
('20200304',10 ,null,0   ,0  ,0  ,44),--Armenia
('20200304',19 ,null,0   ,0  ,0  ,44),--Belarus
('20200304',118,null,0   ,0  ,0  ,44),--Latvia
('20200304',123,null,0   ,0  ,0  ,44),--Lithuania
('20200304',124,null,0   ,0  ,0  ,44),--Luxembourg
('20200304',140,null,0   ,0  ,0  ,44),--Monaco
('20200304',158,null,0   ,0  ,0  ,44),--North Macedonia
('20200304',170,null,1   ,0  ,0  ,44),--Poland
('20200304',226,null,1   ,0  ,0  ,44),--Ukraine

('20200304',211,null,0   ,0  ,0  ,44),--Thailand
('20200304',100,null,1   ,0  ,0  ,44),--India
('20200304',101,null,0   ,0  ,0  ,44),--Indonesia
('20200304',149,null,0   ,0  ,0  ,44),--Nepal
('20200304',201,null,0   ,0  ,0  ,44),--Sri Lanka

('20200304',102,null,835 ,11 ,0  ,44),--Iran
('20200304',115,null,0   ,0  ,0  ,44),--Kuwait
('20200304',16 ,null,0   ,0  ,0  ,44),--Bahrain
('20200304',103,null,5   ,0  ,0  ,44),--Iraq
('20200304',223,null,6   ,0  ,0  ,44),--UAE (United Arab Emirates)
('20200304',119,null,0   ,0  ,0  ,44),--lebanon
('20200304',161,null,6   ,0  ,0  ,44),--Oman
('20200304',173,null,1   ,0  ,0  ,44),--Qatar
('20200304',162,null,0   ,0  ,0  ,44),--Pakistan
('20200304',65 ,null,0   ,0  ,0  ,44),--Egypt
('20200304',1  ,null,0   ,0  ,0  ,44),--Afghanistan
('20200304',111,null,0   ,0  ,0  ,44),--Jordan
('20200304',144,null,0   ,0  ,0  ,44),--Morocco
('20200304',188,null,0   ,0  ,0  ,44),--Saudi Arabia
('20200304',217,null,0   ,0  ,0  ,44),--Tunisia

('20200304',228,null,44  ,4  ,0  ,44),--USA
('20200304',37 ,null,3   ,0  ,0  ,44),--Canada
('20200304',64 ,null,1   ,0  ,0  ,44),--Ecuador
('20200304',137,null,0   ,0  ,0  ,44),--Mexico
('20200304',28 ,null,0   ,0  ,0  ,44),--Brasil
('20200304',9  ,null,1   ,0  ,0  ,44),--Argentina
('20200304',44 ,null,1   ,0  ,0  ,44),--Chile
('20200304',62 ,null,0   ,0  ,0  ,44),--Dominican Republic

('20200304',3  ,null,0   ,0  ,0  ,44),--Algeria
('20200304',155,null,0   ,0  ,0  ,44),--Nigeria
('20200304',189,null,0   ,0  ,0  ,44),--Senegal 

('20200304',58 ,null,0   ,0  ,0  ,44)--Diamond Princess





INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200305',45 ,null,136 ,31 ,0  ,45),--China
('20200305',181,null,438 ,3  ,0  ,45),--Republic of korea
('20200305',110,null,33  ,0  ,0  ,45),--Japan
('20200305',193,null,0   ,0  ,0  ,45),--Singapore
('20200305',12 ,null,14  ,2  ,0  ,45),--Australia
('20200305',128,null,0   ,0  ,0  ,45),--Malaysia
('20200305',233,null,0   ,0  ,0  ,45),--Vietnam
('20200305',169,null,0   ,0  ,0  ,45),--Philippines
('20200305',152,null,0   ,0  ,0  ,45),--New Zealand
('20200305',35 ,null,0   ,0  ,0  ,45),--Cambodia

('20200305',107,null,587 ,27 ,0  ,45),--Italy
('20200305',76 ,null,70  ,0  ,0  ,45),--France
('20200305',82 ,null,66  ,0  ,0  ,45),--Germany
('20200305',200,null,47  ,1  ,0  ,45),--Spain
('20200305',225,null,41  ,0  ,0  ,45),--UK (United Kingdom)
('20200305',207,null,19  ,0  ,0  ,45),--Switzerland
('20200305',160,null,24  ,0  ,0  ,45),--Norway
('20200305',150,null,10  ,0  ,0  ,45),--Netherlands
('20200305',13 ,null,13  ,0  ,0  ,45),--Austria
('20200305',206,null,11  ,0  ,0  ,45),--Sweden
('20200305',99 ,null,10  ,0  ,0  ,45),--Iceland
('20200305',20 ,null,15  ,0  ,0  ,45),--Belgium
('20200305',186,null,8   ,0  ,0  ,45),--San Marino
('20200305',106,null,3   ,0  ,0  ,45),--Israel
('20200305',57 ,null,2   ,0  ,0  ,45),--Denmark
('20200305',51 ,null,0   ,0  ,0  ,45),--Croatia
('20200305',85 ,null,2   ,0  ,0  ,45),--Greece
('20200305',75 ,null,1   ,0  ,0  ,45),--Finland
('20200305',171,null,5   ,0  ,0  ,45),--Portugal
('20200305',19 ,null,5   ,0  ,0  ,45),--Belarus
('20200305',55 ,null,0   ,0  ,0  ,45),--Czechia
('20200305',174,null,0   ,0  ,0  ,45),--Romania
('20200305',14 ,null,0   ,0  ,0  ,45),--Azerbaijan
('20200305',81 ,null,0   ,0  ,0  ,45),--Georgia
('20200305',175,null,0   ,0  ,0  ,45),--Russia
('20200305',26 ,null,2   ,0  ,0  ,45),--Bosnia and Herzegovina
('20200305',69 ,null,0   ,0  ,0  ,45),--Estonia
('20200305',98 ,null,2   ,0  ,0  ,45),--Hungary
('20200305',104,null,0   ,0  ,0  ,45),--Ireland
('20200305',5  ,null,0   ,0  ,0  ,45),--Andorra
('20200305',10 ,null,0   ,0  ,0  ,45),--Armenia
('20200305',118,null,0   ,0  ,0  ,45),--Latvia
('20200305',123,null,0   ,0  ,0  ,45),--Lithuania
('20200305',124,null,0   ,0  ,0  ,45),--Luxembourg
('20200305',140,null,1   ,0  ,0  ,45),--Monaco
('20200305',158,null,0   ,0  ,0  ,45),--North Macedonia
('20200305',170,null,0   ,0  ,0  ,45),--Poland
('20200305',196,null,1   ,0  ,0  ,45),--Slovenia
('20200305',226,null,0   ,0  ,0  ,45),--Ukraine
('20200305',122,null,1   ,0  ,0  ,45),--Liechtenstein

('20200305',84 ,null,1   ,0  ,0  ,45),--Gibraltar

('20200305',211,null,4   ,0  ,0  ,45),--Thailand
('20200305',100,null,23  ,0  ,0  ,45),--India
('20200305',101,null,0   ,0  ,0  ,45),--Indonesia
('20200305',149,null,0   ,0  ,0  ,45),--Nepal
('20200305',201,null,0   ,0  ,0  ,45),--Sri Lanka

('20200305',102,null,586 ,15 ,0  ,45),--Iran
('20200305',115,null,2   ,0  ,0  ,45),--Kuwait
('20200305',16 ,null,0   ,0  ,0  ,45),--Bahrain
('20200305',103,null,5   ,2  ,0  ,45),--Iraq
('20200305',223,null,0   ,0  ,0  ,45),--UAE (United Arab Emirates)
('20200305',161,null,3   ,0  ,0  ,45),--Oman
('20200305',119,null,0   ,0  ,0  ,45),--lebanon
('20200305',173,null,0   ,0  ,0  ,45),--Qatar
('20200305',162,null,0   ,0  ,0  ,45),--Pakistan
('20200305',65 ,null,0   ,0  ,0  ,45),--Egypt
('20200305',144,null,1   ,0  ,0  ,45),--Morocco
('20200305',188,null,1   ,0  ,0  ,45),--Saudi Arabia
('20200305',1  ,null,0   ,0  ,0  ,45),--Afghanistan
('20200305',111,null,0   ,0  ,0  ,45),--Jordan
('20200305',217,null,0   ,0  ,0  ,45),--Tunisia

('20200305',164,null,4   ,0  ,0  ,45),--Palestine

('20200305',228,null,21  ,3  ,0  ,45),--USA
('20200305',37 ,null,0   ,0  ,0  ,45),--Canada
('20200305',64 ,null,0   ,0  ,0  ,45),--Ecuador
('20200305',137,null,0   ,0  ,0  ,45),--Mexico
('20200305',28 ,null,1   ,0  ,0  ,45),--Brasil
('20200305',9  ,null,0   ,0  ,0  ,45),--Argentina
('20200305',44 ,null,0   ,0  ,0  ,45),--Chile
('20200305',62 ,null,0   ,0  ,0  ,45),--Dominican Republic

('20200305',183 ,null,2   ,0  ,0  ,45),--Saint Martin
('20200305',178 ,null,1   ,0  ,0  ,45),--Saint Barthelemy

('20200305',3  ,null,7   ,0  ,0  ,45),--Algeria
('20200305',189,null,3   ,0  ,0  ,45),--Senegal 
('20200305',155,null,0   ,0  ,0  ,45),--Nigeria

('20200305',58 ,null,0   ,0  ,0  ,45)--Diamond Princess



INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200306',45 ,null,146 ,30 ,0  ,46),--China
('20200306',181,null,518 ,7  ,0  ,46),--Republic of korea
('20200306',110,null,32  ,0  ,0  ,46),--Japan
('20200306',193,null,7   ,0  ,0  ,46),--Singapore
('20200306',12 ,null,0   ,0  ,0  ,46),--Australia
('20200306',128,null,5   ,0  ,0  ,46),--Malaysia
('20200306',233,null,0   ,0  ,0  ,46),--Vietnam
('20200306',169,null,2   ,0  ,0  ,46),--Philippines
('20200306',152,null,2   ,0  ,0  ,46),--New Zealand
('20200306',35 ,null,0   ,0  ,0  ,46),--Cambodia

('20200306',107,null,769 ,41 ,0  ,46),--Italy
('20200306',76 ,null,138 ,2  ,0  ,46),--France
('20200306',82 ,null,272 ,0  ,0  ,46),--Germany
('20200306',200,null,59  ,2  ,0  ,46),--Spain
('20200306',225,null,30  ,0  ,0  ,46),--UK (United Kingdom)
('20200306',207,null,30  ,1  ,0  ,46),--Switzerland
('20200306',160,null,30  ,0  ,0  ,46),--Norway
('20200306',150,null,44  ,0  ,0  ,46),--Netherlands
('20200306',13 ,null,10  ,0  ,0  ,46),--Austria
('20200306',206,null,26  ,0  ,0  ,46),--Sweden
('20200306',99 ,null,0   ,0  ,0  ,46),--Iceland
('20200306',20 ,null,27  ,0  ,0  ,46),--Belgium
('20200306',186,null,5   ,0  ,0  ,46),--San Marino
('20200306',106,null,0   ,0  ,0  ,46),--Israel
('20200306',57 ,null,8   ,0  ,0  ,46),--Denmark
('20200306',51 ,null,1   ,0  ,0  ,46),--Croatia
('20200306',85 ,null,23  ,0  ,0  ,46),--Greece
('20200306',75 ,null,5   ,0  ,0  ,46),--Finland
('20200306',171,null,2   ,0  ,0  ,46),--Portugal
('20200306',19 ,null,0   ,0  ,0  ,46),--Belarus
('20200306',55 ,null,7   ,0  ,0  ,46),--Czechia
('20200306',174,null,2   ,0  ,0  ,46),--Romania
('20200306',14 ,null,0   ,0  ,0  ,46),--Azerbaijan
('20200306',81 ,null,6   ,0  ,0  ,46),--Georgia
('20200306',175,null,1   ,0  ,0  ,46),--Russia
('20200306',26 ,null,0   ,0  ,0  ,46),--Bosnia and Herzegovina
('20200306',69 ,null,1   ,0  ,0  ,46),--Estonia
('20200306',98 ,null,0   ,0  ,0  ,46),--Hungary
('20200306',104,null,12  ,0  ,0  ,46),--Ireland
('20200306',5  ,null,0   ,0  ,0  ,46),--Andorra
('20200306',10 ,null,0   ,0  ,0  ,46),--Armenia
('20200306',118,null,0   ,0  ,0  ,46),--Latvia
('20200306',123,null,0   ,0  ,0  ,46),--Lithuania
('20200306',124,null,0   ,0  ,0  ,46),--Luxembourg
('20200306',140,null,0   ,0  ,0  ,46),--Monaco
('20200306',158,null,0   ,0  ,0  ,46),--North Macedonia
('20200306',170,null,0   ,0  ,0  ,46),--Poland
('20200306',196,null,5   ,0  ,0  ,46),--Slovenia
('20200306',226,null,0   ,0  ,0  ,46),--Ukraine
('20200306',122,null,0   ,0  ,0  ,46),--Liechtenstein
('20200306',190,null,1   ,0  ,0  ,46),--Serbia

('20200306',84 ,null,0   ,0  ,0  ,46),--Gibraltar

('20200306',211,null,0   ,0  ,0  ,46),--Thailand
('20200306',100,null,1   ,0  ,0  ,46),--India
('20200306',101,null,0   ,0  ,0  ,46),--Indonesia
('20200306',149,null,0   ,0  ,0  ,46),--Nepal
('20200306',201,null,0   ,0  ,0  ,46),--Sri Lanka
('20200306',24 ,null,1   ,0  ,0  ,46),--Bhutan

('20200306',102,null,591 ,15 ,0  ,46),--Iran
('20200306',115,null,0   ,0  ,0  ,46),--Kuwait
('20200306',16 ,null,0   ,0  ,0  ,46),--Bahrain
('20200306',103,null,0   ,0  ,0  ,46),--Iraq
('20200306',223,null,0   ,0  ,0  ,46),--UAE (United Arab Emirates)
('20200306',161,null,1   ,0  ,0  ,46),--Oman
('20200306',119,null,3   ,0  ,0  ,46),--lebanon
('20200306',173,null,0   ,0  ,0  ,46),--Qatar
('20200306',162,null,0   ,0  ,0  ,46),--Pakistan
('20200306',65 ,null,1   ,0  ,0  ,46),--Egypt
('20200306',144,null,0   ,0  ,0  ,46),--Morocco
('20200306',188,null,6   ,0  ,0  ,46),--Saudi Arabia
('20200306',1  ,null,0   ,0  ,0  ,46),--Afghanistan
('20200306',111,null,0   ,0  ,0  ,46),--Jordan
('20200306',217,null,0   ,0  ,0  ,46),--Tunisia

('20200306',164,null,3   ,0  ,0  ,46),--Palestine

('20200306',228,null,19  ,1  ,0  ,46),--USA
('20200306',37 ,null,15   ,0  ,0  ,46),--Canada
('20200306',64 ,null,6   ,0  ,0  ,46),--Ecuador
('20200306',137,null,0   ,0  ,0  ,46),--Mexico
('20200306',28 ,null,4   ,0  ,0  ,46),--Brasil
('20200306',9  ,null,0   ,0  ,0  ,46),--Argentina
('20200306',44 ,null,0   ,0  ,0  ,46),--Chile
('20200306',62 ,null,0   ,0  ,0  ,46),--Dominican Republic

('20200306',183 ,null,0   ,0  ,0  ,46),--Saint Martin
('20200306',178 ,null,0   ,0  ,0  ,46),--Saint Barthelemy

('20200306',3  ,null,0   ,0  ,0  ,46),--Algeria
('20200306',189,null,0   ,0  ,0  ,46),--Senegal 
('20200306',155,null,0   ,0  ,0  ,46),--Nigeria
('20200306',36, null,1   ,0  ,0  ,46),--Cameroon
('20200306',199,null,1   ,0  ,0  ,46),--South Africa

('20200306',58 ,null,-14   ,0  ,0  ,46)--Diamond Princess




INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200307',45 ,null,102 ,28 ,0  ,47),--China
('20200307',181,null,483 ,2  ,0  ,47),--Republic of korea
('20200307',110,null,59  ,0  ,0  ,47),--Japan
('20200307',193,null,13  ,0  ,0  ,47),--Singapore
('20200307',12 ,null,5   ,0  ,0  ,47),--Australia
('20200307',128,null,28  ,0  ,0  ,47),--Malaysia
('20200307',233,null,1   ,0  ,0  ,47),--Vietnam
('20200307',169,null,0   ,0  ,0  ,47),--Philippines
('20200307',152,null,1   ,0  ,0  ,47),--New Zealand
('20200307',35 ,null,0   ,0  ,0  ,47),--Cambodia

('20200307',107,null,778 ,49 ,0  ,47),--Italy
('20200307',76 ,null,193 ,3  ,0  ,47),--France
('20200307',82 ,null,105 ,0  ,0  ,47),--Germany
('20200307',200,null,117 ,2  ,0  ,47),--Spain
('20200307',225,null,49  ,1  ,0  ,47),--UK (United Kingdom)
('20200307',207,null,0   ,0  ,0  ,47),--Switzerland
('20200307',160,null,27  ,0  ,0  ,47),--Norway
('20200307',150,null,46  ,1  ,0  ,47),--Netherlands
('20200307',13 ,null,19  ,0  ,0  ,47),--Austria
('20200307',206,null,76  ,0  ,0  ,47),--Sweden
('20200307',99 ,null,19  ,0  ,0  ,47),--Iceland
('20200307',20 ,null,59  ,0  ,0  ,47),--Belgium
('20200307',186,null,3   ,0  ,0  ,47),--San Marino
('20200307',106,null,4   ,0  ,0  ,47),--Israel
('20200307',57 ,null,5   ,0  ,0  ,47),--Denmark
('20200307',51 ,null,1   ,0  ,0  ,47),--Croatia
('20200307',85 ,null,0   ,0  ,0  ,47),--Greece
('20200307',75 ,null,7   ,0  ,0  ,47),--Finland
('20200307',171,null,4   ,0  ,0  ,47),--Portugal
('20200307',19 ,null,0   ,0  ,0  ,47),--Belarus
('20200307',55 ,null,0   ,0  ,0  ,47),--Czechia
('20200307',174,null,1   ,0  ,0  ,47),--Romania
('20200307',14 ,null,6   ,0  ,0  ,47),--Azerbaijan
('20200307',81 ,null,0   ,0  ,0  ,47),--Georgia
('20200307',175,null,3   ,0  ,0  ,47),--Russia
('20200307',26 ,null,0   ,0  ,0  ,47),--Bosnia and Herzegovina
('20200307',69 ,null,7   ,0  ,0  ,47),--Estonia
('20200307',98 ,null,3   ,0  ,0  ,47),--Hungary
('20200307',104,null,4   ,0  ,0  ,47),--Ireland
('20200307',5  ,null,0   ,0  ,0  ,47),--Andorra
('20200307',10 ,null,0   ,0  ,0  ,47),--Armenia
('20200307',118,null,0   ,0  ,0  ,47),--Latvia
('20200307',123,null,0   ,0  ,0  ,47),--Lithuania
('20200307',124,null,1   ,0  ,0  ,47),--Luxembourg
('20200307',140,null,0   ,0  ,0  ,47),--Monaco
('20200307',158,null,2   ,0  ,0  ,47),--North Macedonia
('20200307',170,null,4   ,0  ,0  ,47),--Poland
('20200307',196,null,3   ,0  ,0  ,47),--Slovenia
('20200307',226,null,0   ,0  ,0  ,47),--Ukraine
('20200307',122,null,0   ,0  ,0  ,47),--Liechtenstein
('20200307',95 ,null,1   ,0  ,0  ,47),--Holy see
('20200307',195,null,1   ,0  ,0  ,47),--Slovakia

('20200307',84 ,null,0   ,0  ,0  ,47),--Gibraltar

('20200307',211,null,1   ,0  ,0  ,47),--Thailand
('20200307',100,null,1   ,0  ,0  ,47),--India
('20200307',101,null,0   ,0  ,0  ,47),--Indonesia
('20200307',149,null,0   ,0  ,0  ,47),--Nepal
('20200307',201,null,0   ,0  ,0  ,47),--Sri Lanka

('20200307',102,null,1234,17 ,0  ,47),--Iran
('20200307',115,null,0   ,0  ,0  ,47),--Kuwait
('20200307',16 ,null,0   ,0  ,0  ,47),--Bahrain
('20200307',103,null,8   ,2  ,0  ,47),--Iraq
('20200307',223,null,18  ,0  ,0  ,47),--UAE (United Arab Emirates)
('20200307',161,null,0   ,0  ,0  ,47),--Oman
('20200307',119,null,6   ,0  ,0  ,47),--lebanon
('20200307',173,null,3   ,0  ,0  ,47),--Qatar
('20200307',162,null,0   ,0  ,0  ,47),--Pakistan
('20200307',65 ,null,0   ,0  ,0  ,47),--Egypt
('20200307',144,null,0   ,0  ,0  ,47),--Morocco
('20200307',188,null,0   ,0  ,0  ,47),--Saudi Arabia
('20200307',1  ,null,0   ,0  ,0  ,47),--Afghanistan
('20200307',111,null,0   ,0  ,0  ,47),--Jordan
('20200307',217,null,0   ,0  ,0  ,47),--Tunisia

('20200307',164,null,9   ,0  ,0  ,47),--Palestine

('20200307',228,null,65  ,1  ,0  ,47),--USA
('20200307',37 ,null,6   ,0  ,0  ,47),--Canada
('20200307',64 ,null,1   ,0  ,0  ,47),--Ecuador
('20200307',137,null,0   ,0  ,0  ,47),--Mexico
('20200307',28 ,null,6   ,0  ,0  ,47),--Brasil
('20200307',9  ,null,1   ,0  ,0  ,47),--Argentina
('20200307',44 ,null,4   ,0  ,0  ,47),--Chile
('20200307',62 ,null,0   ,0  ,0  ,47),--Dominican Republic
('20200307',46 ,null,1   ,0  ,0  ,47),--Colombia
('20200307',168,null,1   ,0  ,0  ,47),--Peru

('20200307',183 ,null,0   ,0  ,0  ,47),--Saint Martin
('20200307',178 ,null,0   ,0  ,0  ,47),--Saint Barthelemy

('20200307',3  ,null,5   ,0  ,0  ,47),--Algeria
('20200307',189,null,0   ,0  ,0  ,47),--Senegal 
('20200307',155,null,0   ,0  ,0  ,47),--Nigeria
('20200307',36, null,1   ,0  ,0  ,47),--Cameroon
('20200307',199,null,0   ,0  ,0  ,47),--South Africa
('20200307',213,null,1   ,0  ,0  ,47),--Togo

('20200307',58 ,null,0   ,0  ,0  ,47)--Diamond Princess




INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200308',45 ,null,46  ,27  ,0  ,48),--China
('20200308',181,null,367 ,6  ,0  ,48),--Republic of korea
('20200308',110,null,48  ,0  ,0  ,48),--Japan
('20200308',193,null,8   ,0  ,0  ,48),--Singapore
('20200308',12 ,null,12   ,0  ,0  ,48),--Australia
('20200308',128,null,10  ,1  ,0  ,48),--Malaysia
('20200308',233,null,4   ,0  ,0  ,48),--Vietnam
('20200308',169,null,1   ,0  ,0  ,48),--Philippines
('20200308',152,null,0   ,0  ,0  ,48),--New Zealand
('20200308',35 ,null,1   ,0  ,0  ,48),--Cambodia

('20200308',107,null,1247,37 ,0  ,48),--Italy
('20200308',76 ,null,93  ,1  ,0  ,48),--France
('20200308',82 ,null,156 ,0  ,0  ,48),--Germany
('20200308',200,null,56  ,0  ,0  ,48),--Spain
('20200308',225,null,43  ,1  ,0  ,48),--UK (United Kingdom)
('20200308',207,null,55  ,1  ,0  ,48),--Switzerland
('20200308',160,null,34  ,0  ,0  ,48),--Norway
('20200308',150,null,60  ,0  ,0  ,48),--Netherlands
('20200308',13 ,null,38  ,0  ,0  ,48),--Austria
('20200308',206,null,24  ,0  ,0  ,48),--Sweden
('20200308',99 ,null,0   ,0  ,0  ,48),--Iceland
('20200308',20 ,null,60  ,0  ,0  ,48),--Belgium
('20200308',186,null,3   ,1  ,0  ,48),--San Marino
('20200308',106,null,6   ,0  ,0  ,48),--Israel
('20200308',57 ,null,8   ,0  ,0  ,48),--Denmark
('20200308',51 ,null,0   ,0  ,0  ,48),--Croatia
('20200308',85 ,null,34  ,0  ,0  ,48),--Greece
('20200308',75 ,null,0   ,0  ,0  ,48),--Finland
('20200308',171,null,8   ,0  ,0  ,48),--Portugal
('20200308',19 ,null,0   ,0  ,0  ,48),--Belarus
('20200308',55 ,null,14  ,0  ,0  ,48),--Czechia
('20200308',174,null,6   ,0  ,0  ,48),--Romania
('20200308',14 ,null,0   ,0  ,0  ,48),--Azerbaijan
('20200308',81 ,null,3   ,0  ,0  ,48),--Georgia
('20200308',175,null,0   ,0  ,0  ,48),--Russia
('20200308',26 ,null,0   ,0  ,0  ,48),--Bosnia and Herzegovina
('20200308',69 ,null,0   ,0  ,0  ,48),--Estonia
('20200308',98 ,null,3   ,0  ,0  ,48),--Hungary
('20200308',104,null,1   ,0  ,0  ,48),--Ireland
('20200308',5  ,null,0   ,0  ,0  ,48),--Andorra
('20200308',10 ,null,0   ,0  ,0  ,48),--Armenia
('20200308',118,null,0   ,0  ,0  ,48),--Latvia
('20200308',123,null,0   ,0  ,0  ,48),--Lithuania
('20200308',124,null,0   ,0  ,0  ,48),--Luxembourg
('20200308',140,null,0   ,0  ,0  ,48),--Monaco
('20200308',158,null,0   ,0  ,0  ,48),--North Macedonia
('20200308',170,null,1   ,0  ,0  ,48),--Poland
('20200308',196,null,3   ,0  ,0  ,48),--Slovenia
('20200308',226,null,0   ,0  ,0  ,48),--Ukraine
('20200308',122,null,0   ,0  ,0  ,48),--Liechtenstein
('20200308',95 ,null,0   ,0  ,0  ,48),--Holy see
('20200308',195,null,2   ,0  ,0  ,48),--Slovakia
('20200308',31 ,null,2   ,0  ,0  ,48),--Bulgaria
('20200308',131,null,3   ,0  ,0  ,48),--Malta
('20200308',139,null,1   ,0  ,0  ,48),--Moldova

('20200308',72 ,null,1   ,0  ,0  ,48),--Faeroe Islands
('20200308',84 ,null,0   ,0  ,0  ,48),--Gibraltar

('20200308',211,null,2   ,0  ,0  ,48),--Thailand
('20200308',100,null,3   ,0  ,0  ,48),--India
('20200308',101,null,0   ,0  ,0  ,48),--Indonesia
('20200308',149,null,0   ,0  ,0  ,48),--Nepal
('20200308',201,null,0   ,0  ,0  ,48),--Sri Lanka
('20200308',129,null,2   ,0  ,0  ,48),--Maldives

('20200308',102,null,1076,21  ,0  ,48),--Iran
('20200308',115,null,4   ,0  ,0  ,48),--Kuwait
('20200308',16 ,null,7   ,0  ,0  ,48),--Bahrain
('20200308',103,null,10  ,4  ,0  ,48),--Iraq
('20200308',223,null,0   ,0  ,0  ,48),--UAE (United Arab Emirates)
('20200308',161,null,0   ,0  ,0  ,48),--Oman
('20200308',119,null,6   ,0  ,0  ,48),--lebanon
('20200308',173,null,1   ,0  ,0  ,48),--Qatar
('20200308',162,null,0   ,0  ,0  ,48),--Pakistan
('20200308',65 ,null,0   ,0  ,0  ,48),--Egypt
('20200308',144,null,0   ,0  ,0  ,48),--Morocco
('20200308',188,null,2   ,0  ,0  ,48),--Saudi Arabia
('20200308',1  ,null,3   ,0  ,0  ,48),--Afghanistan
('20200308',111,null,0   ,0  ,0  ,48),--Jordan
('20200308',217,null,0   ,0  ,0  ,48),--Tunisia

('20200308',164,null,0   ,0  ,0  ,48),--Palestine

('20200308',228,null,0   ,0  ,0  ,48),--USA
('20200308',37 ,null,6   ,0  ,0  ,48),--Canada
('20200308',64 ,null,0   ,0  ,0  ,48),--Ecuador
('20200308',137,null,2   ,0  ,0  ,48),--Mexico
('20200308',28 ,null,6   ,0  ,0  ,48),--Brasil
('20200308',9  ,null,7   ,1  ,0  ,48),--Argentina
('20200308',44 ,null,0   ,0  ,0  ,48),--Chile
('20200308',62 ,null,0   ,0  ,0  ,48),--Dominican Republic
('20200308',46 ,null,0   ,0  ,0  ,48),--Colombia
('20200308',168,null,0   ,0  ,0  ,48),--Peru
('20200308',50 ,null,5   ,0  ,0  ,48),--Costa Rica

('20200308',183 ,null,0   ,0  ,0  ,48),--Saint Martin
('20200308',178 ,null,0   ,0  ,0  ,48),--Saint Barthelemy
('20200308',77 ,null,5   ,0  ,0  ,48),--French Guiana
('20200308',133,null,2   ,0  ,0  ,48),--Martinique

('20200308',3  ,null,0   ,0  ,0  ,48),--Algeria
('20200308',189,null,0   ,0  ,0  ,48),--Senegal 
('20200308',155,null,0   ,0  ,0  ,48),--Nigeria
('20200308',36, null,0   ,0  ,0  ,48),--Cameroon
('20200308',199,null,1   ,0  ,0  ,48),--South Africa
('20200308',213,null,0   ,0  ,0  ,48),--Togo

('20200308',58 ,null,0   ,1  ,0  ,48)--Diamond Princess



INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200309',45 ,null,45  ,23  ,0 ,49),--China
('20200309',181,null,248 ,1  ,0  ,49),--Republic of korea
('20200309',110,null,33  ,1  ,0  ,49),--Japan
('20200309',193,null,12  ,0  ,0  ,49),--Singapore
('20200309',12 ,null,3   ,0  ,0  ,49),--Australia
('20200309',128,null,0   ,0  ,0  ,49),--Malaysia
('20200309',233,null,9   ,0  ,0  ,49),--Vietnam
('20200309',169,null,4   ,0  ,0  ,49),--Philippines
('20200309',152,null,0   ,0  ,0  ,49),--New Zealand
('20200309',35 ,null,0   ,0  ,0  ,49),--Cambodia

('20200309',107,null,1492,132,0  ,49),--Italy
('20200309',76 ,null,410 ,9  ,0  ,49),--France
('20200309',82 ,null,317 ,0  ,0  ,49),--Germany
('20200309',200,null,159 ,5  ,0  ,49),--Spain
('20200309',225,null,67  ,1  ,0  ,49),--UK (United Kingdom)
('20200309',207,null,68  ,1  ,0  ,49),--Switzerland
('20200309',160,null,22  ,0  ,0  ,49),--Norway
('20200309',150,null,77  ,2  ,0  ,49),--Netherlands
('20200309',13 ,null,10  ,0  ,0  ,49),--Austria
('20200309',206,null,42  ,0  ,0  ,49),--Sweden
('20200309',99 ,null,10  ,0  ,0  ,49),--Iceland
('20200309',20 ,null,31  ,0  ,0  ,49),--Belgium
('20200309',186,null,10  ,0  ,0  ,49),--San Marino
('20200309',106,null,14  ,0  ,0  ,49),--Israel
('20200309',57 ,null,5   ,0  ,0  ,49),--Denmark
('20200309',51 ,null,0   ,0  ,0  ,49),--Croatia
('20200309',85 ,null,7   ,0  ,0  ,49),--Greece
('20200309',75 ,null,11  ,0  ,0  ,49),--Finland
('20200309',171,null,9   ,0  ,0  ,49),--Portugal
('20200309',19 ,null,0   ,0  ,0  ,49),--Belarus
('20200309',55 ,null,6   ,0  ,0  ,49),--Czechia
('20200309',174,null,2   ,0  ,0  ,49),--Romania
('20200309',14 ,null,0   ,0  ,0  ,49),--Azerbaijan
('20200309',81 ,null,1   ,0  ,0  ,49),--Georgia
('20200309',175,null,0   ,0  ,0  ,49),--Russia
('20200309',26 ,null,0   ,0  ,0  ,49),--Bosnia and Herzegovina
('20200309',2  ,null,2   ,0  ,0  ,49),--Albania
('20200309',69 ,null,0   ,0  ,0  ,49),--Estonia
('20200309',98 ,null,2   ,0  ,0  ,49),--Hungary
('20200309',104,null,2   ,0  ,0  ,49),--Ireland
('20200309',5  ,null,0   ,0  ,0  ,49),--Andorra
('20200309',10 ,null,0   ,0  ,0  ,49),--Armenia
('20200309',118,null,2   ,0  ,0  ,49),--Latvia
('20200309',123,null,0   ,0  ,0  ,49),--Lithuania
('20200309',124,null,0   ,0  ,0  ,49),--Luxembourg
('20200309',140,null,0   ,0  ,0  ,49),--Monaco
('20200309',158,null,0   ,0  ,0  ,49),--North Macedonia
('20200309',170,null,5   ,0  ,0  ,49),--Poland
('20200309',196,null,4   ,0  ,0  ,49),--Slovenia
('20200309',226,null,0   ,0  ,0  ,49),--Ukraine
('20200309',122,null,0   ,0  ,0  ,49),--Liechtenstein
('20200309',95 ,null,0   ,0  ,0  ,49),--Holy see
('20200309',195,null,2   ,0  ,0  ,49),--Slovakia
('20200309',31 ,null,2   ,0  ,0  ,49),--Bulgaria
('20200309',131,null,0   ,0  ,0  ,49),--Malta
('20200309',139,null,0   ,0  ,0  ,49),--Moldova

('20200309',72 ,null,1   ,0  ,0  ,49),--Faeroe Islands
('20200309',84 ,null,0   ,0  ,0  ,49),--Gibraltar

('20200309',211,null,0   ,0  ,0  ,49),--Thailand
('20200309',100,null,9   ,0  ,0  ,49),--India
('20200309',101,null,2   ,0  ,0  ,49),--Indonesia
('20200309',149,null,0   ,0  ,0  ,49),--Nepal
('20200309',201,null,0   ,0  ,0  ,49),--Sri Lanka
('20200309',129,null,2   ,0  ,0  ,49),--Maldives
('20200309',17 ,null,3   ,0  ,0  ,49),--Bangladesh

('20200309',102,null,743 ,49 ,0  ,49),--Iran
('20200309',115,null,2   ,0  ,0  ,49),--Kuwait
('20200309',16 ,null,23  ,0  ,0  ,49),--Bahrain
('20200309',103,null,6   ,2  ,0  ,49),--Iraq
('20200309',223,null,0   ,0  ,0  ,49),--UAE (United Arab Emirates)
('20200309',161,null,0   ,0  ,0  ,49),--Oman
('20200309',119,null,4   ,0  ,0  ,49),--lebanon
('20200309',173,null,3   ,0  ,0  ,49),--Qatar
('20200309',162,null,1   ,0  ,0  ,49),--Pakistan
('20200309',65 ,null,7   ,1  ,0  ,49),--Egypt
('20200309',144,null,0   ,0  ,0  ,49),--Morocco
('20200309',188,null,8   ,0  ,0  ,49),--Saudi Arabia
('20200309',1  ,null,0   ,0  ,0  ,49),--Afghanistan
('20200309',111,null,0   ,0  ,0  ,49),--Jordan
('20200309',217,null,1   ,0  ,0  ,49),--Tunisia

('20200309',164,null,3   ,0  ,0  ,49),--Palestine

('20200309',228,null,0   ,0  ,0  ,49),--USA
('20200309',37 ,null,5   ,0  ,0  ,49),--Canada
('20200309',64 ,null,1   ,0  ,0  ,49),--Ecuador
('20200309',137,null,0   ,0  ,0  ,49),--Mexico
('20200309',28 ,null,6   ,0  ,0  ,49),--Brasil
('20200309',9  ,null,3   ,0  ,0  ,49),--Argentina
('20200309',44 ,null,5   ,0  ,0  ,49),--Chile
('20200309',62 ,null,0   ,0  ,0  ,49),--Dominican Republic
('20200309',46 ,null,0   ,0  ,0  ,49),--Colombia
('20200309',168,null,0   ,0  ,0  ,49),--Peru
('20200309',50 ,null,4   ,0  ,0  ,49),--Costa Rica
('20200309',167,null,4   ,0  ,0  ,49),--Paraguay

('20200309',183,null,0   ,0  ,0  ,49),--Saint Martin
('20200309',178,null,0   ,0  ,0  ,49),--Saint Barthelemy
('20200309',77 ,null,0   ,0  ,0  ,49),--French Guiana
('20200309',133,null,0   ,0  ,0  ,49),--Martinique

('20200309',3  ,null,3   ,0  ,0  ,49),--Algeria
('20200309',189,null,0   ,0  ,0  ,49),--Senegal 
('20200309',155,null,0   ,0  ,0  ,49),--Nigeria
('20200309',36, null,0   ,0  ,0  ,49),--Cameroon
('20200309',199,null,1   ,0  ,0  ,49),--South Africa
('20200309',213,null,0   ,0  ,0  ,49),--Togo

('20200309',58 ,null,0   ,0  ,0  ,49)--Diamond Princess










INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200310',45 ,null,20  ,17  ,0 ,50),--China
('20200310',181,null,131 ,3  ,0  ,50),--Republic of korea
('20200310',110,null,26  ,2  ,0  ,50),--Japan
('20200310',193,null,10  ,0  ,0  ,50),--Singapore
('20200310',12 ,null,15  ,0  ,0  ,50),--Australia
('20200310',128,null,117 ,0  ,0  ,50),--Malaysia
('20200310',233,null,1   ,0  ,0  ,50),--Vietnam
('20200310',169,null,23  ,0  ,0  ,50),--Philippines
('20200310',152,null,0   ,0  ,0  ,50),--New Zealand
('20200310',35 ,null,0   ,0  ,0  ,50),--Cambodia
('20200310',30 ,null,1   ,0  ,0  ,50),--Brunei
('20200310',141,null,1   ,0  ,0  ,50),--Mongolia

('20200310',107,null,1797,97 ,0  ,50),--Italy
('20200310',76 ,null,286 ,11 ,0  ,50),--France
('20200310',82 ,null,27  ,2  ,0  ,50),--Germany
('20200310',200,null,435 ,18 ,0  ,50),--Spain
('20200310',225,null,46  ,1  ,0  ,50),--UK (United Kingdom)
('20200310',207,null,0   ,0  ,0  ,50),--Switzerland
('20200310',160,null,23  ,0  ,0  ,50),--Norway
('20200310',150,null,56  ,0  ,0  ,50),--Netherlands
('20200310',13 ,null,19  ,0  ,0  ,50),--Austria
('20200310',206,null,45  ,0  ,0  ,50),--Sweden
('20200310',99 ,null,0   ,0  ,0  ,50),--Iceland
('20200310',20 ,null,39  ,0  ,0  ,50),--Belgium
('20200310',186,null,12  ,1  ,0  ,50),--San Marino
('20200310',106,null,0   ,0  ,0  ,50),--Israel
('20200310',57 ,null,54  ,0  ,0  ,50),--Denmark
('20200310',51 ,null,1   ,0  ,0  ,50),--Croatia
('20200310',85 ,null,0   ,0  ,0  ,50),--Greece
('20200310',75 ,null,10  ,0  ,0  ,50),--Finland
('20200310',171,null,0   ,0  ,0  ,50),--Portugal
('20200310',19 ,null,0   ,0  ,0  ,50),--Belarus
('20200310',55 ,null,6   ,0  ,0  ,50),--Czechia
('20200310',174,null,0   ,0  ,0  ,50),--Romania
('20200310',14 ,null,0   ,0  ,0  ,50),--Azerbaijan
('20200310',81 ,null,2   ,0  ,0  ,50),--Georgia
('20200310',175,null,0   ,0  ,0  ,50),--Russia
('20200310',26 ,null,0   ,0  ,0  ,50),--Bosnia and Herzegovina
('20200310',2  ,null,0   ,0  ,0  ,50),--Albania
('20200310',69 ,null,0   ,0  ,0  ,50),--Estonia
('20200310',98 ,null,0   ,0  ,0  ,50),--Hungary
('20200310',104,null,3   ,0  ,0  ,50),--Ireland
('20200310',5  ,null,0   ,0  ,0  ,50),--Andorra
('20200310',10 ,null,0   ,0  ,0  ,50),--Armenia
('20200310',118,null,3   ,0  ,0  ,50),--Latvia
('20200310',123,null,0   ,0  ,0  ,50),--Lithuania
('20200310',124,null,3   ,0  ,0  ,50),--Luxembourg
('20200310',140,null,0   ,0  ,0  ,50),--Monaco
('20200310',158,null,4   ,0  ,0  ,50),--North Macedonia
('20200310',170,null,5   ,0  ,0  ,50),--Poland
('20200310',196,null,7   ,0  ,0  ,50),--Slovenia
('20200310',226,null,0   ,0  ,0  ,50),--Ukraine
('20200310',122,null,0   ,0  ,0  ,50),--Liechtenstein
('20200310',95 ,null,0   ,0  ,0  ,50),--Holy see
('20200310',195,null,2   ,0  ,0  ,50),--Slovakia
('20200310',31 ,null,0   ,0  ,0  ,50),--Bulgaria
('20200310',131,null,1   ,0  ,0  ,50),--Malta
('20200310',139,null,0   ,0  ,0  ,50),--Moldova
('20200310',54 ,null,2   ,0  ,0  ,50),--Cyprus

('20200310',72 ,null,0   ,0  ,0  ,50),--Faeroe Islands
('20200310',239,null,1   ,0  ,0  ,50),--Guernsey

('20200310',211,null,3   ,0  ,0  ,50),--Thailand
('20200310',100,null,1   ,0  ,0  ,50),--India
('20200310',101,null,13   ,0  ,0  ,50),--Indonesia
('20200310',149,null,0   ,0  ,0  ,50),--Nepal
('20200310',201,null,0   ,0  ,0  ,50),--Sri Lanka
('20200310',129,null,0   ,0  ,0  ,50),--Maldives
('20200310',17 ,null,0   ,0  ,0  ,50),--Bangladesh

('20200310',102,null,595 ,43 ,0  ,50),--Iran
('20200310',115,null,1   ,0  ,0  ,50),--Kuwait
('20200310',16 ,null,30  ,0  ,0  ,50),--Bahrain
('20200310',103,null,1   ,0  ,0  ,50),--Iraq
('20200310',223,null,14  ,0  ,0  ,50),--UAE (United Arab Emirates)
('20200310',161,null,2   ,0  ,0  ,50),--Oman
('20200310',119,null,9   ,0  ,0  ,50),--lebanon
('20200310',173,null,3   ,0  ,0  ,50),--Qatar
('20200310',162,null,10  ,0  ,0  ,50),--Pakistan
('20200310',65 ,null,4   ,0  ,0  ,50),--Egypt
('20200310',144,null,0   ,0  ,0  ,50),--Morocco
('20200310',188,null,0   ,0  ,0  ,50),--Saudi Arabia
('20200310',1  ,null,0   ,0  ,0  ,50),--Afghanistan
('20200310',111,null,0   ,0  ,0  ,50),--Jordan
('20200310',217,null,0   ,0  ,0  ,50),--Tunisia

('20200310',164,null,7   ,0  ,0  ,50),--Palestine

('20200310',228,null,259 ,8  ,0  ,50),--USA
('20200310',37 ,null,15  ,0  ,0  ,50),--Canada
('20200310',64 ,null,0   ,0  ,0  ,50),--Ecuador
('20200310',137,null,0   ,0  ,0  ,50),--Mexico
('20200310',28 ,null,0   ,0  ,0  ,50),--Brasil
('20200310',9  ,null,0   ,0  ,0  ,50),--Argentina
('20200310',44 ,null,3   ,0  ,0  ,50),--Chile
('20200310',62 ,null,4   ,0  ,0  ,50),--Dominican Republic
('20200310',46 ,null,2   ,0  ,0  ,50),--Colombia
('20200310',168,null,3   ,0  ,0  ,50),--Peru
('20200310',50 ,null,0   ,0  ,0  ,50),--Costa Rica
('20200310',167,null,0   ,0  ,0  ,50),--Paraguay
('20200310',165,null,1   ,0  ,0  ,50),--Panama

('20200310',183,null,0   ,0  ,0  ,50),--Saint Martin
('20200310',178,null,0   ,0  ,0  ,50),--Saint Barthelemy
('20200310',77 ,null,0   ,0  ,0  ,50),--French Guiana
('20200310',133,null,0   ,0  ,0  ,50),--Martinique

('20200310',3  ,null,0   ,0  ,0  ,50),--Algeria
('20200310',189,null,0   ,0  ,0  ,50),--Senegal 
('20200310',155,null,0   ,0  ,0  ,50),--Nigeria
('20200310',36, null,0   ,0  ,0  ,50),--Cameroon
('20200310',199,null,4   ,0  ,0  ,50),--South Africa
('20200310',213,null,0   ,0  ,0  ,50),--Togo

('20200310',58 ,null,0   ,0  ,0  ,50)--Diamond Princess


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200311',45 ,null,31  ,22 ,0 ,51),--China
('20200311',181,null,242 ,6  ,0  ,51),--Republic of korea
('20200311',110,null,54  ,3  ,0  ,51),--Japan
('20200311',193,null,6   ,0  ,0  ,51),--Singapore
('20200311',12 ,null,20  ,0  ,0  ,51),--Australia
('20200311',128,null,12  ,0  ,0  ,51),--Malaysia
('20200311',233,null,4   ,0  ,0  ,51),--Vietnam
('20200311',169,null,16  ,0  ,0  ,51),--Philippines
('20200311',152,null,0   ,0  ,0  ,51),--New Zealand
('20200311',35 ,null,1   ,0  ,0  ,51),--Cambodia
('20200311',30 ,null,0   ,0  ,0  ,51),--Brunei
('20200311',141,null,0   ,0  ,0  ,51),--Mongolia

('20200311',107,null,977 ,168,0  ,51),--Italy
('20200311',76 ,null,372 ,3  ,0  ,51),--France
('20200311',82 ,null,157 ,0  ,0  ,51),--Germany
('20200311',200,null,615 ,8  ,0  ,51),--Spain
('20200311',225,null,50  ,3  ,0  ,51),--UK (United Kingdom)
('20200311',207,null,159 ,1  ,0  ,51),--Switzerland
('20200311',160,null,85  ,0  ,0  ,51),--Norway
('20200311',150,null,61  ,1  ,0  ,51),--Netherlands
('20200311',13 ,null,51  ,0  ,0  ,51),--Austria
('20200311',206,null,78  ,0  ,0  ,51),--Sweden
('20200311',99 ,null,0   ,0  ,0  ,51),--Iceland
('20200311',20 ,null,28  ,0  ,0  ,51),--Belgium
('20200311',186,null,14  ,0  ,0  ,51),--San Marino
('20200311',106,null,36  ,0  ,0  ,51),--Israel
('20200311',57 ,null,172 ,0  ,0  ,51),--Denmark
('20200311',51 ,null,4   ,0  ,0  ,51),--Croatia
('20200311',85 ,null,16  ,0  ,0  ,51),--Greece
('20200311',75 ,null,0   ,0  ,0  ,51),--Finland
('20200311',171,null,11  ,0  ,0  ,51),--Portugal
('20200311',19 ,null,3   ,0  ,0  ,51),--Belarus
('20200311',55 ,null,23  ,0  ,0  ,51),--Czechia
('20200311',174,null,10  ,0  ,0  ,51),--Romania
('20200311',14 ,null,0   ,0  ,0  ,51),--Azerbaijan
('20200311',81 ,null,8   ,0  ,0  ,51),--Georgia
('20200311',175,null,0   ,0  ,0  ,51),--Russia
('20200311',26 ,null,2   ,0  ,0  ,51),--Bosnia and Herzegovina
('20200311',2  ,null,8   ,0  ,0  ,51),--Albania
('20200311',69 ,null,3   ,0  ,0  ,51),--Estonia
('20200311',98 ,null,4   ,0  ,0  ,51),--Hungary
('20200311',104,null,10  ,0  ,0  ,51),--Ireland
('20200311',5  ,null,0   ,0  ,0  ,51),--Andorra
('20200311',10 ,null,0   ,0  ,0  ,51),--Armenia
('20200311',118,null,2   ,0  ,0  ,51),--Latvia
('20200311',123,null,0   ,0  ,0  ,51),--Lithuania
('20200311',124,null,0   ,0  ,0  ,51),--Luxembourg
('20200311',140,null,0   ,0  ,0  ,51),--Monaco
('20200311',158,null,0   ,0  ,0  ,51),--North Macedonia
('20200311',170,null,6   ,0  ,0  ,51),--Poland
('20200311',196,null,8   ,0  ,0  ,51),--Slovenia
('20200311',226,null,0   ,0  ,0  ,51),--Ukraine
('20200311',122,null,0   ,0  ,0  ,51),--Liechtenstein
('20200311',95 ,null,0   ,0  ,0  ,51),--Holy see
('20200311',195,null,0   ,0  ,0  ,51),--Slovakia
('20200311',31 ,null,2   ,0  ,0  ,51),--Bulgaria
('20200311',131,null,0   ,0  ,0  ,51),--Malta
('20200311',139,null,2   ,0  ,0  ,51),--Moldova
('20200311',54 ,null,0   ,0  ,0  ,51),--Cyprus
('20200311',190,null,11  ,0  ,0  ,51),--Serbia

('20200311',72 ,null,0   ,0  ,0  ,51),--Faeroe Islands
('20200311',239,null,0   ,0  ,0  ,51),--Guernsey

('20200311',211,null,6   ,0  ,0  ,51),--Thailand
('20200311',100,null,16  ,0  ,0  ,51),--India
('20200311',101,null,8   ,1  ,0  ,51),--Indonesia
('20200311',149,null,0   ,0  ,0  ,51),--Nepal
('20200311',201,null,0   ,0  ,0  ,51),--Sri Lanka
('20200311',129,null,4   ,0  ,0  ,51),--Maldives
('20200311',17 ,null,0   ,0  ,0  ,51),--Bangladesh

('20200311',102,null,881 ,54 ,0  ,51),--Iran
('20200311',115,null,4   ,0  ,0  ,51),--Kuwait
('20200311',16 ,null,1   ,0  ,0  ,51),--Bahrain
('20200311',103,null,0   ,0  ,0  ,51),--Iraq
('20200311',223,null,15  ,0  ,0  ,51),--UAE (United Arab Emirates)
('20200311',161,null,0   ,0  ,0  ,51),--Oman
('20200311',119,null,9   ,1  ,0  ,51),--lebanon
('20200311',173,null,6   ,0  ,0  ,51),--Qatar
('20200311',162,null,0   ,0  ,0  ,51),--Pakistan
('20200311',65 ,null,0   ,0  ,0  ,51),--Egypt
('20200311',144,null,1   ,0  ,0  ,51),--Morocco
('20200311',188,null,5   ,0  ,0  ,51),--Saudi Arabia
('20200311',1  ,null,0   ,0  ,0  ,51),--Afghanistan
('20200311',111,null,0   ,0  ,0  ,51),--Jordan
('20200311',217,null,4   ,0  ,0  ,51),--Tunisia


('20200311',164,null,4   ,0  ,0  ,51),--Palestine

('20200311',228,null,224 ,6  ,0  ,51),--USA
('20200311',37 ,null,16  ,1  ,0  ,51),--Canada
('20200311',64 ,null,0   ,0  ,0  ,51),--Ecuador
('20200311',137,null,0   ,0  ,0  ,51),--Mexico
('20200311',28 ,null,0   ,0  ,0  ,51),--Brasil
('20200311',9  ,null,5   ,0  ,0  ,51),--Argentina
('20200311',44 ,null,4   ,0  ,0  ,51),--Chile
('20200311',62 ,null,0   ,0  ,0  ,51),--Dominican Republic
('20200311',46 ,null,0   ,0  ,0  ,51),--Colombia
('20200311',168,null,2   ,0  ,0  ,51),--Peru
('20200311',50 ,null,0   ,0  ,0  ,51),--Costa Rica
('20200311',167,null,4   ,0  ,0  ,51),--Paraguay
('20200311',165,null,7   ,1  ,0  ,51),--Panama
('20200311',25 ,null,2   ,0  ,0  ,51),--Bolivia
('20200311',109,null,1   ,0  ,0  ,51),--Jamaica

('20200311',183,null,0   ,0  ,0  ,51),--Saint Martin
('20200311',178,null,0   ,0  ,0  ,51),--Saint Barthelemy
('20200311',77 ,null,0   ,0  ,0  ,51),--French Guiana
('20200311',133,null,1   ,0  ,0  ,51),--Martinique

('20200311',3  ,null,0   ,0  ,0  ,51),--Algeria
('20200311',189,null,0   ,0  ,0  ,51),--Senegal 
('20200311',155,null,0   ,0  ,0  ,51),--Nigeria
('20200311',36, null,0   ,0  ,0  ,51),--Cameroon
('20200311',199,null,0   ,0  ,0  ,51),--South Africa
('20200311',213,null,0   ,0  ,0  ,51),--Togo
('20200311',32 ,null,2   ,0  ,0  ,51),--Burkina Faso
('20200311',63 ,null,1   ,0  ,0  ,51),--DRC Congo

('20200311',58 ,null,0   ,0  ,0  ,51)--Diamond Princess



select SUM(report.cases),SUM(deaths),Country.name,Country.id 
from report inner join Country
on country.id = report.CountryId 
where Report.id <= 48
group by Country.name,Country.id
order by 1 desc

select SUM(cases) from Report where id <=48





INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200312',45 ,null,26  ,11 ,0 ,52),--China
('20200312',181,null,114 ,6  ,0  ,52),--Republic of korea
('20200312',110,null,52  ,3  ,0  ,52),--Japan
('20200312',193,null,12  ,0  ,0  ,52),--Singapore
('20200312',12 ,null,10  ,0  ,0  ,52),--Australia
('20200312',128,null,0   ,0  ,0  ,52),--Malaysia
('20200312',233,null,4   ,0  ,0  ,52),--Vietnam
('20200312',169,null,3   ,1  ,0  ,52),--Philippines
('20200312',152,null,0   ,0  ,0  ,52),--New Zealand
('20200312',35 ,null,0   ,0  ,0  ,52),--Cambodia
('20200312',30 ,null,11  ,0  ,0  ,52),--Brunei
('20200312',141,null,0   ,0  ,0  ,52),--Mongolia

('20200312',78 ,null,1   ,0  ,0  ,52),--French Polynesia

('20200312',107,null,2313,196 ,0  ,52),--Italy
('20200312',76 ,null,495 ,15 ,0  ,52),--France
('20200312',82 ,null,271 ,1  ,0  ,52),--Germany
('20200312',200,null,501 ,12 ,0  ,52),--Spain
('20200312',225,null,87  ,0  ,0  ,52),--UK (United Kingdom)
('20200312',207,null,154 ,1  ,0  ,52),--Switzerland
('20200312',160,null,212 ,0  ,0  ,52),--Norway
('20200312',150,null,121 ,1  ,0  ,52),--Netherlands
('20200312',13 ,null,120 ,0  ,0  ,52),--Austria
('20200312',206,null,135 ,0  ,0  ,52),--Sweden
('20200312',99 ,null,0   ,0  ,0  ,52),--Iceland
('20200312',20 ,null,47  ,0  ,0  ,52),--Belgium
('20200312',186,null,0   ,1  ,0  ,52),--San Marino
('20200312',106,null,0   ,0  ,0  ,52),--Israel
('20200312',57 ,null,353 ,0  ,0  ,52),--Denmark
('20200312',51 ,null,0   ,0  ,0  ,52),--Croatia
('20200312',85 ,null,9   ,1  ,0  ,52),--Greece
('20200312',75 ,null,0   ,0  ,0  ,52),--Finland
('20200312',171,null,0   ,0  ,0  ,52),--Portugal
('20200312',19 ,null,3   ,0  ,0  ,52),--Belarus
('20200312',55 ,null,33  ,0  ,0  ,52),--Czechia
('20200312',174,null,23  ,0  ,0  ,52),--Romania
('20200312',14 ,null,0   ,0  ,0  ,52),--Azerbaijan
('20200312',81 ,null,0   ,0  ,0  ,52),--Georgia
('20200312',175,null,13  ,0  ,0  ,52),--Russia
('20200312',26 ,null,0   ,0  ,0  ,52),--Bosnia and Herzegovina
('20200312',2  ,null,0   ,0  ,0  ,52),--Albania
('20200312',69 ,null,0   ,0  ,0  ,52),--Estonia
('20200312',98 ,null,0   ,0  ,0  ,52),--Hungary
('20200312',104,null,9   ,1  ,0  ,52),--Ireland
('20200312',5  ,null,0   ,0  ,0  ,52),--Andorra
('20200312',10 ,null,0   ,0  ,0  ,52),--Armenia
('20200312',118,null,8   ,0  ,0  ,52),--Latvia
('20200312',123,null,2   ,0  ,0  ,52),--Lithuania
('20200312',124,null,12  ,0  ,0  ,52),--Luxembourg
('20200312',140,null,0   ,0  ,0  ,52),--Monaco
('20200312',158,null,0   ,0  ,0  ,52),--North Macedonia
('20200312',170,null,22  ,0  ,0  ,52),--Poland
('20200312',196,null,26  ,0  ,0  ,52),--Slovenia
('20200312',226,null,0   ,0  ,0  ,52),--Ukraine
('20200312',122,null,0   ,0  ,0  ,52),--Liechtenstein
('20200312',95 ,null,0   ,0  ,0  ,52),--Holy see
('20200312',195,null,3   ,0  ,0  ,52),--Slovakia
('20200312',31 ,null,1   ,0  ,0  ,52),--Bulgaria
('20200312',131,null,2   ,0  ,0  ,52),--Malta
('20200312',139,null,1   ,0  ,0  ,52),--Moldova
('20200312',54 ,null,4   ,0  ,0  ,52),--Cyprus
('20200312',190,null,7   ,0  ,0  ,52),--Serbia
('20200312',218,null,1   ,0  ,0  ,52),--Turkey

('20200312',72 ,null,0   ,0  ,0  ,52),--Faeroe Islands
('20200312',239,null,0   ,0  ,0  ,52),--Guernsey

('20200312',211,null,11  ,0  ,0  ,52),--Thailand
('20200312',100,null,13  ,0  ,0  ,52),--India
('20200312',101,null,7   ,0  ,0  ,52),--Indonesia
('20200312',149,null,0   ,0  ,0  ,52),--Nepal
('20200312',201,null,1   ,0  ,0  ,52),--Sri Lanka
('20200312',129,null,0   ,0  ,0  ,52),--Maldives
('20200312',17 ,null,0   ,0  ,0  ,52),--Bangladesh

('20200312',102,null,958 ,63 ,0  ,52),--Iran
('20200312',115,null,11  ,0  ,0  ,52),--Kuwait
('20200312',16 ,null,79  ,0  ,0  ,52),--Bahrain
('20200312',103,null,9   ,1  ,0  ,52),--Iraq
('20200312',223,null,0   ,0  ,0  ,52),--UAE (United Arab Emirates)
('20200312',161,null,0   ,0  ,0  ,52),--Oman
('20200312',119,null,25  ,2  ,0  ,52),--lebanon
('20200312',173,null,238 ,0  ,0  ,52),--Qatar
('20200312',162,null,3   ,0  ,0  ,52),--Pakistan
('20200312',65 ,null,8   ,0  ,0  ,52),--Egypt
('20200312',144,null,2   ,0  ,0  ,52),--Morocco
('20200312',188,null,1   ,0  ,0  ,52),--Saudi Arabia
('20200312',1  ,null,0   ,0  ,0  ,52),--Afghanistan
('20200312',111,null,0   ,0  ,0  ,52),--Jordan
('20200312',217,null,0   ,0  ,0  ,52),--Tunisia


('20200312',164,null,0   ,0  ,0  ,52),--Palestine

('20200312',228,null,291 ,4  ,0  ,52),--USA
('20200312',37 ,null,0   ,0  ,0  ,52),--Canada
('20200312',64 ,null,2   ,0  ,0  ,52),--Ecuador
('20200312',137,null,4   ,0  ,0  ,52),--Mexico
('20200312',28 ,null,18  ,0  ,0  ,52),--Brasil
('20200312',9  ,null,2   ,0  ,0  ,52),--Argentina
('20200312',44 ,null,6   ,0  ,0  ,52),--Chile
('20200312',62 ,null,0   ,0  ,0  ,52),--Dominican Republic
('20200312',46 ,null,6   ,0  ,0  ,52),--Colombia
('20200312',168,null,6   ,0  ,0  ,52),--Peru
('20200312',50 ,null,0   ,0  ,0  ,52),--Costa Rica
('20200312',167,null,2   ,1  ,0  ,52),--Paraguay
('20200312',165,null,0   ,0  ,0  ,52),--Panama
('20200312',25 ,null,0   ,0  ,0  ,52),--Bolivia
('20200312',109,null,0   ,0  ,0  ,52),--Jamaica
('20200312',96 ,null,2   ,0  ,0  ,52),--Honduras


('20200312',183,null,0   ,0  ,0  ,52),--Saint Martin
('20200312',178,null,0   ,0  ,0  ,52),--Saint Barthelemy
('20200312',77 ,null,0   ,0  ,0  ,52),--French Guiana
('20200312',133,null,0   ,0  ,0  ,52),--Martinique

('20200312',3  ,null,5   ,1  ,0  ,52),--Algeria
('20200312',189,null,0   ,0  ,0  ,52),--Senegal 
('20200312',155,null,0   ,0  ,0  ,52),--Nigeria
('20200312',36, null,0   ,0  ,0  ,52),--Cameroon
('20200312',199,null,6   ,0  ,0  ,52),--South Africa
('20200312',213,null,0   ,0  ,0  ,52),--Togo
('20200312',32 ,null,0   ,0  ,0  ,52),--Burkina Faso
('20200312',63 ,null,0   ,0  ,0  ,52),--DRC Congo
('20200312',56 ,null,1   ,0  ,0  ,52),--Cote Divore

('20200312',58 ,null,0   ,0  ,0  ,52)--Diamond Princess














INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200313',45 ,null,26  ,11 ,0 ,53), --China
('20200313',181,null,110 ,0  ,0  ,53),--Republic of korea
('20200313',110,null,55  ,4  ,0  ,53),--Japan
('20200313',193,null,9   ,0  ,0  ,53),--Singapore
('20200313',12 ,null,18  ,0  ,0  ,53),--Australia
('20200313',128,null,0   ,0  ,0  ,53),--Malaysia
('20200313',233,null,0   ,0  ,0  ,53),--Vietnam
('20200313',169,null,0   ,0  ,0  ,53),--Philippines
('20200313',152,null,0   ,0  ,0  ,53),--New Zealand
('20200313',35 ,null,2   ,0  ,0  ,53),--Cambodia
('20200313',30 ,null,0   ,0  ,0  ,53),--Brunei
('20200313',141,null,0   ,0  ,0  ,53),--Mongolia

('20200313',78 ,null,0   ,0  ,0  ,53),--French Polynesia

('20200313',107,null,2651,189,0  ,53),--Italy
('20200313',76 ,null,591 ,13 ,0  ,53),--France
('20200313',82 ,null,802 ,3  ,0  ,53),--Germany
('20200313',200,null,825 ,36 ,0  ,53),--Spain
('20200313',225,null,134 ,2  ,0  ,53),--UK (United Kingdom)
('20200313',207,null,213 ,2  ,0  ,53),--Switzerland
('20200313',160,null,0   ,0  ,0  ,53),--Norway
('20200313',150,null,111 ,0  ,0  ,53),--Netherlands
('20200313',13 ,null,59  ,1  ,0  ,53),--Austria
('20200313',206,null,159 ,0  ,0  ,53),--Sweden
('20200313',99 ,null,0   ,0  ,0  ,53),--Iceland
('20200313',20 ,null,0   ,0  ,0  ,53),--Belgium
('20200313',186,null,0   ,0  ,0  ,53),--San Marino
('20200313',106,null,0   ,0  ,0  ,53),--Israel
('20200313',57 ,null,59  ,0  ,0  ,53),--Denmark
('20200313',51 ,null,0   ,0  ,0  ,53),--Croatia
('20200313',85 ,null,0   ,0  ,0  ,53),--Greece
('20200313',75 ,null,69  ,0  ,0  ,53),--Finland
('20200313',171,null,0   ,0  ,0  ,53),--Portugal
('20200313',19 ,null,0   ,0  ,0  ,53),--Belarus
('20200313',55 ,null,22  ,0  ,0  ,53),--Czechia
('20200313',174,null,0   ,0  ,0  ,53),--Romania
('20200313',14 ,null,2   ,0  ,0  ,53),--Azerbaijan
('20200313',81 ,null,2   ,0  ,0  ,53),--Georgia
('20200313',175,null,14  ,0  ,0  ,53),--Russia
('20200313',26 ,null,0   ,0  ,0  ,53),--Bosnia and Herzegovina
('20200313',2  ,null,13  ,0  ,0  ,53),--Albania
('20200313',69 ,null,0   ,0  ,0  ,53),--Estonia
('20200313',98 ,null,3   ,0  ,0  ,53),--Hungary
('20200313',104,null,27  ,0  ,0  ,53),--Ireland
('20200313',5  ,null,0   ,0  ,0  ,53),--Andorra
('20200313',10 ,null,0   ,0  ,0  ,53),--Armenia
('20200313',118,null,0   ,0  ,0  ,53),--Latvia
('20200313',123,null,0   ,0  ,0  ,53),--Lithuania
('20200313',124,null,0   ,0  ,0  ,53),--Luxembourg
('20200313',140,null,0   ,0  ,0  ,53),--Monaco
('20200313',158,null,0   ,0  ,0  ,53),--North Macedonia
('20200313',170,null,5   ,1  ,0  ,53),--Poland
('20200313',196,null,0   ,0  ,0  ,53),--Slovenia
('20200313',226,null,2   ,0  ,0  ,53),--Ukraine
('20200313',122,null,3   ,0  ,0  ,53),--Liechtenstein
('20200313',95 ,null,0   ,0  ,0  ,53),--Holy see
('20200313',195,null,11  ,0  ,0  ,53),--Slovakia
('20200313',31 ,null,0   ,0  ,0  ,53),--Bulgaria
('20200313',131,null,3   ,0  ,0  ,53),--Malta
('20200313',139,null,0   ,0  ,0  ,53),--Moldova
('20200313',54 ,null,0   ,0  ,0  ,53),--Cyprus
('20200313',190,null,0   ,0  ,0  ,53),--Serbia
('20200313',218,null,0   ,0  ,0  ,53),--Turkey

('20200313',72 ,null,0   ,0  ,0  ,53),--Faeroe Islands
('20200313',239,null,0   ,0  ,0  ,53),--Guernsey
('20200313',240,null,2   ,0  ,0  ,53),--Guernsey

('20200313',211,null,5   ,0  ,0  ,53),--Thailand
('20200313',100,null,1   ,1  ,0  ,53),--India
('20200313',101,null,0   ,0  ,0  ,53),--Indonesia
('20200313',149,null,0   ,0  ,0  ,53),--Nepal
('20200313',201,null,1   ,0  ,0  ,53),--Sri Lanka
('20200313',129,null,0   ,0  ,0  ,53),--Maldives
('20200313',17 ,null,0   ,0  ,0  ,53),--Bangladesh

('20200313',102,null,1075,75 ,0  ,53),--Iran
('20200313',115,null,0   ,0  ,0  ,53),--Kuwait
('20200313',16 ,null,6   ,0  ,0  ,53),--Bahrain
('20200313',103,null,0   ,0  ,0  ,53),--Iraq
('20200313',223,null,11  ,0  ,0  ,53),--UAE (United Arab Emirates)
('20200313',161,null,0   ,0  ,0  ,53),--Oman
('20200313',119,null,0   ,0  ,0  ,53),--lebanon
('20200313',173,null,0   ,0  ,0  ,53),--Qatar
('20200313',162,null,1   ,0  ,0  ,53),--Pakistan
('20200313',65 ,null,0   ,0  ,0  ,53),--Egypt
('20200313',144,null,1   ,0  ,0  ,53),--Morocco
('20200313',188,null,0   ,0  ,0  ,53),--Saudi Arabia
('20200313',1  ,null,0   ,0  ,0  ,53),--Afghanistan
('20200313',111,null,0   ,0  ,0  ,53),--Jordan
('20200313',217,null,1   ,0  ,0  ,53),--Tunisia


('20200313',164,null,1   ,0  ,0  ,53),--Palestine

('20200313',228,null,277 ,7  ,0  ,53),--USA
('20200313',37 ,null,45  ,0  ,0  ,53),--Canada
('20200313',64 ,null,0   ,0  ,0  ,53),--Ecuador
('20200313',137,null,1   ,0  ,0  ,53),--Mexico
('20200313',28 ,null,25  ,0  ,0  ,53),--Brasil
('20200313',9  ,null,12  ,0  ,0  ,53),--Argentina
('20200313',44 ,null,10  ,0  ,0  ,53),--Chile
('20200313',62 ,null,0   ,0  ,0  ,53),--Dominican Republic
('20200313',46 ,null,0   ,0  ,0  ,53),--Colombia
('20200313',168,null,5   ,0  ,0  ,53),--Peru
('20200313',50 ,null,9   ,0  ,0  ,53),--Costa Rica
('20200313',167,null,0   ,0  ,0  ,53),--Paraguay
('20200313',165,null,4   ,0  ,0  ,53),--Panama
('20200313',25 ,null,0   ,0  ,0  ,53),--Bolivia
('20200313',109,null,0   ,0  ,0  ,53),--Jamaica
('20200313',96 ,null,0   ,0  ,0  ,53),--Honduras
('20200313',93 ,null,1   ,0  ,0  ,53),--Guyana
('20200313',203,null,1   ,0  ,0  ,53),--St. Vicent Grenadines

('20200313',183,null,0   ,0  ,0  ,53),--Saint Martin
('20200313',178,null,0   ,0  ,0  ,53),--Saint Barthelemy
('20200313',77 ,null,1   ,0  ,0  ,53),--French Guiana
('20200313',133,null,1   ,0  ,0  ,53),--Martinique

('20200313',3  ,null,0   ,0  ,0  ,53),--Algeria
('20200313',189,null,6   ,0  ,0  ,53),--Senegal 
('20200313',155,null,0   ,0  ,0  ,53),--Nigeria
('20200313',36, null,0   ,0  ,0  ,53),--Cameroon
('20200313',199,null,4   ,0  ,0  ,53),--South Africa
('20200313',213,null,0   ,0  ,0  ,53),--Togo
('20200313',32 ,null,0   ,0  ,0  ,53),--Burkina Faso
('20200313',63 ,null,0   ,0  ,0  ,53),--DRC Congo
('20200313',56 ,null,0   ,0  ,0  ,53),--Cote Divore

('20200313',58 ,null,0   ,0  ,0  ,53)--Diamond Princess






INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200314',45 ,null,18  ,14 ,0 ,54), --China
('20200314',181,null,107 ,6  ,0  ,54),--Republic of korea
('20200314',110,null,41  ,2  ,0  ,54),--Japan
('20200314',193,null,13  ,0  ,0  ,54),--Singapore
('20200314',12 ,null,57  ,0  ,0  ,54),--Australia
('20200314',128,null,68  ,0  ,0  ,54),--Malaysia
('20200314',233,null,9   ,0  ,0  ,54),--Vietnam
('20200314',169,null,12  ,0  ,0  ,54),--Philippines
('20200314',152,null,1   ,0  ,0  ,54),--New Zealand
('20200314',35 ,null,2   ,0  ,0  ,54),--Cambodia
('20200314',30 ,null,13  ,0  ,0  ,54),--Brunei
('20200314',141,null,0   ,0  ,0  ,54),--Mongolia

('20200314',78 ,null,0   ,0  ,0  ,54),--French Polynesia

('20200314',107,null,2547,252,0  ,54),--Italy
('20200314',76 ,null,780 ,18 ,0  ,54),--France
('20200314',82 ,null,693 ,0  ,0  ,54),--Germany
('20200314',200,null,1266,36 ,0  ,54),--Spain
('20200314',225,null,208 ,2  ,0  ,54),--UK (United Kingdom)
('20200314',207,null,267 ,0  ,0  ,54),--Switzerland
('20200314',160,null,261 ,1  ,0  ,54),--Norway
('20200314',150,null,190 ,5  ,0  ,54),--Netherlands
('20200314',13 ,null,143 ,0  ,0  ,54),--Austria
('20200314',206,null,155 ,0  ,0  ,54),--Sweden
('20200314',99 ,null,0   ,0  ,0  ,54),--Iceland
('20200314',20 ,null,285 ,0  ,0  ,54),--Belgium
('20200314',186,null,3   ,0  ,0  ,54),--San Marino
('20200314',106,null,25  ,0  ,0  ,54),--Israel
('20200314',57 ,null,127 ,0  ,0  ,54),--Denmark
('20200314',51 ,null,11  ,0  ,0  ,54),--Croatia

('20200314',85 ,null,0   ,0  ,0  ,54),--Greece
('20200314',75 ,null,0   ,0  ,0  ,54),--Finland
('20200314',171,null,71  ,0  ,0  ,54),--Portugal
('20200314',19 ,null,9   ,0  ,0  ,54),--Belarus
('20200314',55 ,null,34  ,0  ,0  ,54),--Czechia
('20200314',174,null,16  ,0  ,0  ,54),--Romania
('20200314',14 ,null,0   ,0  ,0  ,54),--Azerbaijan
('20200314',81 ,null,0   ,0  ,0  ,54),--Georgia
('20200314',175,null,0   ,0  ,0  ,54),--Russia
('20200314',26 ,null,7   ,0  ,0  ,54),--Bosnia and Herzegovina
('20200314',2  ,null,10  ,1  ,0  ,54),--Albania
('20200314',69 ,null,66  ,0  ,0  ,54),--Estonia
('20200314',98 ,null,3   ,0  ,0  ,54),--Hungary
('20200314',104,null,20  ,0  ,0  ,54),--Ireland
('20200314',5  ,null,1   ,0  ,0  ,54),--Andorra
('20200314',10 ,null,7   ,0  ,0  ,54),--Armenia
('20200314',118,null,0   ,0  ,0  ,54),--Latvia
('20200314',123,null,3   ,0  ,0  ,54),--Lithuania
('20200314',124,null,21  ,1  ,0  ,54),--Luxembourg
('20200314',140,null,1   ,0  ,0  ,54),--Monaco
('20200314',158,null,2   ,0  ,0  ,54),--North Macedonia
('20200314',170,null,15  ,0  ,0  ,54),--Poland
('20200314',196,null,84  ,0  ,0  ,54),--Slovenia
('20200314',226,null,0   ,0  ,0  ,54),--Ukraine
('20200314',122,null,0   ,0  ,0  ,54),--Liechtenstein
('20200314',95 ,null,0   ,0  ,0  ,54),--Holy see
('20200314',195,null,9   ,0  ,0  ,54),--Slovakia
('20200314',31 ,null,0   ,0  ,0  ,54),--Bulgaria
('20200314',131,null,3   ,0  ,0  ,54),--Malta
('20200314',139,null,4   ,0  ,0  ,54),--Moldova
('20200314',54 ,null,8   ,0  ,0  ,54),--Cyprus
('20200314',190,null,12  ,0  ,0  ,54),--Serbia
('20200314',218,null,4   ,0  ,0  ,54),--Turkey

('20200314',72 ,null,1   ,0  ,0  ,54),--Faeroe Islands
('20200314',239,null,0   ,0  ,0  ,54),--Guernsey
('20200314',240,null,0   ,0  ,0  ,54),--Guernsey

('20200314',211,null,0   ,0  ,0  ,54),--Thailand
('20200314',100,null,8   ,1  ,0  ,54),--India
('20200314',101,null,35  ,2  ,0  ,54),--Indonesia
('20200314',149,null,0   ,0  ,0  ,54),--Nepal
('20200314',201,null,3   ,0  ,0  ,54),--Sri Lanka
('20200314',129,null,1   ,0  ,0  ,54),--Maldives
('20200314',17 ,null,0   ,0  ,0  ,54),--Bangladesh

('20200314',102,null,1289,85 ,0  ,54),--Iran
('20200314',115,null,20  ,0  ,0  ,54),--Kuwait
('20200314',16 ,null,15  ,0  ,0  ,54),--Bahrain
('20200314',103,null,23  ,2  ,0  ,54),--Iraq
('20200314',223,null,0   ,0  ,0  ,54),--UAE (United Arab Emirates)
('20200314',161,null,1   ,0  ,0  ,54),--Oman
('20200314',119,null,11   ,0  ,0  ,54),--lebanon
('20200314',173,null,0   ,0  ,0  ,54),--Qatar
('20200314',162,null,1   ,0  ,0  ,54),--Pakistan
('20200314',65 ,null,26  ,1  ,0  ,54),--Egypt
('20200314',144,null,1   ,0  ,0  ,54),--Morocco
('20200314',188,null,41  ,0  ,0  ,54),--Saudi Arabia
('20200314',1  ,null,0   ,0  ,0  ,54),--Afghanistan
('20200314',111,null,0   ,0  ,0  ,54),--Jordan
('20200314',217,null,9   ,0  ,0  ,54),--Tunisia
('20200314',204,null,1   ,0  ,0  ,54),--Sudan

('20200314',164,null,4   ,0  ,0  ,54),--Palestine

('20200314',228,null,414 ,5  ,0  ,54),--USA
('20200314',37 ,null,38  ,0  ,0  ,54),--Canada
('20200314',64 ,null,6   ,0  ,0  ,54),--Ecuador
('20200314',137,null,14  ,0  ,0  ,54),--Mexico
('20200314',28 ,null,21  ,0  ,0  ,54),--Brasil
('20200314',9  ,null,3   ,1  ,0  ,54),--Argentina
('20200314',44 ,null,10  ,0  ,0  ,54),--Chile
('20200314',62 ,null,0   ,0  ,0  ,54),--Dominican Republic
('20200314',46 ,null,7   ,0  ,0  ,54),--Colombia
('20200314',168,null,6   ,0  ,0  ,54),--Peru
('20200314',50 ,null,1   ,0  ,0  ,54),--Costa Rica
('20200314',167,null,1   ,0  ,0  ,54),--Paraguay
('20200314',165,null,13  ,0  ,0  ,54),--Panama
('20200314',25 ,null,0   ,0  ,0  ,54),--Bolivia
('20200314',109,null,6   ,0  ,0  ,54),--Jamaica
('20200314',96 ,null,0   ,0  ,0  ,54),--Honduras
('20200314',93 ,null,0   ,0  ,0  ,54),--Guyana
('20200314',203,null,0   ,0  ,0  ,54),--St. Vicent Grenadines
('20200314',52 ,null,4   ,0  ,0  ,54),--Cuba
('20200314',172,null,3   ,0  ,0  ,54),--Puerto rico
('20200314',232,null,2   ,0  ,0  ,54),--Venezuela
('20200314',8  ,null,1   ,0  ,0  ,54),--Antigua and barbuda
('20200314',88 ,null,1   ,0  ,0  ,54),--Guadeloupe
('20200314',216,null,1   ,0  ,0  ,54),--Trinidad and Tobago

('20200314',183,null,0   ,0  ,0  ,54),--Saint Martin
('20200314',178,null,0   ,0  ,0  ,54),--Saint Barthelemy
('20200314',77 ,null,0   ,0  ,0  ,54),--French Guiana
('20200314',133,null,2   ,0  ,0  ,54),--Martinique
('20200314',40 ,null,1   ,0  ,0  ,54),--Martinique

('20200314',3  ,null,1   ,1  ,0  ,54),--Algeria
('20200314',189,null,0   ,0  ,0  ,54),--Senegal 
('20200314',155,null,0   ,0  ,0  ,54),--Nigeria
('20200314',36, null,0   ,0  ,0  ,54),--Cameroon
('20200314',199,null,0   ,0  ,0  ,54),--South Africa
('20200314',213,null,0   ,0  ,0  ,54),--Togo
('20200314',32 ,null,0   ,0  ,0  ,54),--Burkina Faso
('20200314',63 ,null,1   ,0  ,0  ,54),--DRC Congo
('20200314',56 ,null,0   ,0  ,0  ,54),--Cote Divore
('20200314',71 ,null,1   ,0  ,0  ,54),--Ethiopia
('20200314',79 ,null,1   ,0  ,0  ,54),--Gabon
('20200314',83 ,null,1   ,0  ,0  ,54),--Ghana
('20200314',91 ,null,1   ,0  ,0  ,54),--Guinea
('20200314',113,null,1   ,0  ,0  ,54),--Kenya

('20200314',177,null,1   ,0  ,0  ,54),--Reunion
('20200314',58 ,null,1   ,0  ,0  ,54)--Diamond Princess





INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200315',45 ,null,27  ,10 ,0 ,55), --China
('20200315',181,null,76  ,3  ,0  ,55),--Republic of korea
('20200315',110,null,64  ,1  ,0  ,55),--Japan
('20200315',193,null,12  ,0  ,0  ,55),--Singapore
('20200315',12 ,null,52  ,0  ,0  ,55),--Australia
('20200315',128,null,41  ,0  ,0  ,55),--Malaysia
('20200315',233,null,5   ,0  ,0  ,55),--Vietnam
('20200315',169,null,47  ,4  ,0  ,55),--Philippines
('20200315',152,null,0   ,0  ,0  ,55),--New Zealand
('20200315',35 ,null,0   ,0  ,0  ,55),--Cambodia
('20200315',30 ,null,15  ,0  ,0  ,55),--Brunei
('20200315',141,null,0   ,0  ,0  ,55),--Mongolia

('20200315',78 ,null,2   ,0  ,0  ,55),--French Polynesia

('20200315',107,null,3497,173,0  ,55),--Italy
('20200315',76 ,null,829 ,12 ,0  ,55),--France
('20200315',82 ,null,733 ,2  ,0  ,55),--Germany
('20200315',200,null,1522,16 ,0  ,55),--Spain
('20200315',225,null,342 ,11 ,0  ,55),--UK (United Kingdom)
('20200315',207,null,234 ,5  ,0  ,55),--Switzerland
('20200315',160,null,157 ,0  ,0  ,55),--Norway
('20200315',150,null,155 ,2  ,0  ,55),--Netherlands
('20200315',13 ,null,296 ,0  ,0  ,55),--Austria
('20200315',206,null,149 ,0  ,0  ,55),--Sweden
('20200315',99 ,null,77  ,0  ,0  ,55),--Iceland
('20200315',20 ,null,90  ,0  ,0  ,55),--Belgium
('20200315',186,null,26  ,3  ,0  ,55),--San Marino
('20200315',106,null,78  ,0  ,0  ,55),--Israel
('20200315',57 ,null,26  ,0  ,0  ,55),--Denmark
('20200315',51 ,null,10  ,0  ,0  ,55),--Croatia

('20200315',85 ,null,130 ,1  ,0  ,55),--Greece
('20200315',75 ,null,101 ,0  ,0  ,55),--Finland
('20200315',171,null,0   ,0  ,0  ,55),--Portugal
('20200315',19 ,null,0   ,0  ,0  ,55),--Belarus
('20200315',55 ,null,64  ,0  ,0  ,55),--Czechia
('20200315',174,null,59  ,0  ,0  ,55),--Romania
('20200315',14 ,null,8   ,0  ,0  ,55),--Azerbaijan
('20200315',81 ,null,5   ,0  ,0  ,55),--Georgia
('20200315',175,null,0   ,0  ,0  ,55),--Russia
('20200315',26 ,null,7   ,0  ,0  ,55),--Bosnia and Herzegovina
('20200315',2  ,null,5   ,0  ,0  ,55),--Albania
('20200315',69 ,null,0   ,0  ,0  ,55),--Estonia
('20200315',98 ,null,13  ,0  ,0  ,55),--Hungary
('20200315',104,null,39  ,1  ,0  ,55),--Ireland
('20200315',5  ,null,0   ,0  ,0  ,55),--Andorra
('20200315',10 ,null,0   ,0  ,0  ,55),--Armenia
('20200315',118,null,14  ,0  ,0  ,55),--Latvia
('20200315',123,null,3   ,0  ,0  ,55),--Lithuania
('20200315',124,null,0   ,0  ,0  ,55),--Luxembourg
('20200315',140,null,0   ,0  ,0  ,55),--Monaco
('20200315',158,null,4   ,0  ,0  ,55),--North Macedonia
('20200315',170,null,47  ,2  ,0  ,55),--Poland
('20200315',196,null,0   ,0  ,0  ,55),--Slovenia
('20200315',226,null,0   ,0  ,0  ,55),--Ukraine
('20200315',122,null,0   ,0  ,0  ,55),--Liechtenstein
('20200315',95 ,null,0   ,0  ,0  ,55),--Holy see
('20200315',195,null,14  ,0  ,0  ,55),--Slovakia
('20200315',31 ,null,36  ,1  ,0  ,55),--Bulgaria
('20200315',131,null,0   ,0  ,0  ,55),--Malta
('20200315',139,null,4   ,0  ,0  ,55),--Moldova
('20200315',54 ,null,7   ,0  ,0  ,55),--Cyprus
('20200315',190,null,10  ,0  ,0  ,55),--Serbia
('20200315',218,null,0   ,0  ,0  ,55),--Turkey
('20200315',112,null,6   ,0  ,0  ,55),--Kazakhstan

('20200315',72 ,null,6   ,0  ,0  ,55),--Faeroe Islands
('20200315',239,null,0   ,0  ,0  ,55),--Guernsey
('20200315',240,null,0   ,0  ,0  ,55),--Jersey

('20200315',211,null,0   ,0  ,0  ,55),--Thailand
('20200315',100,null,25  ,2  ,0  ,55),--India
('20200315',101,null,48  ,1  ,0  ,55),--Indonesia
('20200315',149,null,0   ,0  ,0  ,55),--Nepal
('20200315',201,null,5   ,0  ,0  ,55),--Sri Lanka
('20200315',129,null,1   ,0  ,0  ,55),--Maldives
('20200315',17 ,null,0   ,0  ,0  ,55),--Bangladesh

('20200315',102,null,1365,94 ,0  ,55),--Iran
('20200315',115,null,12  ,0  ,0  ,55),--Kuwait
('20200315',16 ,null,1   ,0  ,0  ,55),--Bahrain
('20200315',103,null,0   ,0  ,0  ,55),--Iraq
('20200315',223,null,0   ,0  ,0  ,55),--UAE (United Arab Emirates)
('20200315',161,null,1   ,0  ,0  ,55),--Oman
('20200315',119,null,16  ,0  ,0  ,55),--lebanon
('20200315',173,null,75  ,0  ,0  ,55),--Qatar
('20200315',162,null,7   ,0  ,0  ,55),--Pakistan
('20200315',65 ,null,0   ,0  ,0  ,55),--Egypt
('20200315',144,null,11  ,0  ,0  ,55),--Morocco
('20200315',188,null,41  ,0  ,0  ,55),--Saudi Arabia
('20200315',1  ,null,3   ,0  ,0  ,55),--Afghanistan
('20200315',111,null,0   ,0  ,0  ,55),--Jordan
('20200315',217,null,0   ,0  ,0  ,55),--Tunisia
('20200315',204,null,0   ,0  ,0  ,55),--Sudan

('20200315',164,null,3   ,0  ,0  ,55),--Palestine

('20200315',228,null,0   ,0  ,0  ,55),--USA
('20200315',37 ,null,68  ,0  ,0  ,55),--Canada
('20200315',64 ,null,0   ,0  ,0  ,55),--Ecuador
('20200315',137,null,15  ,0  ,0  ,55),--Mexico
('20200315',28 ,null,23  ,0  ,0  ,55),--Brasil
('20200315',9  ,null,11  ,0  ,0  ,55),--Argentina
('20200315',44 ,null,18  ,0  ,0  ,55),--Chile
('20200315',62 ,null,0   ,0  ,0  ,55),--Dominican Republic
('20200315',46 ,null,8   ,0  ,0  ,55),--Colombia
('20200315',168,null,15  ,0  ,0  ,55),--Peru
('20200315',50 ,null,0   ,0  ,0  ,55),--Costa Rica
('20200315',167,null,0   ,0  ,0  ,55),--Paraguay
('20200315',165,null,0   ,0  ,0  ,55),--Panama
('20200315',25 ,null,0   ,0  ,0  ,55),--Bolivia
('20200315',109,null,1   ,0  ,0  ,55),--Jamaica
('20200315',96 ,null,0   ,0  ,0  ,55),--Honduras
('20200315',93 ,null,0   ,0  ,0  ,55),--Guyana
('20200315',203,null,0   ,0  ,0  ,55),--St. Vicent Grenadines
('20200315',52 ,null,0   ,0  ,0  ,55),--Cuba
('20200315',172,null,0   ,0  ,0  ,55),--Puerto rico
('20200315',232,null,0   ,0  ,0  ,55),--Venezuela
('20200315',8  ,null,0   ,0  ,0  ,55),--Antigua and barbuda
('20200315',216,null,0   ,0  ,0  ,55),--Trinidad and Tobago

('20200315',183,null,0   ,0  ,0  ,55),--Saint Martin
('20200315',178,null,0   ,0  ,0  ,55),--Saint Barthelemy
('20200315',77 ,null,1   ,0  ,0  ,55),--French Guiana
('20200315',133,null,4   ,0  ,0  ,55),--Martinique
('20200315',40 ,null,0   ,0  ,0  ,55),--Cayman Islands
('20200315',88 ,null,2   ,0  ,0  ,55),--Guadeloupe
('20200315',53 ,null,2   ,0  ,0  ,55),--Cura�ao


('20200315',3  ,null,11  ,1  ,0  ,55),--Algeria
('20200315',189,null,11  ,0  ,0  ,55),--Senegal 
('20200315',155,null,0   ,0  ,0  ,55),--Nigeria
('20200315',36, null,1   ,0  ,0  ,55),--Cameroon
('20200315',199,null,21  ,0  ,0  ,55),--South Africa
('20200315',213,null,0   ,0  ,0  ,55),--Togo
('20200315',32 ,null,1   ,0  ,0  ,55),--Burkina Faso
('20200315',63 ,null,0   ,0  ,0  ,55),--DRC Congo
('20200315',56 ,null,2   ,0  ,0  ,55),--Cote Divore
('20200315',71 ,null,0   ,0  ,0  ,55),--Ethiopia
('20200315',79 ,null,0   ,0  ,0  ,55),--Gabon
('20200315',83 ,null,1   ,0  ,0  ,55),--Ghana
('20200315',91 ,null,0   ,0  ,0  ,55),--Guinea
('20200315',113,null,0   ,0  ,0  ,55),--Kenya
('20200315',147,null,2   ,0  ,0  ,55),--Namibia
('20200315',41 ,null,1   ,0  ,0  ,55),--Central African Republic
('20200315',48 ,null,1   ,0  ,0  ,55),--Congo
('20200315',67 ,null,1   ,0  ,0  ,55),--Equatorial Guinea
('20200315',70 ,null,1   ,0  ,0  ,55),--Eswatini
('20200315',134,null,1   ,0  ,0  ,55),--Mauritania

('20200315',136,null,1   ,0  ,0  ,55),--Mayotte
('20200315',177,null,1   ,0  ,0  ,55),--Reunion
('20200315',58 ,null,0   ,0  ,0  ,55)--Diamond Princess




INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200316',45 ,null,29  ,14 ,0  ,56), --China
('20200316',181,null,74  ,0  ,0  ,56),--Republic of korea
('20200316',110,null,34  ,2  ,0  ,56),--Japan
('20200316',193,null,31  ,0  ,0  ,56),--Singapore
('20200316',12 ,null,0   ,0  ,0  ,56),--Australia
('20200316',128,null,315 ,0  ,0  ,56),--Malaysia
('20200316',233,null,4   ,0  ,0  ,56),--Vietnam
('20200316',169,null,0   ,0  ,0  ,56),--Philippines
('20200316',152,null,0   ,0  ,0  ,56),--New Zealand
('20200316',35 ,null,0   ,0  ,0  ,56),--Cambodia
('20200316',30 ,null,10  ,0  ,0  ,56),--Brunei
('20200316',141,null,0   ,0  ,0  ,56),--Mongolia

('20200316',78 ,null,0   ,0  ,0  ,56),--French Polynesia

('20200316',107,null,3590,368,0  ,56),--Italy
('20200316',76 ,null,911 ,36 ,0  ,56),--France
('20200316',82 ,null,1043,4  ,0  ,56),--Germany
('20200316',200,null,2000,152,0  ,56),--Spain
('20200316',225,null,251 ,14 ,0  ,56),--UK (United Kingdom)
('20200316',207,null,841 ,2  ,0  ,56),--Switzerland
('20200316',160,null,170 ,0  ,0  ,56),--Norway
('20200316',150,null,176 ,8  ,0  ,56),--Netherlands
('20200316',13 ,null,159 ,0  ,0  ,56),--Austria
('20200316',206,null,68  ,3  ,0  ,56),--Sweden
('20200316',99 ,null,0   ,0  ,0  ,56),--Iceland
('20200316',20 ,null,396 ,5  ,0  ,56),--Belgium
('20200316',186,null,0   ,0  ,0  ,56),--San Marino
('20200316',106,null,22  ,0  ,0  ,56),--Israel
('20200316',57 ,null,71  ,1  ,0  ,56),--Denmark
('20200316',51 ,null,12  ,0  ,0  ,56),--Croatia

('20200316',85 ,null,103 ,2  ,0  ,56),--Greece
('20200316',75 ,null,57  ,0  ,0  ,56),--Finland
('20200316',171,null,133 ,0  ,0  ,56),--Portugal
('20200316',19 ,null,15  ,0  ,0  ,56),--Belarus
('20200316',55 ,null,84  ,0  ,0  ,56),--Czechia
('20200316',174,null,35  ,0  ,0  ,56),--Romania
('20200316',14 ,null,0   ,0  ,0  ,56),--Azerbaijan
('20200316',81 ,null,3   ,0  ,0  ,56),--Georgia
('20200316',175,null,29  ,0  ,0  ,56),--Russia
('20200316',26 ,null,0   ,0  ,0  ,56),--Bosnia and Herzegovina
('20200316',2  ,null,4   ,0  ,0  ,56),--Albania
('20200316',69 ,null,126 ,0  ,0  ,56),--Estonia
('20200316',98 ,null,7   ,1  ,0  ,56),--Hungary
('20200316',104,null,40  ,0  ,0  ,56),--Ireland
('20200316',5  ,null,0   ,0  ,0  ,56),--Andorra
('20200316',10 ,null,18  ,0  ,0  ,56),--Armenia
('20200316',118,null,1   ,0  ,0  ,56),--Latvia
('20200316',123,null,5   ,0  ,0  ,56),--Lithuania
('20200316',124,null,0   ,0  ,0  ,56),--Luxembourg
('20200316',140,null,7   ,0  ,0  ,56),--Monaco
('20200316',158,null,0   ,0  ,0  ,56),--North Macedonia
('20200316',170,null,39  ,0  ,0  ,56),--Poland
('20200316',196,null,78  ,0  ,0  ,56),--Slovenia
('20200316',226,null,0   ,0  ,0  ,56),--Ukraine
('20200316',122,null,3   ,0  ,0  ,56),--Liechtenstein
('20200316',95 ,null,0   ,0  ,0  ,56),--Holy see
('20200316',195,null,17  ,0  ,0  ,56),--Slovakia
('20200316',31 ,null,8   ,0  ,0  ,56),--Bulgaria
('20200316',131,null,9   ,0  ,0  ,56),--Malta
('20200316',139,null,11  ,0  ,0  ,56),--Moldova
('20200316',54 ,null,12  ,0  ,0  ,56),--Cyprus
('20200316',190,null,0   ,0  ,0  ,56),--Serbia
('20200316',218,null,0   ,0  ,0  ,56),--Turkey
('20200316',112,null,0   ,0  ,0  ,56),--Kazakhstan
('20200316',229,null,4   ,0  ,0  ,56),--Kazakhstan

('20200316',72 ,null,2   ,0  ,0  ,56),--Faeroe Islands
('20200316',239,null,0   ,0  ,0  ,56),--Guernsey
('20200316',240,null,0   ,0  ,0  ,56),--Jersey

('20200316',211,null,39  ,0  ,0  ,56),--Thailand
('20200316',100,null,7   ,0  ,0  ,56),--India
('20200316',101,null,0   ,0  ,0  ,56),--Indonesia
('20200316',149,null,0   ,0  ,0  ,56),--Nepal
('20200316',201,null,8   ,0  ,0  ,56),--Sri Lanka
('20200316',129,null,3   ,0  ,0  ,56),--Maldives
('20200316',17 ,null,2   ,0  ,0  ,56),--Bangladesh

('20200316',102,null,2262,245,0  ,56),--Iran
('20200316',115,null,0   ,0  ,0  ,56),--Kuwait
('20200316',16 ,null,10  ,1  ,0  ,56),--Bahrain
('20200316',103,null,0   ,0  ,0  ,56),--Iraq
('20200316',223,null,13  ,0  ,0  ,56),--UAE (United Arab Emirates)
('20200316',161,null,2   ,0  ,0  ,56),--Oman
('20200316',119,null,6   ,0  ,0  ,56),--lebanon
('20200316',173,null,64  ,0  ,0  ,56),--Qatar
('20200316',162,null,24  ,0  ,0  ,56),--Pakistan
('20200316',65 ,null,31  ,0  ,0  ,56),--Egypt
('20200316',144,null,10  ,0  ,0  ,56),--Morocco
('20200316',188,null,0   ,0  ,0  ,56),--Saudi Arabia
('20200316',1  ,null,6   ,0  ,0  ,56),--Afghanistan
('20200316',111,null,5   ,0  ,0  ,56),--Jordan
('20200316',217,null,2   ,0  ,0  ,56),--Tunisia
('20200316',204,null,0   ,1  ,0  ,56),--Sudan

('20200316',164,null,0   ,0  ,0  ,56),--Palestine

('20200316',228,null,0   ,0  ,0  ,56),--USA
('20200316',37 ,null,60  ,0  ,0  ,56),--Canada
('20200316',64 ,null,14  ,0  ,0  ,56),--Ecuador
('20200316',137,null,12  ,0  ,0  ,56),--Mexico
('20200316',28 ,null,79  ,0  ,0  ,56),--Brasil
('20200316',9  ,null,11  ,0  ,0  ,56),--Argentina
('20200316',44 ,null,14  ,0  ,0  ,56),--Chile
('20200316',62 ,null,0   ,0  ,0  ,56),--Dominican Republic
('20200316',46 ,null,0   ,0  ,0  ,56),--Colombia
('20200316',168,null,28  ,0  ,0  ,56),--Peru
('20200316',50 ,null,0   ,0  ,0  ,56),--Costa Rica
('20200316',167,null,2   ,0  ,0  ,56),--Paraguay
('20200316',165,null,16  ,0  ,0  ,56),--Panama
('20200316',25 ,null,8   ,0  ,0  ,56),--Bolivia
('20200316',109,null,2   ,0  ,0  ,56),--Jamaica
('20200316',96 ,null,0   ,0  ,0  ,56),--Honduras
('20200316',93 ,null,3   ,0  ,0  ,56),--Guyana
('20200316',203,null,0   ,0  ,0  ,56),--St. Vicent Grenadines
('20200316',52 ,null,0   ,0  ,0  ,56),--Cuba
('20200316',172,null,0   ,0  ,0  ,56),--Puerto rico
('20200316',232,null,0   ,0  ,0  ,56),--Venezuela
('20200316',8  ,null,0   ,0  ,0  ,56),--Antigua and barbuda
('20200316',216,null,1   ,0  ,0  ,56),--Trinidad and Tobago
('20200316',227,null,4   ,0  ,0  ,56),--Uruguay

('20200316',183,null,0   ,0  ,0  ,56),--Saint Martin
('20200316',178,null,2   ,0  ,0  ,56),--Saint Barthelemy
('20200316',77 ,null,0   ,0  ,0  ,56),--French Guiana
('20200316',133,null,5   ,0  ,0  ,56),--Martinique
('20200316',40 ,null,0   ,1  ,0  ,56),--Cayman Islands
('20200316',88 ,null,3   ,0  ,0  ,56),--Guadeloupe
('20200316',53 ,null,0   ,0  ,0  ,56),--Cura�ao


('20200316',3  ,null,12  ,0  ,0  ,56),--Algeria
('20200316',189,null,5   ,0  ,0  ,56),--Senegal 
('20200316',155,null,0   ,0  ,0  ,56),--Nigeria
('20200316',36, null,0   ,0  ,0  ,56),--Cameroon
('20200316',199,null,13  ,0  ,0  ,56),--South Africa
('20200316',213,null,0   ,0  ,0  ,56),--Togo
('20200316',32 ,null,0   ,0  ,0  ,56),--Burkina Faso
('20200316',63 ,null,0   ,0  ,0  ,56),--DRC Congo
('20200316',56 ,null,0   ,0  ,0  ,56),--Cote Divore
('20200316',71 ,null,0   ,0  ,0  ,56),--Ethiopia
('20200316',79 ,null,0   ,0  ,0  ,56),--Gabon
('20200316',83 ,null,0   ,0  ,0  ,56),--Ghana
('20200316',91 ,null,0   ,0  ,0  ,56),--Guinea
('20200316',113,null,0   ,0  ,0  ,56),--Kenya
('20200316',147,null,0   ,0  ,0  ,56),--Namibia
('20200316',41 ,null,0   ,0  ,0  ,56),--Central African Republic
('20200316',48 ,null,0   ,0  ,0  ,56),--Congo
('20200316',67 ,null,0   ,0  ,0  ,56),--Equatorial Guinea
('20200316',70 ,null,0   ,0  ,0  ,56),--Eswatini
('20200316',134,null,0   ,0  ,0  ,56),--Mauritania
('20200316',176,null,5   ,0  ,0  ,56),--Rwanda
('20200316',191,null,2   ,0  ,0  ,56),--Seychelles

('20200316',136,null,0   ,0  ,0  ,56),--Mayotte
('20200316',177,null,3   ,0  ,0  ,56),--Reunion

('20200316',58 ,null,15   ,0  ,0  ,56)--Diamond Princess




INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200317',45 ,null,39  ,13 ,0  ,57), --China
('20200317',181,null,84  ,6  ,0  ,57),--Republic of korea
('20200317',110,null,15  ,4  ,0  ,57),--Japan
('20200317',193,null,0   ,0  ,0  ,57),--Singapore
('20200317',12 ,null,77  ,0  ,0  ,57),--Australia
('20200317',128,null,0   ,0  ,0  ,57),--Malaysia
('20200317',233,null,4   ,0  ,0  ,57),--Vietnam
('20200317',169,null,47  ,0  ,0  ,57),--Philippines
('20200317',152,null,5   ,0  ,0  ,57),--New Zealand
('20200317',35 ,null,12  ,0  ,0  ,57),--Cambodia
('20200317',30 ,null,10  ,0  ,0  ,57),--Brunei
('20200317',141,null,3   ,0  ,0  ,57),--Mongolia

('20200317',78 ,null,0   ,0  ,0  ,57),--French Polynesia
('20200317',89 ,null,3   ,0  ,0  ,57),--Guam

('20200317',107,null,3233,349,0  ,57),--Italy
('20200317',76 ,null,1193,21 ,0  ,57),--France
('20200317',82 ,null,1174,1  ,0  ,57),--Germany
('20200317',200,null,1438,21 ,0  ,57),--Spain
('20200317',225,null,152 ,20 ,0  ,57),--UK (United Kingdom)
('20200317',207,null,0   ,1  ,0  ,57),--Switzerland
('20200317',160,null,92  ,2  ,0  ,57),--Norway
('20200317',150,null,278 ,4  ,0  ,57),--Netherlands
('20200317',13 ,null,173 ,2  ,0  ,57),--Austria
('20200317',206,null,67  ,0  ,0  ,57),--Sweden
('20200317',99 ,null,19  ,0  ,0  ,57),--Iceland
('20200317',20 ,null,0   ,0  ,0  ,57),--Belgium
('20200317',186,null,10  ,4  ,0  ,57),--San Marino
('20200317',106,null,50  ,0  ,0  ,57),--Israel
('20200317',57 ,null,62  ,3  ,0  ,57),--Denmark
('20200317',51 ,null,7   ,0  ,0  ,57),--Croatia

('20200317',85 ,null,0   ,0  ,0  ,57),--Greece
('20200317',75 ,null,5   ,0  ,0  ,57),--Finland
('20200317',171,null,86  ,0  ,0  ,57),--Portugal
('20200317',19 ,null,0   ,0  ,0  ,57),--Belarus
('20200317',55 ,null,85  ,0  ,0  ,57),--Czechia
('20200317',174,null,0   ,0  ,0  ,57),--Romania
('20200317',14 ,null,0   ,0  ,0  ,57),--Azerbaijan
('20200317',81 ,null,0   ,0  ,0  ,57),--Georgia
('20200317',175,null,30  ,0  ,0  ,57),--Russia
('20200317',26 ,null,0   ,0  ,0  ,57),--Bosnia and Herzegovina
('20200317',2  ,null,9   ,0  ,0  ,57),--Albania
('20200317',69 ,null,0   ,0  ,0  ,57),--Estonia
('20200317',98 ,null,11  ,0  ,0  ,57),--Hungary
('20200317',104,null,54  ,0  ,0  ,57),--Ireland
('20200317',5  ,null,12  ,0  ,0  ,57),--Andorra
('20200317',10 ,null,26  ,0  ,0  ,57),--Armenia
('20200317',118,null,5   ,0  ,0  ,57),--Latvia
('20200317',123,null,3   ,0  ,0  ,57),--Lithuania
('20200317',124,null,43  ,0  ,0  ,57),--Luxembourg
('20200317',140,null,0   ,0  ,0  ,57),--Monaco
('20200317',158,null,6   ,0  ,0  ,57),--North Macedonia
('20200317',170,null,0   ,0  ,0  ,57),--Poland
('20200317',196,null,34  ,0  ,0  ,57),--Slovenia
('20200317',226,null,4   ,0  ,0  ,57),--Ukraine
('20200317',122,null,0   ,0  ,0  ,57),--Liechtenstein
('20200317',95 ,null,0   ,0  ,0  ,57),--Holy see
('20200317',195,null,11  ,0  ,0  ,57),--Slovakia
('20200317',31 ,null,16  ,0  ,0  ,57),--Bulgaria
('20200317',131,null,9   ,0  ,0  ,57),--Malta
('20200317',139,null,6   ,0  ,0  ,57),--Moldova
('20200317',54 ,null,0   ,0  ,0  ,57),--Cyprus
('20200317',190,null,24  ,0  ,0  ,57),--Serbia
('20200317',218,null,42  ,0  ,0  ,57),--Turkey
('20200317',112,null,0   ,0  ,0  ,57),--Kazakhstan
('20200317',229,null,0   ,0  ,0  ,57),--Uzbekistan

('20200317',72 ,null,36   ,0  ,0  ,57),--Faeroe Islands
('20200317',239,null,0   ,0  ,0  ,57),--Guernsey
('20200317',240,null,0   ,0  ,0  ,57),--Jersey
('20200317',84 ,null,2   ,0  ,0  ,57),--Gibraltar


('20200317',211,null,33  ,0  ,0  ,57),--Thailand
('20200317',100,null,23  ,1  ,0  ,57),--India
('20200317',101,null,55  ,1  ,0  ,57),--Indonesia
('20200317',149,null,0   ,0  ,0  ,57),--Nepal
('20200317',201,null,10  ,0  ,0  ,57),--Sri Lanka
('20200317',129,null,0   ,0  ,0  ,57),--Maldives
('20200317',17 ,null,3   ,0  ,0  ,57),--Bangladesh

('20200317',102,null,0   ,0  ,0  ,57),--Iran
('20200317',115,null,18  ,0  ,0  ,57),--Kuwait
('20200317',16 ,null,8   ,0  ,0  ,57),--Bahrain
('20200317',103,null,0   ,0  ,0  ,57),--Iraq
('20200317',223,null,0   ,0  ,0  ,57),--UAE (United Arab Emirates)
('20200317',161,null,2   ,0  ,0  ,57),--Oman
('20200317',119,null,10  ,0  ,0  ,57),--lebanon
('20200317',173,null,38  ,0  ,0  ,57),--Qatar
('20200317',162,null,135 ,0  ,0  ,57),--Pakistan
('20200317',65 ,null,40  ,2  ,0  ,57),--Egypt
('20200317',144,null,10  ,1  ,0  ,57),--Morocco
('20200317',188,null,30  ,0  ,0  ,57),--Saudi Arabia
('20200317',1  ,null,5   ,0  ,0  ,57),--Afghanistan
('20200317',111,null,29  ,0  ,0  ,57),--Jordan
('20200317',217,null,2   ,0  ,0  ,57),--Tunisia
('20200317',204,null,0   ,0  ,0  ,57),--Sudan
('20200317',198,null,1   ,0  ,0  ,57),--Somalia

('20200317',164,null,1   ,0  ,0  ,57),--Palestine

('20200317',228,null,1825,17 ,0  ,57),--USA
('20200317',37 ,null,120 ,0  ,0  ,57),--Canada
('20200317',64 ,null,21  ,0  ,0  ,57),--Ecuador
('20200317',137,null,0   ,0  ,0  ,57),--Mexico
('20200317',28 ,null,34  ,0  ,0  ,57),--Brasil
('20200317',9  ,null,9   ,0  ,0  ,57),--Argentina
('20200317',44 ,null,81  ,0  ,0  ,57),--Chile
('20200317',62 ,null,16  ,1  ,0  ,57),--Dominican Republic
('20200317',46 ,null,21  ,0  ,0  ,57),--Colombia
('20200317',168,null,15  ,0  ,0  ,57),--Peru
('20200317',50 ,null,18  ,0  ,0  ,57),--Costa Rica
('20200317',167,null,1   ,0  ,0  ,57),--Paraguay
('20200317',165,null,26  ,0  ,0  ,57),--Panama
('20200317',25 ,null,0   ,0  ,0  ,57),--Bolivia
('20200317',109,null,0   ,0  ,0  ,57),--Jamaica
('20200317',96 ,null,6   ,0  ,0  ,57),--Honduras
('20200317',93 ,null,0   ,0  ,0  ,57),--Guyana
('20200317',203,null,0   ,0  ,0  ,57),--St. Vicent Grenadines
('20200317',52 ,null,1   ,0  ,0  ,57),--Cuba
('20200317',172,null,0   ,0  ,0  ,57),--Puerto rico
('20200317',232,null,16  ,0  ,0  ,57),--Venezuela
('20200317',8  ,null,0   ,0  ,0  ,57),--Antigua and barbuda
('20200317',216,null,3   ,0  ,0  ,57),--Trinidad and Tobago
('20200317',227,null,2   ,0  ,0  ,57),--Uruguay
('20200317',182,null,2   ,0  ,0  ,57),--Saint lucia
('20200317',15 ,null,1   ,0  ,0  ,57),--Bahamas
('20200317',90 ,null,1   ,0  ,0  ,57),--Guatemala
('20200317',205,null,1   ,0  ,0  ,57),--suriname

('20200317',183,null,0   ,0  ,0  ,57),--Saint Martin
('20200317',178,null,0   ,0  ,0  ,57),--Saint Barthelemy
('20200317',77 ,null,0   ,0  ,0  ,57),--French Guiana
('20200317',133,null,1   ,0  ,0  ,57),--Martinique
('20200317',40 ,null,0   ,0  ,0  ,57),--Cayman Islands
('20200317',88 ,null,12  ,0  ,0  ,57),--Guadeloupe
('20200317',53 ,null,1   ,0  ,0  ,57),--Cura�ao
('20200317',222,null,2   ,0  ,0  ,57),--US Virgin Islands
('20200317',11 ,null,2   ,0  ,0  ,57),--Aruba


('20200317',3  ,null,11  ,1  ,0  ,57),--Algeria
('20200317',189,null,1   ,0  ,0  ,57),--Senegal 
('20200317',155,null,0   ,0  ,0  ,57),--Nigeria
('20200317',36, null,2   ,0  ,0  ,57),--Cameroon
('20200317',199,null,11  ,0  ,0  ,57),--South Africa
('20200317',213,null,0   ,0  ,0  ,57),--Togo
('20200317',32 ,null,0   ,0  ,0  ,57),--Burkina Faso
('20200317',63 ,null,1   ,0  ,0  ,57),--DRC Congo
('20200317',56 ,null,3   ,0  ,0  ,57),--Cote Divore
('20200317',71 ,null,4   ,0  ,0  ,57),--Ethiopia
('20200317',79 ,null,0   ,0  ,0  ,57),--Gabon
('20200317',83 ,null,0   ,0  ,0  ,57),--Ghana
('20200317',91 ,null,0   ,0  ,0  ,57),--Guinea
('20200317',113,null,2   ,0  ,0  ,57),--Kenya
('20200317',147,null,0   ,0  ,0  ,57),--Namibia
('20200317',41 ,null,0   ,0  ,0  ,57),--Central African Republic
('20200317',48 ,null,0   ,0  ,0  ,57),--Congo
('20200317',67 ,null,0   ,0  ,0  ,57),--Equatorial Guinea
('20200317',70 ,null,0   ,0  ,0  ,57),--Eswatini
('20200317',134,null,0   ,0  ,0  ,57),--Mauritania
('20200317',176,null,2   ,0  ,0  ,57),--Rwanda
('20200317',191,null,2   ,0  ,0  ,57),--Seychelles
('20200317',22 ,null,1   ,0  ,0  ,57),--Benin
('20200317',121,null,1   ,0  ,0  ,57),--Liberia
('20200317',210,null,1   ,0  ,0  ,57),--Tanzania


('20200317',136,null,0   ,0  ,0  ,57),--Mayotte
('20200317',177,null,0   ,0  ,0  ,57),--Reunion

('20200317',58 ,null,0   ,0  ,0  ,57)--Diamond Princess




INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200318',45 ,null,39  ,13 ,0  ,58), --China
('20200318',181,null,84  ,6  ,0  ,58),--Republic of korea
('20200318',110,null,15  ,4  ,0  ,58),--Japan
('20200318',193,null,0   ,0  ,0  ,58),--Singapore
('20200318',12 ,null,77  ,0  ,0  ,58),--Australia
('20200318',128,null,0   ,0  ,0  ,58),--Malaysia
('20200318',233,null,4   ,0  ,0  ,58),--Vietnam
('20200318',169,null,47  ,0  ,0  ,58),--Philippines
('20200318',152,null,5   ,0  ,0  ,58),--New Zealand
('20200318',35 ,null,12  ,0  ,0  ,58),--Cambodia
('20200318',30 ,null,10  ,0  ,0  ,58),--Brunei
('20200318',141,null,3   ,0  ,0  ,58),--Mongolia

('20200318',78 ,null,0   ,0  ,0  ,58),--French Polynesia
('20200318',89 ,null,3   ,0  ,0  ,58),--Guam

('20200318',107,null,3526,345,0  ,58),--Italy
('20200318',76 ,null,1079,27 ,0  ,58),--France
('20200318',82 ,null,1144,0  ,0  ,58),--Germany
('20200318',200,null,1987,182,0  ,58),--Spain
('20200318',225,null,407 ,5  ,0  ,58),--UK (United Kingdom)
('20200318',207,null,450 ,5  ,0  ,58),--Switzerland
('20200318',160,null,139 ,0  ,0  ,58),--Norway
('20200318',150,null,292 ,19 ,0  ,58),--Netherlands
('20200318',13 ,null,373 ,2  ,0  ,58),--Austria
('20200318',206,null,108 ,0  ,0  ,58),--Sweden
('20200318',99 ,null,45  ,0  ,0  ,58),--Iceland
('20200318',20 ,null,401 ,9  ,0  ,58),--Belgium
('20200318',186,null,2   ,2  ,0  ,58),--San Marino
('20200318',106,null,0   ,0  ,0  ,58),--Israel
('20200318',57 ,null,79  ,3  ,0  ,58),--Denmark
('20200318',51 ,null,9   ,0  ,0  ,58),--Croatia

('20200318',85 ,null,56  ,0  ,0  ,58),--Greece
('20200318',75 ,null,47  ,0  ,0  ,58),--Finland
('20200318',171,null,117 ,1  ,0  ,58),--Portugal
('20200318',19 ,null,0   ,0  ,0  ,58),--Belarus
('20200318',55 ,null,136 ,0  ,0  ,58),--Czechia
('20200318',174,null,26  ,0  ,0  ,58),--Romania
('20200318',14 ,null,0   ,0  ,0  ,58),--Azerbaijan
('20200318',81 ,null,1   ,0  ,0  ,58),--Georgia
('20200318',175,null,0   ,0  ,0  ,58),--Russia
('20200318',26 ,null,0   ,0  ,0  ,58),--Bosnia and Herzegovina
('20200318',2  ,null,4   ,0  ,0  ,58),--Albania
('20200318',69 ,null,20  ,0  ,0  ,58),--Estonia
('20200318',98 ,null,11  ,0  ,0  ,58),--Hungary
('20200318',104,null,69  ,0  ,0  ,58),--Ireland
('20200318',5  ,null,2   ,0  ,0  ,58),--Andorra
('20200318',10 ,null,0   ,0  ,0  ,58),--Armenia
('20200318',118,null,24  ,0  ,0  ,58),--Latvia
('20200318',123,null,8   ,0  ,0  ,58),--Lithuania
('20200318',124,null,56  ,0  ,0  ,58),--Luxembourg
('20200318',140,null,0   ,0  ,0  ,58),--Monaco
('20200318',158,null,12  ,0  ,0  ,58),--North Macedonia
('20200318',170,null,96  ,2  ,0  ,58),--Poland
('20200318',196,null,22  ,1  ,0  ,58),--Slovenia
('20200318',226,null,9   ,1  ,0  ,58),--Ukraine
('20200318',122,null,0   ,0  ,0  ,58),--Liechtenstein
('20200318',95 ,null,0   ,0  ,0  ,58),--Holy see
('20200318',195,null,25  ,0  ,0  ,58),--Slovakia
('20200318',31 ,null,14  ,0  ,0  ,58),--Bulgaria
('20200318',131,null,8   ,0  ,0  ,58),--Malta
('20200318',139,null,1   ,0  ,0  ,58),--Moldova
('20200318',54 ,null,0   ,0  ,0  ,58),--Cyprus
('20200318',190,null,23  ,0  ,0  ,58),--Serbia
('20200318',218,null,0   ,0  ,0  ,58),--Turkey
('20200318',112,null,27  ,0  ,0  ,58),--Kazakhstan
('20200318',229,null,0   ,0  ,0  ,58),--Uzbekistan
('20200318',142,null,2   ,0  ,0  ,58),--Montenegro

('20200318',72 ,null,36  ,0  ,0  ,58),--Faeroe Islands
('20200318',239,null,0   ,0  ,0  ,58),--Guernsey
('20200318',240,null,3   ,0  ,0  ,58),--Jersey
('20200318',84 ,null,0   ,0  ,0  ,58),--Gibraltar


('20200318',211,null,30  ,0  ,0  ,58),--Thailand
('20200318',100,null,23  ,1  ,0  ,58),--India
('20200318',101,null,0   ,0  ,0  ,58),--Indonesia
('20200318',149,null,0   ,0  ,0  ,58),--Nepal
('20200318',201,null,10  ,0  ,0  ,58),--Sri Lanka
('20200318',129,null,0   ,0  ,0  ,58),--Maldives
('20200318',17 ,null,0   ,0  ,0  ,58),--Bangladesh

('20200318',102,null,1178,135,0  ,58),--Iran
('20200318',115,null,7   ,0  ,0  ,58),--Kuwait
('20200318',16 ,null,16  ,0  ,0  ,58),--Bahrain
('20200318',103,null,30  ,2  ,0  ,58),--Iraq
('20200318',223,null,0   ,0  ,0  ,58),--UAE (United Arab Emirates)
('20200318',161,null,2   ,0  ,0  ,58),--Oman
('20200318',119,null,21  ,0  ,0  ,58),--lebanon
('20200318',173,null,41  ,0  ,0  ,58),--Qatar
('20200318',162,null,134 ,0  ,0  ,58),--Pakistan
('20200318',65 ,null,40  ,2  ,0  ,58),--Egypt
('20200318',144,null,9   ,1  ,0  ,58),--Morocco
('20200318',188,null,38  ,0  ,0  ,58),--Saudi Arabia
('20200318',1  ,null,1   ,0  ,0  ,58),--Afghanistan
('20200318',111,null,29  ,0  ,0  ,58),--Jordan
('20200318',217,null,4   ,0  ,0  ,58),--Tunisia
('20200318',204,null,0   ,0  ,0  ,58),--Sudan
('20200318',198,null,0   ,0  ,0  ,58),--Somalia

('20200318',164,null,2   ,0  ,0  ,58),--Palestine

('20200318',228,null,1822,17 ,0  ,58),--USA
('20200318',37 ,null,120 ,0  ,0  ,58),--Canada
('20200318',64 ,null,21  ,0  ,0  ,58),--Ecuador
('20200318',137,null,29  ,0  ,0  ,58),--Mexico
('20200318',28 ,null,34  ,0  ,0  ,58),--Brasil
('20200318',9  ,null,9   ,0  ,0  ,58),--Argentina
('20200318',44 ,null,81  ,0  ,0  ,58),--Chile
('20200318',62 ,null,16  ,1  ,0  ,58),--Dominican Republic
('20200318',46 ,null,11  ,0  ,0  ,58),--Colombia
('20200318',168,null,15  ,0  ,0  ,58),--Peru
('20200318',50 ,null,6   ,0  ,0  ,58),--Costa Rica
('20200318',167,null,1   ,0  ,0  ,58),--Paraguay
('20200318',165,null,26  ,0  ,0  ,58),--Panama
('20200318',25 ,null,0   ,0  ,0  ,58),--Bolivia
('20200318',109,null,2   ,0  ,0  ,58),--Jamaica
('20200318',96 ,null,6   ,0  ,0  ,58),--Honduras
('20200318',93 ,null,0   ,0  ,0  ,58),--Guyana
('20200318',203,null,0   ,0  ,0  ,58),--St. Vicent Grenadines
('20200318',52 ,null,1   ,0  ,0  ,58),--Cuba
('20200318',172,null,0   ,0  ,0  ,58),--Puerto rico
('20200318',232,null,16  ,0  ,0  ,58),--Venezuela
('20200318',8  ,null,0   ,0  ,0  ,58),--Antigua and barbuda
('20200318',216,null,3   ,0  ,0  ,58),--Trinidad and Tobago
('20200318',227,null,2   ,0  ,0  ,58),--Uruguay
('20200318',182,null,0   ,0  ,0  ,58),--Saint lucia
('20200318',15 ,null,1   ,0  ,0  ,58),--Bahamas
('20200318',90 ,null,5   ,0  ,0  ,58),--Guatemala
('20200318',205,null,0   ,0  ,0  ,58),--suriname

('20200318',183,null,0   ,0  ,0  ,58),--Saint Martin
('20200318',178,null,0   ,0  ,0  ,58),--Saint Barthelemy
('20200318',77 ,null,0   ,0  ,0  ,58),--French Guiana
('20200318',133,null,0   ,0  ,0  ,58),--Martinique
('20200318',40 ,null,0   ,0  ,0  ,58),--Cayman Islands
('20200318',88 ,null,12  ,0  ,0  ,58),--Guadeloupe
('20200318',53 ,null,1   ,0  ,0  ,58),--Cura�ao
('20200318',222,null,1   ,0  ,0  ,58),--US Virgin Islands
('20200318',11 ,null,2   ,0  ,0  ,58),--Aruba


('20200318',3  ,null,11  ,0  ,0  ,58),--Algeria
('20200318',189,null,1   ,0  ,0  ,58),--Senegal 
('20200318',155,null,0   ,0  ,0  ,58),--Nigeria
('20200318',36, null,0   ,0  ,0  ,58),--Cameroon
('20200318',199,null,11  ,0  ,0  ,58),--South Africa
('20200318',213,null,0   ,0  ,0  ,58),--Togo
('20200318',32 ,null,5   ,0  ,0  ,58),--Burkina Faso
('20200318',63 ,null,1   ,0  ,0  ,58),--DRC Congo
('20200318',56 ,null,3   ,0  ,0  ,58),--Cote Divore
('20200318',71 ,null,1   ,0  ,0  ,58),--Ethiopia
('20200318',79 ,null,0   ,0  ,0  ,58),--Gabon
('20200318',83 ,null,4   ,0  ,0  ,58),--Ghana
('20200318',91 ,null,0   ,0  ,0  ,58),--Guinea
('20200318',113,null,0   ,0  ,0  ,58),--Kenya
('20200318',147,null,0   ,0  ,0  ,58),--Namibia
('20200318',41 ,null,0   ,0  ,0  ,58),--Central African Republic
('20200318',48 ,null,0   ,0  ,0  ,58),--Congo
('20200318',67 ,null,0   ,0  ,0  ,58),--Equatorial Guinea
('20200318',70 ,null,0   ,0  ,0  ,58),--Eswatini
('20200318',134,null,0   ,0  ,0  ,58),--Mauritania
('20200318',176,null,2   ,0  ,0  ,58),--Rwanda
('20200318',191,null,0   ,0  ,0  ,58),--Seychelles
('20200318',22 ,null,1   ,0  ,0  ,58),--Benin
('20200318',121,null,1   ,0  ,0  ,58),--Liberia
('20200318',210,null,1   ,0  ,0  ,58),--Tanzania


('20200318',136,null,0   ,0  ,0  ,58),--Mayotte
('20200318',177,null,0   ,0  ,0  ,58),--Reunion

('20200318',58 ,null,0   ,0  ,0  ,58)--Diamond Princess





INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200319',45 ,null,58  ,11 ,0  ,59), --China
('20200319',181,null,93  ,3  ,0  ,59),--Republic of korea
('20200319',110,null,44  ,1  ,0  ,59),--Japan
('20200319',193,null,47  ,0  ,0  ,59),--Singapore
('20200319',12 ,null,96  ,1  ,0  ,59),--Australia
('20200319',128,null,120 ,2  ,0  ,59),--Malaysia
('20200319',233,null,5   ,0  ,0  ,59),--Vietnam
('20200319',169,null,0   ,2  ,0  ,59),--Philippines
('20200319',152,null,9   ,0  ,0  ,59),--New Zealand
('20200319',35 ,null,11  ,0  ,0  ,59),--Cambodia
('20200319',30 ,null,2   ,0  ,0  ,59),--Brunei
('20200319',141,null,1   ,0  ,0  ,59),--Mongolia

('20200319',78 ,null,0   ,0  ,0  ,59),--French Polynesia
('20200319',89 ,null,2   ,0  ,0  ,59),--Guam

('20200319',107,null,4207,473,0  ,59),--Italy
('20200319',76 ,null,0   ,0  ,0  ,59),--France
('20200319',82 ,null,1042,0  ,0  ,59),--Germany
('20200319',200,null,2538,107,0  ,59),--Spain
('20200319',225,null,672 ,0  ,0  ,59),--UK (United Kingdom)
('20200319',207,null,353 ,2  ,0  ,59),--Switzerland
('20200319',160,null,115 ,0  ,0  ,59),--Norway
('20200319',150,null,0   ,0  ,0  ,59),--Netherlands
('20200319',13 ,null,314 ,1  ,0  ,59),--Austria
('20200319',206,null,112 ,0  ,0  ,59),--Sweden
('20200319',99 ,null,25  ,0  ,0  ,59),--Iceland
('20200319',20 ,null,0   ,0  ,0  ,59),--Belgium
('20200319',186,null,5   ,3  ,0  ,59),--San Marino
('20200319',106,null,0   ,0  ,0  ,59),--Israel
('20200319',57 ,null,67  ,0  ,0  ,59),--Denmark
('20200319',51 ,null,16  ,0  ,0  ,59),--Croatia

('20200319',85 ,null,0   ,0  ,0  ,59),--Greece
('20200319',75 ,null,40  ,0  ,0  ,59),--Finland
('20200319',171,null,194 ,1  ,0  ,59),--Portugal
('20200319',19 ,null,10  ,0  ,0  ,59),--Belarus
('20200319',55 ,null,30  ,0  ,0  ,59),--Czechia
('20200319',174,null,62  ,0  ,0  ,59),--Romania
('20200319',14 ,null,13  ,0  ,0  ,59),--Azerbaijan
('20200319',81 ,null,4   ,0  ,0  ,59),--Georgia
('20200319',175,null,54  ,0  ,0  ,59),--Russia
('20200319',26 ,null,7   ,0  ,0  ,59),--Bosnia and Herzegovina
('20200319',2  ,null,2   ,1  ,0  ,59),--Albania
('20200319',69 ,null,33  ,0  ,0  ,59),--Estonia
('20200319',98 ,null,8   ,0  ,0  ,59),--Hungary
('20200319',104,null,0   ,0  ,0  ,59),--Ireland
('20200319',5  ,null,23   ,0  ,0  ,59),--Andorra
('20200319',10 ,null,32  ,0  ,0  ,59),--Armenia
('20200319',118,null,11  ,0  ,0  ,59),--Latvia
('20200319',123,null,1   ,0  ,0  ,59),--Lithuania
('20200319',124,null,63  ,1  ,0  ,59),--Luxembourg
('20200319',140,null,0   ,0  ,0  ,59),--Monaco
('20200319',158,null,5   ,0  ,0  ,59),--North Macedonia
('20200319',170,null,0   ,0  ,0  ,59),--Poland
('20200319',196,null,0   ,0  ,0  ,59),--Slovenia
('20200319',226,null,7   ,0  ,0  ,59),--Ukraine
('20200319',122,null,18  ,0  ,0  ,59),--Liechtenstein
('20200319',95 ,null,0   ,0  ,0  ,59),--Holy see
('20200319',195,null,8   ,0  ,0  ,59),--Slovakia
('20200319',31 ,null,11  ,0  ,0  ,59),--Bulgaria
('20200319',131,null,10  ,0  ,0  ,59),--Malta
('20200319',139,null,0   ,0  ,0  ,59),--Moldova
('20200319',54 ,null,25  ,0  ,0  ,59),--Cyprus
('20200319',190,null,11  ,0  ,0  ,59),--Serbia
('20200319',218,null,51  ,1  ,0  ,59),--Turkey
('20200319',112,null,3   ,0  ,0  ,59),--Kazakhstan
('20200319',229,null,0   ,0  ,0  ,59),--Uzbekistan
('20200319',142,null,0   ,0  ,0  ,59),--Montenegro
('20200319',116,null,3   ,0  ,0  ,59),--Kyrgyzstan


('20200319',72 ,null,11   ,0  ,0  ,59),--Faeroe Islands
('20200319',239,null,0   ,0  ,0  ,59),--Guernsey
('20200319',240,null,0   ,0  ,0  ,59),--Jersey
('20200319',84 ,null,5   ,0  ,0  ,59),--Gibraltar



('20200319',211,null,35  ,0  ,0  ,59),--Thailand
('20200319',100,null,14  ,0  ,0  ,59),--India
('20200319',101,null,55  ,14 ,0  ,59),--Indonesia
('20200319',149,null,0   ,0  ,0  ,59),--Nepal
('20200319',201,null,13  ,0  ,0  ,59),--Sri Lanka
('20200319',129,null,0   ,0  ,0  ,59),--Maldives
('20200319',17 ,null,2   ,0  ,0  ,59),--Bangladesh

('20200319',102,null,1192,147,0  ,59),--Iran
('20200319',115,null,12  ,0  ,0  ,59),--Kuwait
('20200319',16 ,null,5   ,0  ,0  ,59),--Bahrain
('20200319',103,null,0   ,0  ,0  ,59),--Iraq
('20200319',223,null,15  ,0  ,0  ,59),--UAE (United Arab Emirates)
('20200319',161,null,9   ,0  ,0  ,59),--Oman
('20200319',119,null,13  ,1  ,0  ,59),--lebanon
('20200319',173,null,0   ,0  ,0  ,59),--Qatar
('20200319',162,null,54  ,0  ,0  ,59),--Pakistan
('20200319',65 ,null,30  ,2  ,0  ,59),--Egypt
('20200319',144,null,11  ,0  ,0  ,59),--Morocco
('20200319',188,null,67  ,0  ,0  ,59),--Saudi Arabia
('20200319',1  ,null,0   ,0  ,0  ,59),--Afghanistan
('20200319',111,null,13  ,0  ,0  ,59),--Jordan
('20200319',217,null,5   ,0  ,0  ,59),--Tunisia
('20200319',204,null,0   ,0  ,0  ,59),--Sudan
('20200319',198,null,0   ,0  ,0  ,59),--Somalia
('20200319',59 ,null,1   ,0  ,0  ,59),--Djibouti

('20200319',164,null,3   ,0  ,0  ,59),--Palestine

('20200319',228,null,3551,42 ,0  ,59),--USA
('20200319',37 ,null,145 ,7  ,0  ,59),--Canada
('20200319',64 ,null,97  ,0  ,0  ,59),--Ecuador
('20200319',137,null,11  ,0  ,0  ,59),--Mexico
('20200319',28 ,null,57  ,1  ,0  ,59),--Brasil
('20200319',9  ,null,14  ,0  ,0  ,59),--Argentina
('20200319',44 ,null,82  ,0  ,0  ,59),--Chile
('20200319',62 ,null,0   ,0  ,0  ,59),--Dominican Republic
('20200319',46 ,null,48  ,0  ,0  ,59),--Colombia
('20200319',168,null,59  ,0  ,0  ,59),--Peru
('20200319',50 ,null,9   ,0  ,0  ,59),--Costa Rica
('20200319',167,null,2   ,0  ,0  ,59),--Paraguay
('20200319',165,null,17  ,0  ,0  ,59),--Panama
('20200319',25 ,null,1   ,0  ,0  ,59),--Bolivia
('20200319',109,null,1   ,0  ,0  ,59),--Jamaica
('20200319',96 ,null,1   ,0  ,0  ,59),--Honduras
('20200319',93 ,null,1   ,0  ,0  ,59),--Guyana
('20200319',203,null,0   ,0  ,0  ,59),--St. Vicent Grenadines
('20200319',52 ,null,5   ,0  ,0  ,59),--Cuba
('20200319',172,null,2   ,0  ,0  ,59),--Puerto rico
('20200319',232,null,3   ,0  ,0  ,59),--Venezuela
('20200319',8  ,null,0   ,0  ,0  ,59),--Antigua and barbuda
('20200319',216,null,2   ,0  ,0  ,59),--Trinidad and Tobago
('20200319',227,null,23  ,0  ,0  ,59),--Uruguay
('20200319',182,null,0   ,0  ,0  ,59),--Saint lucia
('20200319',15 ,null,2   ,0  ,0  ,59),--Bahamas
('20200319',90 ,null,0   ,0  ,0  ,59),--Guatemala
('20200319',205,null,0   ,0  ,0  ,59),--suriname
('20200319',18,null,2   ,0  ,0  ,59),--Barbados
('20200319',143,null,1   ,0  ,0  ,59),--Montserrat

('20200319',183,null,2   ,0  ,0  ,59),--Saint Martin
('20200319',178,null,0   ,0  ,0  ,59),--Saint Barthelemy
('20200319',77 ,null,4   ,0  ,0  ,59),--French Guiana
('20200319',133,null,7   ,0  ,0  ,59),--Martinique
('20200319',40 ,null,0   ,0  ,0  ,59),--Cayman Islands
('20200319',88 ,null,15  ,0  ,0  ,59),--Guadeloupe
('20200319',53 ,null,0   ,0  ,0  ,59),--Cura�ao
('20200319',222,null,0   ,0  ,0  ,59),--US Virgin Islands
('20200319',11 ,null,2   ,0  ,0  ,59),--Aruba


('20200319',3  ,null,12  ,2  ,0  ,59),--Algeria
('20200319',189,null,9   ,0  ,0  ,59),--Senegal 
('20200319',155,null,6   ,0  ,0  ,59),--Nigeria
('20200319',36, null,5   ,0  ,0  ,59),--Cameroon
('20200319',199,null,54  ,0  ,0  ,59),--South Africa
('20200319',213,null,0   ,0  ,0  ,59),--Togo
('20200319',32 ,null,6   ,1  ,0  ,59),--Burkina Faso
('20200319',63 ,null,4   ,0  ,0  ,59),--DRC Congo
('20200319',56 ,null,3   ,0  ,0  ,59),--Cote Divore
('20200319',71 ,null,1   ,0  ,0  ,59),--Ethiopia
('20200319',79 ,null,2   ,0  ,0  ,59),--Gabon
('20200319',83 ,null,1   ,0  ,0  ,59),--Ghana
('20200319',91 ,null,0   ,0  ,0  ,59),--Guinea
('20200319',113,null,4   ,0  ,0  ,59),--Kenya
('20200319',147,null,0   ,0  ,0  ,59),--Namibia
('20200319',41 ,null,0   ,0  ,0  ,59),--Central African Republic
('20200319',48 ,null,2   ,0  ,0  ,59),--Congo
('20200319',67 ,null,2   ,0  ,0  ,59),--Equatorial Guinea
('20200319',70 ,null,0   ,0  ,0  ,59),--Eswatini
('20200319',134,null,1   ,0  ,0  ,59),--Mauritania
('20200319',176,null,4   ,0  ,0  ,59),--Rwanda
('20200319',191,null,2   ,0  ,0  ,59),--Seychelles
('20200319',22 ,null,0   ,0  ,0  ,59),--Benin
('20200319',121,null,1   ,0  ,0  ,59),--Liberia
('20200319',210,null,2   ,0  ,0  ,59),--Tanzania
('20200319',135,null,3   ,0  ,0  ,59),--Mauritius
('20200319',237,null,2   ,0  ,0  ,59),--Zambia
('20200319',80 ,null,1   ,0  ,0  ,59),--Gambia

('20200319',136,null,2   ,0  ,0  ,59),--Mayotte
('20200319',177,null,3   ,0  ,0  ,59),--Reunion

('20200319',58 ,null,0   ,0  ,0  ,59)--Diamond Princess


INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200320',45 ,null,126 ,11 ,0  ,60), --China
('20200320',181,null,239 ,10  ,0  ,60),--Republic of korea
('20200320',110,null,77  ,4  ,0  ,60),--Japan
('20200320',193,null,32  ,0  ,0  ,60),--Singapore
('20200320',12 ,null,199 ,0  ,0  ,60),--Australia
('20200320',128,null,227 ,0  ,0  ,60),--Malaysia
('20200320',233,null,19  ,0  ,0  ,60),--Vietnam
('20200320',169,null,30  ,3  ,0  ,60),--Philippines
('20200320',152,null,19  ,0  ,0  ,60),--New Zealand
('20200320',35 ,null,12  ,0  ,0  ,60),--Cambodia
('20200320',30 ,null,17  ,0  ,0  ,60),--Brunei
('20200320',141,null,1   ,0  ,0  ,60),--Mongolia
('20200320',74 ,null,1   ,0  ,0  ,60),--Fiji

('20200320',78 ,null,8   ,0  ,0  ,60),--French Polynesia
('20200320',89 ,null,7   ,0  ,0  ,60),--Guam
('20200320',151,null,2   ,0  ,0  ,60),--New Caledonia

('20200320',107,null,5322,429,0  ,60),--Italy
('20200320',76 ,null,1834,128,0  ,60),--France
('20200320',82 ,null,2801,8  ,0  ,60),--Germany
('20200320',200,null,3431,169,0  ,60),--Spain
('20200320',225,null,647 ,41 ,0  ,60),--UK (United Kingdom)
('20200320',207,null,853 ,12 ,0  ,60),--Switzerland
('20200320',160,null,129 ,3  ,0  ,60),--Norway
('20200320',150,null,409 ,18 ,0  ,60),--Netherlands
('20200320',13 ,null,197 ,1  ,0  ,60),--Austria
('20200320',206,null,144 ,0  ,0  ,60),--Sweden
('20200320',99 ,null,80  ,0  ,0  ,60),--Iceland
('20200320',20 ,null,309 ,0  ,0  ,60),--Belgium
('20200320',186,null,17  ,0  ,0  ,60),--San Marino
('20200320',106,null,102 ,0  ,0  ,60),--Israel
('20200320',57 ,null,88  ,2  ,0  ,60),--Denmark
('20200320',51 ,null,0   ,0  ,0  ,60),--Croatia

('20200320',85 ,null,0   ,0  ,0  ,60),--Greece
('20200320',75 ,null,10  ,0  ,0  ,60),--Finland
('20200320',171,null,143 ,1  ,0  ,60),--Portugal
('20200320',19 ,null,0   ,0  ,0  ,60),--Belarus
('20200320',55 ,null,172 ,0  ,0  ,60),--Czechia
('20200320',174,null,14  ,0  ,0  ,60),--Romania
('20200320',14 ,null,0   ,0  ,0  ,60),--Azerbaijan
('20200320',81 ,null,4   ,0  ,0  ,60),--Georgia
('20200320',175,null,52  ,0  ,0  ,60),--Russia
('20200320',26 ,null,8   ,0  ,0  ,60),--Bosnia and Herzegovina
('20200320',2  ,null,11  ,0  ,0  ,60),--Albania
('20200320',69 ,null,9   ,0  ,0  ,60),--Estonia
('20200320',98 ,null,15  ,0  ,0  ,60),--Hungary
('20200320',104,null,265 ,1  ,0  ,60),--Ireland
('20200320',5  ,null,36  ,0  ,0  ,60),--Andorra
('20200320',10 ,null,38  ,0  ,0  ,60),--Armenia
('20200320',118,null,15  ,0  ,0  ,60),--Latvia
('20200320',123,null,10  ,0  ,0  ,60),--Lithuania
('20200320',124,null,135 ,2  ,0  ,60),--Luxembourg
('20200320',140,null,0   ,0  ,0  ,60),--Monaco
('20200320',158,null,17  ,0  ,0  ,60),--North Macedonia
('20200320',170,null,38  ,0  ,0  ,60),--Poland
('20200320',196,null,33  ,0  ,0  ,60),--Slovenia
('20200320',226,null,0   ,0  ,0  ,60),--Ukraine
('20200320',122,null,0   ,0  ,0  ,60),--Liechtenstein
('20200320',95 ,null,0   ,0  ,0  ,60),--Holy see
('20200320',195,null,18  ,0  ,0  ,60),--Slovakia
('20200320',31 ,null,2   ,1  ,0  ,60),--Bulgaria
('20200320',131,null,5   ,0  ,0  ,60),--Malta
('20200320',139,null,13  ,0  ,0  ,60),--Moldova
('20200320',54 ,null,0   ,0  ,0  ,60),--Cyprus
('20200320',190,null,41  ,0  ,0  ,60),--Serbia
('20200320',218,null,51  ,1  ,0  ,60),--Turkey
('20200320',112,null,10  ,0  ,0  ,60),--Kazakhstan
('20200320',229,null,5   ,0  ,0  ,60),--Uzbekistan
('20200320',142,null,8   ,0  ,0  ,60),--Montenegro
('20200320',116,null,0   ,0  ,0  ,60),--Kyrgyzstan


('20200320',72 ,null,14   ,0  ,0  ,60),--Faeroe Islands
('20200320',239,null,0   ,0  ,0  ,60),--Guernsey
('20200320',240,null,0   ,0  ,0  ,60),--Jersey
('20200320',84 ,null,2   ,0  ,0  ,60),--Gibraltar
('20200320',86 ,null,2   ,0  ,0  ,60),--Greenland


('20200320',211,null,110 ,0  ,0  ,60),--Thailand
('20200320',100,null,44  ,4  ,0  ,60),--India
('20200320',101,null,82  ,6  ,0  ,60),--Indonesia
('20200320',149,null,0   ,0  ,0  ,60),--Nepal
('20200320',201,null,17  ,0  ,0  ,60),--Sri Lanka
('20200320',129,null,0   ,0  ,0  ,60),--Maldives
('20200320',17 ,null,7   ,1  ,0  ,60),--Bangladesh
('20200320',24 ,null,1   ,1  ,0  ,60),--Bhutan


('20200320',102,null,1046,149,0  ,60),--Iran
('20200320',115,null,6   ,0  ,0  ,60),--Kuwait
('20200320',16 ,null,13  ,0  ,0  ,60),--Bahrain
('20200320',103,null,13  ,0  ,0  ,60),--Iraq
('20200320',223,null,27  ,0  ,0  ,60),--UAE (United Arab Emirates)
('20200320',161,null,6   ,0  ,0  ,60),--Oman
('20200320',119,null,29  ,0  ,0  ,60),--lebanon
('20200320',173,null,10  ,0  ,0  ,60),--Qatar
('20200320',162,null,61  ,2  ,0  ,60),--Pakistan
('20200320',65 ,null,14  ,0  ,0  ,60),--Egypt
('20200320',144,null,12  ,0  ,0  ,60),--Morocco
('20200320',188,null,0   ,0  ,0  ,60),--Saudi Arabia
('20200320',1  ,null,0   ,0  ,0  ,60),--Afghanistan
('20200320',111,null,4   ,0  ,0  ,60),--Jordan
('20200320',217,null,10  ,0  ,0  ,60),--Tunisia
('20200320',204,null,0   ,0  ,0  ,60),--Sudan
('20200320',198,null,0   ,0  ,0  ,60),--Somalia
('20200320',59 ,null,0   ,0  ,0  ,60),--Djibouti

('20200320',164,null,3   ,0  ,0  ,60),--Palestine

('20200320',228,null,3355,50 ,0  ,60),--USA
('20200320',37 ,null,167 ,0  ,0  ,60),--Canada
('20200320',64 ,null,44  ,1  ,0  ,60),--Ecuador
('20200320',137,null,25  ,1  ,0  ,60),--Mexico
('20200320',28 ,null,137 ,3  ,0  ,60),--Brasil
('20200320',9  ,null,0   ,0  ,0  ,60),--Argentina
('20200320',44 ,null,104 ,0  ,0  ,60),--Chile
('20200320',62 ,null,0   ,0  ,0  ,60),--Dominican Republic
('20200320',46 ,null,15  ,0  ,0  ,60),--Colombia
('20200320',168,null,89  ,0  ,0  ,60),--Peru
('20200320',50 ,null,37  ,0  ,0  ,60),--Costa Rica
('20200320',167,null,0   ,0  ,0  ,60),--Paraguay
('20200320',165,null,23  ,0  ,0  ,60),--Panama
('20200320',25 ,null,3   ,0  ,0  ,60),--Bolivia
('20200320',109,null,2   ,1  ,0  ,60),--Jamaica
('20200320',96 ,null,3   ,0  ,0  ,60),--Honduras
('20200320',93 ,null,0   ,0  ,0  ,60),--Guyana
('20200320',203,null,0   ,0  ,0  ,60),--St. Vicent Grenadines
('20200320',52 ,null,1   ,0  ,0  ,60),--Cuba
('20200320',172,null,1   ,0  ,0  ,60),--Puerto rico
('20200320',232,null,0   ,0  ,0  ,60),--Venezuela
('20200320',8  ,null,0   ,0  ,0  ,60),--Antigua and barbuda
('20200320',216,null,2   ,0  ,0  ,60),--Trinidad and Tobago
('20200320',227,null,50  ,0  ,0  ,60),--Uruguay
('20200320',182,null,0   ,0  ,0  ,60),--Saint lucia
('20200320',15 ,null,0   ,0  ,0  ,60),--Bahamas
('20200320',90 ,null,3   ,0  ,0  ,60),--Guatemala
('20200320',205,null,0   ,0  ,0  ,60),--suriname
('20200320',18,null ,2   ,0  ,0  ,60),--Barbados
('20200320',143,null,0   ,0  ,0  ,60),--Montserrat
('20200320',66 ,null,1   ,0  ,0  ,60),--El Salvador
('20200320',153,null,1   ,0  ,0  ,60),--Nicaragua


('20200320',183,null,0   ,0  ,0  ,60),--Saint Martin
('20200320',178,null,0   ,0  ,0  ,60),--Saint Barthelemy
('20200320',77 ,null,4   ,0  ,0  ,60),--French Guiana
('20200320',133,null,9   ,0  ,0  ,60),--Martinique
('20200320',40 ,null,2   ,0  ,0  ,60),--Cayman Islands
('20200320',88 ,null,12  ,0  ,0  ,60),--Guadeloupe
('20200320',53 ,null,0   ,0  ,0  ,60),--Cura�ao
('20200320',222,null,0   ,0  ,0  ,60),--US Virgin Islands
('20200320',11 ,null,1   ,0  ,0  ,60),--Aruba
('20200320',194,null,1   ,0  ,0  ,60),--Sint Maarten


('20200320',3  ,null,10  ,1  ,0  ,60),--Algeria
('20200320',189,null,2   ,0  ,0  ,60),--Senegal 
('20200320',155,null,4   ,0  ,0  ,60),--Nigeria
('20200320',36, null,5   ,0  ,0  ,60),--Cameroon
('20200320',199,null,34  ,0  ,0  ,60),--South Africa
('20200320',213,null,8   ,0  ,0  ,60),--Togo
('20200320',32 ,null,14  ,0  ,0  ,60),--Burkina Faso
('20200320',63 ,null,7   ,0  ,0  ,60),--DRC Congo
('20200320',56 ,null,0   ,0  ,0  ,60),--Cote Divore
('20200320',71 ,null,3   ,0  ,0  ,60),--Ethiopia
('20200320',79 ,null,0   ,0  ,0  ,60),--Gabon
('20200320',83 ,null,2   ,0  ,0  ,60),--Ghana
('20200320',91 ,null,0   ,0  ,0  ,60),--Guinea
('20200320',113,null,0   ,0  ,0  ,60),--Kenya
('20200320',147,null,1   ,0  ,0  ,60),--Namibia
('20200320',41 ,null,0   ,0  ,0  ,60),--Central African Republic
('20200320',48 ,null,0   ,0  ,0  ,60),--Congo
('20200320',67 ,null,1   ,0  ,0  ,60),--Equatorial Guinea
('20200320',70 ,null,0   ,0  ,0  ,60),--Eswatini
('20200320',134,null,0   ,0  ,0  ,60),--Mauritania
('20200320',176,null,0   ,0  ,0  ,60),--Rwanda
('20200320',191,null,0   ,0  ,0  ,60),--Seychelles
('20200320',22 ,null,0   ,0  ,0  ,60),--Benin
('20200320',121,null,0   ,0  ,0  ,60),--Liberia
('20200320',210,null,3   ,0  ,0  ,60),--Tanzania
('20200320',135,null,4   ,0  ,0  ,60),--Mauritius
('20200320',237,null,0   ,0  ,0  ,60),--Zambia
('20200320',80 ,null,0   ,0  ,0  ,60),--Gambia
('20200320',42 ,null,1   ,0  ,0  ,60),--Chad
('20200320',154,null,1   ,0  ,0  ,60),--Niger


('20200320',136,null,1   ,0  ,0  ,60),--Mayotte
('20200320',177,null,3   ,0  ,0  ,60),--Reunion

('20200320',58 ,null,0   ,0  ,0  ,60)--Diamond Princess






INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200321',45 ,null,116 ,8  ,0  ,61), --China
('20200321',181,null,147 ,8  ,0  ,61),--Republic of korea
('20200321',110,null,46  ,2  ,0  ,61),--Japan
('20200321',193,null,40  ,0  ,0  ,61),--Singapore
('20200321',12 ,null,146 ,1  ,0  ,61),--Australia
('20200321',128,null,130 ,1  ,0  ,61),--Malaysia
('20200321',233,null,6   ,0  ,0  ,61),--Vietnam
('20200321',169,null,13  ,1  ,0  ,61),--Philippines
('20200321',152,null,14  ,0  ,0  ,61),--New Zealand
('20200321',35 ,null,4   ,0  ,0  ,61),--Cambodia
('20200321',30 ,null,5   ,0  ,0  ,61),--Brunei
('20200321',141,null,0   ,0  ,0  ,61),--Mongolia
('20200321',74 ,null,0   ,0  ,0  ,61),--Fiji
('20200321',166,null,1   ,0  ,0  ,61),--Papua new Guinea

('20200321',78 ,null,0   ,0  ,0  ,61),--French Polynesia
('20200321',89 ,null,2   ,0  ,0  ,61),--Guam
('20200321',151,null,0   ,0  ,0  ,61),--New Caledonia

('20200321',107,null,5986,625,0  ,61),--Italy
('20200321',76 ,null,1598,78 ,0  ,61),--France
('20200321',82 ,null,7324,25  ,0  ,61),--Germany
('20200321',200,null,2833,235,0  ,61),--Spain
('20200321',225,null,706 ,33 ,0  ,61),--UK (United Kingdom)
('20200321',207,null,706 ,33 ,0  ,61),--Switzerland
('20200321',160,null,190 ,1  ,0  ,61),--Norway
('20200321',150,null,534 ,30 ,0  ,61),--Netherlands
('20200321',13 ,null,806 ,1  ,0  ,61),--Austria
('20200321',206,null,144 ,0  ,0  ,61),--Sweden
('20200321',99 ,null,79  ,1  ,0  ,61),--Iceland
('20200321',20 ,null,462 ,23 ,0  ,61),--Belgium
('20200321',186,null,25  ,0  ,0  ,61),--San Marino
('20200321',106,null,183 ,1  ,0  ,61),--Israel
('20200321',57 ,null,123 ,3  ,0  ,61),--Denmark
('20200321',51 ,null,45  ,1  ,0  ,61),--Croatia

('20200321',85 ,null,77  ,3  ,0  ,61),--Greece
('20200321',75 ,null,81  ,0  ,0  ,61),--Finland
('20200321',171,null,235 ,3  ,0  ,61),--Portugal
('20200321',19 ,null,11  ,0  ,0  ,61),--Belarus
('20200321',55 ,null,210 ,0  ,0  ,61),--Czechia
('20200321',174,null,48  ,0  ,0  ,61),--Romania
('20200321',14 ,null,10  ,0  ,0  ,61),--Azerbaijan
('20200321',81 ,null,5   ,0  ,0  ,61),--Georgia
('20200321',175,null,54  ,0  ,0  ,61),--Russia
('20200321',26 ,null,0   ,0  ,0  ,61),--Bosnia and Herzegovina
('20200321',2  ,null,0   ,0  ,0  ,61),--Albania
('20200321',69 ,null,16  ,0  ,0  ,61),--Estonia
('20200321',98 ,null,12  ,3  ,0  ,61),--Hungary
('20200321',104,null,265 ,1  ,0  ,61),--Ireland
('20200321',5  ,null,0   ,0  ,0  ,61),--Andorra
('20200321',10 ,null,14  ,0  ,0  ,61),--Armenia
('20200321',118,null,25  ,0  ,0  ,61),--Latvia
('20200321',123,null,33  ,1  ,0  ,61),--Lithuania
('20200321',124,null,139 ,1  ,0  ,61),--Luxembourg
('20200321',140,null,3   ,0  ,0  ,61),--Monaco
('20200321',158,null,22  ,0  ,0  ,61),--North Macedonia
('20200321',170,null,100 ,0  ,0  ,61),--Poland
('20200321',196,null,22  ,0  ,0  ,61),--Slovenia
('20200321',226,null,10  ,1  ,0  ,61),--Ukraine
('20200321',122,null,9   ,0  ,0  ,61),--Liechtenstein
('20200321',95 ,null,0   ,0  ,0  ,61),--Holy see
('20200321',195,null,14  ,0  ,0  ,61),--Slovakia
('20200321',31 ,null,33  ,0  ,0  ,61),--Bulgaria
('20200321',131,null,11  ,0  ,0  ,61),--Malta
('20200321',139,null,17  ,1  ,0  ,61),--Moldova
('20200321',54 ,null,9   ,0  ,0  ,61),--Cyprus
('20200321',190,null,12  ,1  ,0  ,61),--Serbia
('20200321',218,null,479 ,7  ,0  ,61),--Turkey
('20200321',112,null,3   ,0  ,0  ,61),--Kazakhstan
('20200321',229,null,12  ,0  ,0  ,61),--Uzbekistan
('20200321',142,null,4   ,0  ,0  ,61),--Montenegro
('20200321',116,null,0   ,0  ,0  ,61),--Kyrgyzstan


('20200321',72 ,null,8   ,0  ,0  ,61),--Faeroe Islands
('20200321',239,null,0   ,0  ,0  ,61),--Guernsey
('20200321',240,null,7   ,0  ,0  ,61),--Jersey
('20200321',84 ,null,0   ,0  ,0  ,61),--Gibraltar
('20200321',86 ,null,0   ,0  ,0  ,61),--Greenland
('20200321',105,null,1   ,0  ,0  ,61),--Isle of man

('20200321',211,null,0   ,0  ,0  ,61),--Thailand
('20200321',100,null,0   ,0  ,0  ,61),--India
('20200321',101,null,60  ,7  ,0  ,61),--Indonesia
('20200321',149,null,0   ,0  ,0  ,61),--Nepal
('20200321',201,null,0   ,0  ,0  ,61),--Sri Lanka
('20200321',129,null,0   ,0  ,0  ,61),--Maldives
('20200321',17 ,null,0   ,0  ,0  ,61),--Bangladesh
('20200321',24 ,null,0   ,0  ,0  ,61),--Bhutan
('20200321',212,null,1   ,0  ,0  ,61),--Timor-leste

('20200321',102,null,1237,149,0  ,61),--Iran
('20200321',115,null,11  ,0  ,0  ,61),--Kuwait
('20200321',16 ,null,16  ,0  ,0  ,61),--Bahrain
('20200321',103,null,16  ,2  ,0  ,61),--Iraq
('20200321',223,null,0   ,0  ,0  ,61),--UAE (United Arab Emirates)
('20200321',161,null,9   ,0  ,0  ,61),--Oman
('20200321',119,null,14  ,0  ,0  ,61),--lebanon
('20200321',173,null,8   ,0  ,0  ,61),--Qatar
('20200321',162,null,159 ,0  ,0  ,61),--Pakistan
('20200321',65 ,null,46  ,1  ,0  ,61),--Egypt
('20200321',144,null,13  ,1  ,0  ,61),--Morocco
('20200321',188,null,36  ,0  ,0  ,61),--Saudi Arabia
('20200321',1  ,null,2   ,0  ,0  ,61),--Afghanistan
('20200321',111,null,13  ,0  ,0  ,61),--Jordan
('20200321',217,null,15  ,1  ,0  ,61),--Tunisia
('20200321',204,null,0   ,0  ,0  ,61),--Sudan
('20200321',198,null,0   ,0  ,0  ,61),--Somalia
('20200321',59 ,null,0   ,0  ,0  ,61),--Djibouti

('20200321',164,null,1   ,0  ,0  ,61),--Palestine

('20200321',228,null,4777,51 ,0  ,61),--USA
('20200321',37 ,null,110 ,0  ,0  ,61),--Canada
('20200321',64 ,null,168 ,2  ,0  ,61),--Ecuador
('20200321',137,null,46  ,0  ,0  ,61),--Mexico
('20200321',28 ,null,193 ,0  ,0  ,61),--Brasil
('20200321',9  ,null,31  ,1  ,0  ,61),--Argentina
('20200321',44 ,null,92  ,0  ,0  ,61),--Chile
('20200321',62 ,null,38  ,0  ,0  ,61),--Dominican Republic
('20200321',46 ,null,37  ,0  ,0  ,61),--Colombia
('20200321',168,null,0   ,2  ,0  ,61),--Peru
('20200321',50 ,null,26  ,1  ,0  ,61),--Costa Rica
('20200321',167,null,2   ,0  ,0  ,61),--Paraguay
('20200321',165,null,28  ,0  ,0  ,61),--Panama
('20200321',25 ,null,1   ,0  ,0  ,61),--Bolivia
('20200321',109,null,1   ,0  ,0  ,61),--Jamaica
('20200321',96 ,null,12  ,0  ,0  ,61),--Honduras
('20200321',93 ,null,0   ,0  ,0  ,61),--Guyana
('20200321',203,null,0   ,0  ,0  ,61),--St. Vicent Grenadines
('20200321',52 ,null,5   ,0  ,0  ,61),--Cuba

('20200321',232,null,0   ,0  ,0  ,61),--Venezuela
('20200321',8  ,null,0   ,0  ,0  ,61),--Antigua and barbuda
('20200321',216,null,2   ,0  ,0  ,61),--Trinidad and Tobago
('20200321',227,null,15  ,0  ,0  ,61),--Uruguay
('20200321',182,null,0   ,0  ,0  ,61),--Saint lucia
('20200321',15 ,null,1   ,0  ,0  ,61),--Bahamas
('20200321',90 ,null,3   ,0  ,0  ,61),--Guatemala
('20200321',205,null,1   ,0  ,0  ,61),--suriname
('20200321',18,null ,2   ,0  ,0  ,61),--Barbados
('20200321',143,null,0   ,0  ,0  ,61),--Montserrat
('20200321',66 ,null,0   ,0  ,0  ,61),--El Salvador
('20200321',153,null,0   ,0  ,0  ,61),--Nicaragua
('20200321',94 ,null,1   ,0  ,0  ,61),--Haiti

('20200321',88 ,null,6   ,0  ,0  ,61),--Guadeloupe
('20200321',133,null,0   ,0  ,0  ,61),--Martinique
('20200321',77 ,null,4   ,0  ,0  ,61),--French Guiana
('20200321',172,null,8   ,0  ,0  ,61),--Puerto rico
('20200321',183,null,0   ,0  ,0  ,61),--Saint Martin
('20200321',178,null,0   ,0  ,0  ,61),--Saint Barthelemy
('20200321',40 ,null,0   ,0  ,0  ,61),--Cayman Islands
('20200321',53 ,null,0   ,0  ,0  ,61),--Cura�ao
('20200321',222,null,0   ,0  ,0  ,61),--US Virgin Islands
('20200321',11 ,null,0   ,0  ,0  ,61),--Aruba
('20200321',194,null,0   ,0  ,0  ,61),--Sint Maarten
('20200321',23 ,null,2   ,0  ,0  ,61),--Bermuda

('20200321',3  ,null,12  ,3  ,0  ,61),--Algeria
('20200321',189,null,0   ,0  ,0  ,61),--Senegal 
('20200321',155,null,0   ,0  ,0  ,61),--Nigeria
('20200321',36, null,7   ,0  ,0  ,61),--Cameroon
('20200321',199,null,55  ,0  ,0  ,61),--South Africa
('20200321',213,null,0   ,0  ,0  ,61),--Togo
('20200321',32 ,null,0   ,0  ,0  ,61),--Burkina Faso
('20200321',63 ,null,0   ,0  ,0  ,61),--DRC Congo
('20200321',56 ,null,0   ,0  ,0  ,61),--Cote Divore
('20200321',71 ,null,0   ,0  ,0  ,61),--Ethiopia
('20200321',79 ,null,0   ,1  ,0  ,61),--Gabon
('20200321',83 ,null,5   ,0  ,0  ,61),--Ghana
('20200321',91 ,null,0   ,0  ,0  ,61),--Guinea
('20200321',113,null,0   ,0  ,0  ,61),--Kenya
('20200321',147,null,0   ,0  ,0  ,61),--Namibia
('20200321',41 ,null,0   ,0  ,0  ,61),--Central African Republic
('20200321',48 ,null,0   ,0  ,0  ,61),--Congo
('20200321',67 ,null,0   ,0  ,0  ,61),--Equatorial Guinea
('20200321',70 ,null,0   ,0  ,0  ,61),--Eswatini
('20200321',134,null,0   ,0  ,0  ,61),--Mauritania
('20200321',176,null,0   ,0  ,0  ,61),--Rwanda
('20200321',191,null,0   ,0  ,0  ,61),--Seychelles
('20200321',22 ,null,0   ,0  ,0  ,61),--Benin
('20200321',121,null,0   ,0  ,0  ,61),--Liberia
('20200321',210,null,0   ,0  ,0  ,61),--Tanzania
('20200321',135,null,5   ,0  ,0  ,61),--Mauritius
('20200321',237,null,0   ,0  ,0  ,61),--Zambia
('20200321',80 ,null,0   ,0  ,0  ,61),--Gambia
('20200321',42 ,null,0   ,0  ,0  ,61),--Chad
('20200321',154,null,0   ,0  ,0  ,61),--Niger
('20200321',34 ,null,1   ,0  ,0  ,61),--Cabo Verde
('20200321',238,null,1   ,0  ,0  ,61),--Zimbabwe

('20200321',136,null,0   ,0  ,0  ,61),--Mayotte
('20200321',177,null,13  ,0  ,0  ,61),--Reunion

('20200321',58 ,null,0   ,0  ,0  ,61)--Diamond Princess






INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200322',45 ,null,82  ,6  ,0  ,62), --China
('20200322',181,null,98  ,2  ,0  ,62),--Republic of korea
('20200322',110,null,50  ,1  ,0  ,62),--Japan
('20200322',193,null,47  ,2  ,0  ,62),--Singapore
('20200322',12 ,null,208 ,0  ,0  ,62),--Australia
('20200322',128,null,153 ,0  ,0  ,62),--Malaysia
('20200322',233,null,3   ,0  ,0  ,62),--Vietnam
('20200322',169,null,77  ,1  ,0  ,62),--Philippines
('20200322',152,null,13  ,0  ,0  ,62),--New Zealand
('20200322',35 ,null,2   ,0  ,0  ,62),--Cambodia
('20200322',30 ,null,5   ,0  ,0  ,62),--Brunei
('20200322',141,null,4   ,0  ,0  ,62),--Mongolia
('20200322',74 ,null,1   ,0  ,0  ,62),--Fiji
('20200322',166,null,0   ,0  ,0  ,62),--Papua new Guinea

('20200322',78 ,null,4   ,0  ,0  ,62),--French Polynesia
('20200322',89 ,null,1   ,0  ,0  ,62),--Guam
('20200322',151,null,2   ,0  ,0  ,62),--New Caledonia

('20200322',107,null,6557,795,0  ,62),--Italy
('20200322',76 ,null,1821,122,0  ,62),--France
('20200322',82 ,null,3140,22 ,0  ,62),--Germany
('20200322',200,null,4946,324,0  ,62),--Spain
('20200322',225,null,1035,56 ,0  ,62),--UK (United Kingdom)
('20200322',207,null,1237,13 ,0  ,62),--Switzerland
('20200322',160,null,184 ,0  ,0  ,62),--Norway
('20200322',150,null,637 ,30 ,0  ,62),--Netherlands
('20200322',13 ,null,375 ,2  ,0  ,62),--Austria
('20200322',206,null,123 ,4  ,0  ,62),--Sweden
('20200322',99 ,null,64  ,0  ,0  ,62),--Iceland
('20200322',20 ,null,558 ,30 ,0  ,62),--Belgium
('20200322',186,null,0   ,6  ,0  ,62),--San Marino
('20200322',106,null,171 ,0  ,0  ,62),--Israel
('20200322',57 ,null,71  ,4  ,0  ,62),--Denmark
('20200322',51 ,null,80  ,0  ,0  ,62),--Croatia

('20200322',85 ,null,35  ,0  ,0  ,62),--Greece
('20200322',75 ,null,71  ,1  ,0  ,62),--Finland
('20200322',171,null,260 ,6  ,0  ,62),--Portugal
('20200322',19 ,null,19  ,0  ,0  ,62),--Belarus
('20200322',55 ,null,91  ,0  ,0  ,62),--Czechia
('20200322',174,null,59  ,0  ,0  ,62),--Romania
('20200322',14 ,null,9   ,0  ,0  ,62),--Azerbaijan
('20200322',81 ,null,6   ,0  ,0  ,62),--Georgia
('20200322',175,null,53  ,0  ,0  ,62),--Russia
('20200322',26 ,null,48  ,1  ,0  ,62),--Bosnia and Herzegovina
('20200322',2  ,null,6   ,0  ,0  ,62),--Albania
('20200322',69 ,null,23  ,0  ,0  ,62),--Estonia
('20200322',98 ,null,46  ,0  ,0  ,62),--Hungary
('20200322',104,null,102 ,0  ,0  ,62),--Ireland
('20200322',5  ,null,13  ,0  ,0  ,62),--Andorra
('20200322',10 ,null,24  ,0  ,0  ,62),--Armenia
('20200322',118,null,13  ,0  ,0  ,62),--Latvia
('20200322',123,null,36  ,0  ,0  ,62),--Lithuania
('20200322',124,null,186 ,3  ,0  ,62),--Luxembourg
('20200322',140,null,6   ,0  ,0  ,62),--Monaco
('20200322',158,null,14  ,0  ,0  ,62),--North Macedonia
('20200322',170,null,111 ,0  ,0  ,62),--Poland
('20200322',196,null,42  ,0  ,0  ,62),--Slovenia
('20200322',226,null,21  ,0  ,0  ,62),--Ukraine
('20200322',122,null,2   ,0  ,0  ,62),--Liechtenstein
('20200322',95 ,null,0   ,0  ,0  ,62),--Holy see
('20200322',195,null,41  ,0  ,0  ,62),--Slovakia
('20200322',31 ,null,38  ,0  ,0  ,62),--Bulgaria
('20200322',131,null,9   ,0  ,0  ,62),--Malta
('20200322',139,null,0   ,0  ,0  ,62),--Moldova
('20200322',54 ,null,0   ,0  ,0  ,62),--Cyprus
('20200322',190,null,38  ,0  ,0  ,62),--Serbia
('20200322',218,null,277 ,12 ,0  ,62),--Turkey
('20200322',112,null,3   ,0  ,0  ,62),--Kazakhstan
('20200322',229,null,0   ,0  ,0  ,62),--Uzbekistan
('20200322',142,null,0   ,0  ,0  ,62),--Montenegro
('20200322',116,null,8   ,0  ,0  ,62),--Kyrgyzstan


('20200322',72 ,null,12  ,0  ,0  ,62),--Faeroe Islands
('20200322',239,null,0   ,0  ,0  ,62),--Guernsey
('20200322',240,null,0   ,0  ,0  ,62),--Jersey
('20200322',84 ,null,0   ,0  ,0  ,62),--Gibraltar
('20200322',86 ,null,0   ,0  ,0  ,62),--Greenland
('20200322',105,null,1   ,0  ,0  ,62),--Isle of man

('20200322',211,null,89  ,0  ,0  ,62),--Thailand
('20200322',100,null,88  ,0  ,0  ,62),--India
('20200322',101,null,81  ,6  ,0  ,62),--Indonesia
('20200322',149,null,0   ,0  ,0  ,62),--Nepal
('20200322',201,null,13  ,0  ,0  ,62),--Sri Lanka
('20200322',129,null,0   ,0  ,0  ,62),--Maldives
('20200322',17 ,null,7   ,2  ,0  ,62),--Bangladesh
('20200322',24 ,null,0   ,0  ,0  ,62),--Bhutan
('20200322',212,null,0   ,0  ,0  ,62),--Timor-leste

('20200322',102,null,966 ,123,0  ,62),--Iran
('20200322',115,null,17  ,0  ,0  ,62),--Kuwait
('20200322',16 ,null,21  ,0  ,0  ,62),--Bahrain
('20200322',103,null,21  ,3  ,0  ,62),--Iraq
('20200322',223,null,13  ,2  ,0  ,62),--UAE (United Arab Emirates)
('20200322',161,null,4   ,0  ,0  ,62),--Oman
('20200322',119,null,43  ,0  ,0  ,62),--lebanon
('20200322',173,null,10  ,0  ,0  ,62),--Qatar
('20200322',162,null,34  ,1  ,0  ,62),--Pakistan
('20200322',65 ,null,29  ,1  ,0  ,62),--Egypt
('20200322',144,null,12  ,0  ,0  ,62),--Morocco
('20200322',188,null,118 ,0  ,0  ,62),--Saudi Arabia
('20200322',1  ,null,0   ,0  ,0  ,62),--Afghanistan
('20200322',111,null,15  ,0  ,0  ,62),--Jordan
('20200322',217,null,6   ,0  ,0  ,62),--Tunisia
('20200322',204,null,1   ,0  ,0  ,62),--Sudan
('20200322',198,null,0   ,0  ,0  ,62),--Somalia
('20200322',59 ,null,0   ,0  ,0  ,62),--Djibouti

('20200322',164,null,4   ,0  ,0  ,62),--Palestine

('20200322',228,null,0   ,0  ,0  ,62),--USA
('20200322',37 ,null,202 ,4  ,0  ,62),--Canada
('20200322',64 ,null,139 ,2  ,0  ,62),--Ecuador
('20200322',137,null,0   ,0  ,0  ,62),--Mexico
('20200322',28 ,null,283 ,7  ,0  ,62),--Brasil
('20200322',9  ,null,30  ,0  ,0  ,62),--Argentina
('20200322',44 ,null,0   ,1  ,0  ,62),--Chile
('20200322',62 ,null,0   ,0  ,0  ,62),--Dominican Republic
('20200322',46 ,null,51  ,0  ,0  ,62),--Colombia
('20200322',168,null,84  ,0  ,0  ,62),--Peru
('20200322',50 ,null,0   ,0  ,0  ,62),--Costa Rica
('20200322',167,null,5   ,1  ,0  ,62),--Paraguay
('20200322',165,null,0   ,0  ,0  ,62),--Panama
('20200322',25 ,null,3   ,0  ,0  ,62),--Bolivia
('20200322',109,null,0   ,0  ,0  ,62),--Jamaica
('20200322',96 ,null,0   ,0  ,0  ,62),--Honduras
('20200322',93 ,null,0   ,0  ,0  ,62),--Guyana
('20200322',203,null,0   ,0  ,0  ,62),--St. Vicent Grenadines
('20200322',52 ,null,0   ,0  ,0  ,62),--Cuba
('20200322',232,null,0   ,0  ,0  ,62),--Venezuela
('20200322',8  ,null,0   ,0  ,0  ,62),--Antigua and barbuda
('20200322',216,null,0   ,0  ,0  ,62),--Trinidad and Tobago
('20200322',227,null,0   ,0  ,0  ,62),--Uruguay
('20200322',182,null,0   ,0  ,0  ,62),--Saint lucia
('20200322',15 ,null,0   ,0  ,0  ,62),--Bahamas
('20200322',90 ,null,0   ,0  ,0  ,62),--Guatemala
('20200322',205,null,0   ,0  ,0  ,62),--suriname
('20200322',18,null ,0   ,0  ,0  ,62),--Barbados
('20200322',143,null,0   ,0  ,0  ,62),--Montserrat
('20200322',66 ,null,0   ,0  ,0  ,62),--El Salvador
('20200322',153,null,1   ,0  ,0  ,62),--Nicaragua
('20200322',94 ,null,0   ,0  ,0  ,62),--Haiti

('20200322',88 ,null,0   ,0  ,0  ,62),--Guadeloupe
('20200322',133,null,0   ,0  ,0  ,62),--Martinique
('20200322',77 ,null,0   ,0  ,0  ,62),--French Guiana
('20200322',172,null,7   ,1  ,0  ,62),--Puerto rico
('20200322',183,null,0   ,0  ,0  ,62),--Saint Martin
('20200322',178,null,0   ,0  ,0  ,62),--Saint Barthelemy
('20200322',40 ,null,0   ,0  ,0  ,62),--Cayman Islands
('20200322',53 ,null,0   ,1  ,0  ,62),--Cura�ao
('20200322',222,null,3   ,0  ,0  ,62),--US Virgin Islands
('20200322',11 ,null,0   ,0  ,0  ,62),--Aruba
('20200322',194,null,0   ,0  ,0  ,62),--Sint Maarten
('20200322',23 ,null,0   ,0  ,0  ,62),--Bermuda

('20200322',3  ,null,0   ,5  ,0  ,62),--Algeria
('20200322',189,null,18  ,0  ,0  ,62),--Senegal 
('20200322',155,null,10  ,0  ,0  ,62),--Nigeria
('20200322',36, null,5   ,0  ,0  ,62),--Cameroon
('20200322',199,null,35  ,0  ,0  ,62),--South Africa
('20200322',213,null,6   ,0  ,0  ,62),--Togo
('20200322',32 ,null,32  ,2  ,0  ,62),--Burkina Faso
('20200322',63 ,null,9   ,1  ,0  ,62),--DRC Congo
('20200322',56 ,null,0   ,0  ,0  ,62),--Cote Divore
('20200322',71 ,null,0   ,0  ,0  ,62),--Ethiopia
('20200322',79 ,null,0   ,0  ,0  ,62),--Gabon
('20200322',83 ,null,3   ,0  ,0  ,62),--Ghana
('20200322',91 ,null,0   ,0  ,0  ,62),--Guinea
('20200322',113,null,0   ,0  ,0  ,62),--Kenya
('20200322',147,null,0   ,0  ,0  ,62),--Namibia
('20200322',41 ,null,0   ,0  ,0  ,62),--Central African Republic
('20200322',48 ,null,1   ,0  ,0  ,62),--Congo
('20200322',67 ,null,2   ,0  ,0  ,62),--Equatorial Guinea
('20200322',70 ,null,0   ,0  ,0  ,62),--Eswatini
('20200322',134,null,0   ,0  ,0  ,62),--Mauritania
('20200322',176,null,6   ,0  ,0  ,62),--Rwanda
('20200322',191,null,1   ,0  ,0  ,62),--Seychelles
('20200322',22 ,null,0   ,0  ,0  ,62),--Benin
('20200322',121,null,1   ,0  ,0  ,62),--Liberia
('20200322',210,null,0   ,0  ,0  ,62),--Tanzania
('20200322',135,null,0   ,0  ,0  ,62),--Mauritius
('20200322',237,null,0   ,0  ,0  ,62),--Zambia
('20200322',80 ,null,0   ,0  ,0  ,62),--Gambia
('20200322',42 ,null,0   ,0  ,0  ,62),--Chad
('20200322',154,null,0   ,0  ,0  ,62),--Niger
('20200322',34 ,null,2   ,0  ,0  ,62),--Cabo Verde
('20200322',238,null,1   ,0  ,0  ,62),--Zimbabwe
('20200322',126,null,3   ,0  ,0  ,62),--Madagascar
('20200322',6  ,null,2   ,0  ,0  ,62),--Angola
('20200322',68 ,null,1   ,0  ,0  ,62),--Eritrea
('20200322',224,null,1   ,0  ,0  ,62),--Uganda

('20200322',136,null,19  ,0  ,0  ,62),--Mayotte
('20200322',177,null,7   ,0  ,0  ,62),--Reunion

('20200322',58 ,null,0   ,0  ,0  ,62)--Diamond Princess



INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200323',45 ,null,103 ,9  ,0  ,63), --China
('20200323',181,null,64  ,7  ,0  ,63),--Republic of korea
('20200323',110,null,43  ,5  ,0  ,63),--Japan
('20200323',193,null,23  ,0  ,0  ,63),--Singapore
('20200323',12 ,null,315 ,0  ,0  ,63),--Australia
('20200323',128,null,123 ,7  ,0  ,63),--Malaysia
('20200323',233,null,19  ,0  ,0  ,63),--Vietnam
('20200323',169,null,73  ,6  ,0  ,63),--Philippines
('20200323',152,null,36  ,0  ,0  ,63),--New Zealand
('20200323',35 ,null,31  ,0  ,0  ,63),--Cambodia
('20200323',30 ,null,5   ,0  ,0  ,63),--Brunei
('20200323',141,null,0   ,0  ,0  ,63),--Mongolia
('20200323',74 ,null,0   ,0  ,0  ,63),--Fiji
('20200323',166,null,0   ,0  ,0  ,63),--Papua new Guinea

('20200323',78 ,null,2   ,0  ,0  ,63),--French Polynesia
('20200323',89 ,null,12  ,1  ,0  ,63),--Guam
('20200323',151,null,1   ,0  ,0  ,63),--New Caledonia

('20200323',107,null,5560,649,0  ,63),--Italy
('20200323',76 ,null,1525,112,0  ,63),--France
('20200323',82 ,null,3311,27 ,0  ,63),--Germany
('20200323',200,null,3646,349,0  ,63),--Spain
('20200323',225,null,669 ,48 ,0  ,63),--UK (United Kingdom)
('20200323',207,null,849 ,4  ,0  ,63),--Switzerland
('20200323',160,null,206 ,0  ,0  ,63),--Norway
('20200323',150,null,637 ,30 ,0  ,63),--Netherlands
('20200323',13 ,null,375 ,2  ,0  ,63),--Austria
('20200323',206,null,160 ,1  ,0  ,63),--Sweden
('20200323',99 ,null,95  ,0  ,0  ,63),--Iceland
('20200323',20 ,null,558 ,30 ,0  ,63),--Belgium
('20200323',186,null,0   ,0  ,0  ,63),--San Marino
('20200323',106,null,188 ,0  ,0  ,63),--Israel
('20200323',57 ,null,69  ,0  ,0  ,63),--Denmark
('20200323',51 ,null,29  ,0  ,0  ,63),--Croatia

('20200323',85 ,null,94  ,2  ,0  ,63),--Greece
('20200323',75 ,null,105 ,0  ,0  ,63),--Finland
('20200323',171,null,320 ,2  ,0  ,63),--Portugal
('20200323',19 ,null,0   ,0  ,0  ,63),--Belarus
('20200323',55 ,null,170 ,1  ,0  ,63),--Czechia
('20200323',174,null,66  ,2  ,0  ,63),--Romania
('20200323',14 ,null,12  ,0  ,0  ,63),--Azerbaijan
('20200323',81 ,null,5   ,0  ,0  ,63),--Georgia
('20200323',175,null,132 ,0  ,0  ,63),--Russia
('20200323',26 ,null,33  ,0  ,0  ,63),--Bosnia and Herzegovina
('20200323',2  ,null,13  ,0  ,0  ,63),--Albania
('20200323',69 ,null,20  ,0  ,0  ,63),--Estonia
('20200323',98 ,null,36  ,3  ,0  ,63),--Hungary
('20200323',104,null,121 ,1  ,0  ,63),--Ireland
('20200323',5  ,null,25  ,0  ,0  ,63),--Andorra
('20200323',10 ,null,30  ,0  ,0  ,63),--Armenia
('20200323',118,null,15  ,0  ,0  ,63),--Latvia
('20200323',123,null,38  ,0  ,0  ,63),--Lithuania
('20200323',124,null,128 ,0  ,0  ,63),--Luxembourg
('20200323',140,null,5   ,0  ,0  ,63),--Monaco
('20200323',158,null,29  ,1  ,0  ,63),--North Macedonia
('20200323',170,null,98  ,2  ,0  ,63),--Poland
('20200323',196,null,31  ,0  ,0  ,63),--Slovenia
('20200323',226,null,0   ,0  ,0  ,63),--Ukraine
('20200323',122,null,10  ,0  ,0  ,63),--Liechtenstein
('20200323',95 ,null,0   ,0  ,0  ,63),--Holy see
('20200323',195,null,7   ,0  ,0  ,63),--Slovakia
('20200323',31 ,null,22  ,0  ,0  ,63),--Bulgaria
('20200323',131,null,17  ,0  ,0  ,63),--Malta
('20200323',139,null,14  ,0  ,0  ,63),--Moldova
('20200323',54 ,null,11  ,0  ,0  ,63),--Cyprus
('20200323',190,null,39  ,0  ,0  ,63),--Serbia
('20200323',218,null,289 ,9  ,0  ,63),--Turkey
('20200323',112,null,4   ,0  ,0  ,63),--Kazakhstan
('20200323',229,null,13  ,0  ,0  ,63),--Uzbekistan
('20200323',142,null,7   ,0  ,0  ,63),--Montenegro
('20200323',116,null,0   ,0  ,0  ,63),--Kyrgyzstan


('20200323',241,null,31  ,1  ,0  ,63),--Kosovo
('20200323',72 ,null,23  ,0  ,0  ,63),--Faeroe Islands
('20200323',239,null,16  ,0  ,0  ,63),--Guernsey
('20200323',240,null,3   ,0  ,0  ,63),--Jersey
('20200323',84 ,null,5   ,0  ,0  ,63),--Gibraltar
('20200323',86 ,null,0   ,0  ,0  ,63),--Greenland
('20200323',105,null,0   ,0  ,0  ,63),--Isle of man

('20200323',211,null,310 ,0  ,0  ,63),--Thailand
('20200323',100,null,132 ,3  ,0  ,63),--India
('20200323',101,null,64  ,10 ,0  ,63),--Indonesia
('20200323',149,null,0   ,0  ,0  ,63),--Nepal
('20200323',201,null,10  ,0  ,0  ,63),--Sri Lanka
('20200323',129,null,0   ,0  ,0  ,63),--Maldives
('20200323',17 ,null,3   ,0  ,0  ,63),--Bangladesh
('20200323',24 ,null,0   ,0  ,0  ,63),--Bhutan
('20200323',212,null,0   ,0  ,0  ,63),--Timor-leste

('20200323',102,null,1028,129,0  ,63),--Iran
('20200323',115,null,13  ,0  ,0  ,63),--Kuwait
('20200323',16 ,null,31  ,1  ,0  ,63),--Bahrain
('20200323',103,null,19  ,3  ,0  ,63),--Iraq
('20200323',223,null,0   ,0  ,0  ,63),--UAE (United Arab Emirates)
('20200323',161,null,3   ,0  ,0  ,63),--Oman
('20200323',119,null,42  ,0  ,0  ,63),--lebanon
('20200323',173,null,24  ,0  ,0  ,63),--Qatar
('20200323',162,null,289 ,2  ,0  ,63),--Pakistan
('20200323',65 ,null,42  ,6  ,0  ,63),--Egypt
('20200323',144,null,29  ,1  ,0  ,63),--Morocco
('20200323',188,null,119 ,0  ,0  ,63),--Saudi Arabia
('20200323',1  ,null,16  ,1  ,0  ,63),--Afghanistan
('20200323',111,null,28  ,0  ,0  ,63),--Jordan
('20200323',217,null,15  ,2  ,0  ,63),--Tunisia
('20200323',204,null,0   ,0  ,0  ,63),--Sudan
('20200323',198,null,0   ,0  ,0  ,63),--Somalia
('20200323',59 ,null,0   ,0  ,0  ,63),--Djibouti
('20200323',242,null,1   ,0  ,0  ,63),--Syria

('20200323',164,null,7   ,0  ,0  ,63),--Palestine

('20200323',228,null,16354,201,0 ,63),--USA
('20200323',37 ,null,336 ,6  ,0  ,63),--Canada
('20200323',64 ,null,26  ,0  ,0  ,63),--Ecuador
('20200323',137,null,87  ,1  ,0  ,63),--Mexico
('20200323',28 ,null,0   ,0  ,0  ,63),--Brasil
('20200323',9  ,null,67  ,1  ,0  ,63),--Argentina
('20200323',44 ,null,198 ,0  ,0  ,63),--Chile
('20200323',62 ,null,0   ,0  ,0  ,63),--Dominican Republic
('20200323',46 ,null,0   ,0  ,0  ,63),--Colombia
('20200323',168,null,0   ,0  ,0  ,63),--Peru
('20200323',50 ,null,0   ,0  ,0  ,63),--Costa Rica
('20200323',167,null,4   ,0  ,0  ,63),--Paraguay
('20200323',165,null,108 ,2  ,0  ,63),--Panama
('20200323',25 ,null,5   ,0  ,0  ,63),--Bolivia
('20200323',109,null,3   ,0  ,0  ,63),--Jamaica
('20200323',96 ,null,2   ,0  ,0  ,63),--Honduras
('20200323',93 ,null,0   ,0  ,0  ,63),--Guyana
('20200323',203,null,0   ,0  ,0  ,63),--St. Vicent Grenadines
('20200323',52 ,null,0   ,0  ,0  ,63),--Cuba
('20200323',232,null,0   ,0  ,0  ,63),--Venezuela
('20200323',8  ,null,0   ,0  ,0  ,63),--Antigua and barbuda
('20200323',216,null,0   ,0  ,0  ,63),--Trinidad and Tobago
('20200323',227,null,41  ,0  ,0  ,63),--Uruguay
('20200323',182,null,0   ,0  ,0  ,63),--Saint lucia
('20200323',15 ,null,0   ,0  ,0  ,63),--Bahamas
('20200323',90 ,null,6   ,0  ,0  ,63),--Guatemala
('20200323',205,null,0   ,0  ,0  ,63),--suriname
('20200323',18,null ,0   ,0  ,0  ,63),--Barbados
('20200323',143,null,0   ,0  ,0  ,63),--Montserrat
('20200323',66 ,null,0   ,0  ,0  ,63),--El Salvador
('20200323',153,null,0   ,0  ,0  ,63),--Nicaragua
('20200323',94 ,null,0   ,0  ,0  ,63),--Haiti
('20200323',87 ,null,1   ,0  ,0  ,63),--Grenada


('20200323',88 ,null,5   ,0  ,0  ,63),--Guadeloupe
('20200323',133,null,0   ,0  ,0  ,63),--Martinique
('20200323',77 ,null,3   ,0  ,0  ,63),--French Guiana
('20200323',172,null,2   ,0  ,0  ,63),--Puerto rico
('20200323',183,null,1   ,0  ,0  ,63),--Saint Martin
('20200323',178,null,0   ,0  ,0  ,63),--Saint Barthelemy
('20200323',40 ,null,0   ,0  ,0  ,63),--Cayman Islands
('20200323',53 ,null,0   ,0  ,0  ,63),--Cura�ao
('20200323',222,null,0   ,0  ,0  ,63),--US Virgin Islands
('20200323',11 ,null,3   ,0  ,0  ,63),--Aruba
('20200323',194,null,0   ,0  ,0  ,63),--Sint Maarten
('20200323',23 ,null,0   ,0  ,0  ,63),--Bermuda

('20200323',3  ,null,107 ,2  ,0  ,63),--Algeria
('20200323',189,null,11  ,0  ,0  ,63),--Senegal 
('20200323',155,null,0   ,0  ,0  ,63),--Nigeria
('20200323',36, null,13  ,0  ,0  ,63),--Cameroon
('20200323',199,null,34  ,0  ,0  ,63),--South Africa
('20200323',213,null,1   ,0  ,0  ,63),--Togo
('20200323',32 ,null,3   ,0  ,0  ,63),--Burkina Faso
('20200323',63 ,null,7   ,1  ,0  ,63),--DRC Congo
('20200323',56 ,null,16  ,0  ,0  ,63),--Cote Divore
('20200323',71 ,null,2   ,0  ,0  ,63),--Ethiopia
('20200323',79 ,null,3   ,0  ,0  ,63),--Gabon
('20200323',83 ,null,5   ,0  ,0  ,63),--Ghana
('20200323',91 ,null,0   ,0  ,0  ,63),--Guinea
('20200323',113,null,8   ,0  ,0  ,63),--Kenya
('20200323',147,null,0   ,0  ,0  ,63),--Namibia
('20200323',41 ,null,1   ,0  ,0  ,63),--Central African Republic
('20200323',48 ,null,0   ,0  ,0  ,63),--Congo
('20200323',67 ,null,0   ,0  ,0  ,63),--Equatorial Guinea
('20200323',70 ,null,3   ,0  ,0  ,63),--Eswatini
('20200323',134,null,0   ,0  ,0  ,63),--Mauritania
('20200323',176,null,0   ,0  ,0  ,63),--Rwanda
('20200323',191,null,0   ,0  ,0  ,63),--Seychelles
('20200323',22 ,null,0   ,0  ,0  ,63),--Benin
('20200323',121,null,0   ,0  ,0  ,63),--Liberia
('20200323',210,null,6   ,0  ,0  ,63),--Tanzania
('20200323',135,null,0   ,0  ,0  ,63),--Mauritius
('20200323',237,null,1   ,0  ,0  ,63),--Zambia
('20200323',80 ,null,0   ,0  ,0  ,63),--Gambia
('20200323',42 ,null,0   ,0  ,0  ,63),--Chad
('20200323',154,null,0   ,0  ,0  ,63),--Niger
('20200323',34 ,null,0   ,0  ,0  ,63),--Cabo Verde
('20200323',238,null,0   ,0  ,0  ,63),--Zimbabwe
('20200323',126,null,9   ,0  ,0  ,63),--Madagascar
('20200323',6  ,null,0   ,0  ,0  ,63),--Angola
('20200323',68 ,null,0   ,0  ,0  ,63),--Eritrea
('20200323',224,null,0   ,0  ,0  ,63),--Uganda
('20200323',145,null,1   ,0  ,0  ,63),--Mozambique

('20200323',136,null,3   ,0  ,0  ,63),--Mayotte
('20200323',177,null,17  ,0  ,0  ,63),--Reunion

('20200323',58 ,null,0   ,0  ,0  ,63)--Diamond Princess






INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200324',45 ,null,146 ,7  ,0  ,64), --China
('20200324',181,null,76  ,9  ,0  ,64),--Republic of korea
('20200324',110,null,93  ,1  ,0  ,64),--Japan
('20200324',193,null,52  ,0  ,0  ,64),--Singapore
('20200324',12 ,null,313 ,0  ,0  ,64),--Australia
('20200324',128,null,212 ,4  ,0  ,64),--Malaysia
('20200324',233,null,10  ,0  ,0  ,64),--Vietnam
('20200324',169,null,82  ,8  ,0  ,64),--Philippines
('20200324',152,null,0   ,0  ,0  ,64),--New Zealand
('20200324',35 ,null,3   ,0  ,0  ,64),--Cambodia
('20200324',30 ,null,3   ,0  ,0  ,64),--Brunei
('20200324',141,null,0   ,0  ,0  ,64),--Mongolia
('20200324',74 ,null,1   ,0  ,0  ,64),--Fiji
('20200324',166,null,0   ,0  ,0  ,64),--Papua new Guinea

('20200324',78 ,null,1   ,0  ,0  ,64),--French Polynesia
('20200324',89 ,null,2   ,0  ,0  ,64),--Guam
('20200324',151,null,3   ,0  ,0  ,64),--New Caledonia

('20200324',107,null,4789,601,0  ,64),--Italy
('20200324',76 ,null,3794,186,0  ,64),--France
('20200324',82 ,null,4438,32 ,0  ,64),--Germany
('20200324',200,null,4517,462,0  ,64),--Spain
('20200324',225,null,967 ,54 ,0  ,64),--UK (United Kingdom)
('20200324',207,null,1044,6  ,0  ,64),--Switzerland
('20200324',160,null,239 ,1  ,0  ,64),--Norway
('20200324',150,null,545 ,34 ,0  ,64),--Netherlands
('20200324',13 ,null,855 ,9  ,0  ,64),--Austria
('20200324',206,null,110 ,4  ,0  ,64),--Sweden
('20200324',99 ,null,20  ,1  ,0  ,64),--Iceland
('20200324',20 ,null,343 ,13 ,0  ,64),--Belgium
('20200324',186,null,36  ,0  ,0  ,64),--San Marino
('20200324',106,null,167 ,0  ,0  ,64),--Israel
('20200324',57 ,null,65  ,11 ,0  ,64),--Denmark
('20200324',51 ,null,71  ,0  ,0  ,64),--Croatia

('20200324',85 ,null,71  ,2  ,0  ,64),--Greece
('20200324',75 ,null,74  ,0  ,0  ,64),--Finland
('20200324',171,null,460 ,9  ,0  ,64),--Portugal
('20200324',19 ,null,5   ,0  ,0  ,64),--Belarus
('20200324',55 ,null,71  ,0  ,0  ,64),--Czechia
('20200324',174,null,143 ,5  ,0  ,64),--Romania
('20200324',14 ,null,7   ,0  ,0  ,64),--Azerbaijan
('20200324',81 ,null,13  ,0  ,0  ,64),--Georgia
('20200324',175,null,0   ,0  ,0  ,64),--Russia
('20200324',26 ,null,6   ,0  ,0  ,64),--Bosnia and Herzegovina
('20200324',2  ,null,64  ,2  ,0  ,64),--Albania
('20200324',69 ,null,26  ,0  ,0  ,64),--Estonia
('20200324',98 ,null,20  ,1  ,0  ,64),--Hungary
('20200324',104,null,219 ,2  ,0  ,64),--Ireland
('20200324',5  ,null,51  ,1  ,0  ,64),--Andorra
('20200324',10 ,null,45  ,0  ,0  ,64),--Armenia
('20200324',118,null,41  ,0  ,0  ,64),--Latvia
('20200324',123,null,36  ,0  ,0  ,64),--Lithuania
('20200324',124,null,77  ,0  ,0  ,64),--Luxembourg
('20200324',140,null,0   ,0  ,0  ,64),--Monaco
('20200324',158,null,22  ,1  ,0  ,64),--North Macedonia
('20200324',170,null,115 ,1  ,0  ,64),--Poland
('20200324',196,null,28  ,0  ,0  ,64),--Slovenia
('20200324',226,null,37  ,0  ,0  ,64),--Ukraine
('20200324',122,null,0   ,0  ,0  ,64),--Liechtenstein
('20200324',95 ,null,0   ,0  ,0  ,64),--Holy see
('20200324',195,null,6   ,0  ,0  ,64),--Slovakia
('20200324',31 ,null,16  ,0  ,0  ,64),--Bulgaria
('20200324',131,null,17  ,0  ,0  ,64),--Malta
('20200324',139,null,15  ,0  ,0  ,64),--Moldova
('20200324',54 ,null,21  ,0  ,0  ,64),--Cyprus
('20200324',190,null,61  ,2  ,0  ,64),--Serbia
('20200324',218,null,0   ,0  ,0  ,64),--Turkey
('20200324',112,null,3   ,0  ,0  ,64),--Kazakhstan
('20200324',229,null,0   ,0  ,0  ,64),--Uzbekistan
('20200324',142,null,1   ,0  ,0  ,64),--Montenegro
('20200324',116,null,2   ,0  ,0  ,64),--Kyrgyzstan


('20200324',241,null,30  ,0  ,0  ,64),--Kosovo
('20200324',72 ,null,3   ,0  ,0  ,64),--Faeroe Islands
('20200324',239,null,3   ,0  ,0  ,64),--Guernsey
('20200324',240,null,3   ,0  ,0  ,64),--Jersey
('20200324',84 ,null,0   ,0  ,0  ,64),--Gibraltar
('20200324',86 ,null,2   ,0  ,0  ,64),--Greenland
('20200324',105,null,11  ,0  ,0  ,64),--Isle of man

('20200324',211,null,106 ,3  ,0  ,64),--Thailand
('20200324',100,null,19  ,2  ,0  ,64),--India
('20200324',101,null,65  ,1  ,0  ,64),--Indonesia
('20200324',149,null,1   ,0  ,0  ,64),--Nepal
('20200324',201,null,15  ,0  ,0  ,64),--Sri Lanka
('20200324',129,null,0   ,0  ,0  ,64),--Maldives
('20200324',17 ,null,6   ,1  ,0  ,64),--Bangladesh
('20200324',24 ,null,0   ,0  ,0  ,64),--Bhutan
('20200324',212,null,0   ,0  ,0  ,64),--Timor-leste
('20200324',146,null,2   ,0  ,0  ,64),--Myanmar

('20200324',102,null,1411,127,0  ,64),--Iran
('20200324',115,null,2   ,0  ,0  ,64),--Kuwait
('20200324',16 ,null,40  ,0  ,0  ,64),--Bahrain
('20200324',103,null,33  ,3  ,0  ,64),--Iraq
('20200324',223,null,45  ,0  ,0  ,64),--UAE (United Arab Emirates)
('20200324',161,null,28  ,0  ,0  ,64),--Oman
('20200324',119,null,19  ,0  ,0  ,64),--lebanon
('20200324',173,null,7   ,0  ,0  ,64),--Qatar
('20200324',162,null,103 ,1  ,0  ,64),--Pakistan
('20200324',65 ,null,39  ,5  ,0  ,64),--Egypt
('20200324',144,null,28  ,0  ,0  ,64),--Morocco
('20200324',188,null,51  ,0  ,0  ,64),--Saudi Arabia
('20200324',1  ,null,2   ,0  ,0  ,64),--Afghanistan
('20200324',111,null,15  ,0  ,0  ,64),--Jordan
('20200324',217,null,14  ,0  ,0  ,64),--Tunisia
('20200324',204,null,0   ,0  ,0  ,64),--Sudan
('20200324',198,null,0   ,0  ,0  ,64),--Somalia
('20200324',59 ,null,2   ,0  ,0  ,64),--Djibouti
('20200324',242,null,0   ,0  ,0  ,64),--Syria

('20200324',164,null,0   ,0  ,0  ,64),--Palestine

('20200324',228,null,10591,69 ,0 ,64),--USA
('20200324',37 ,null,48  ,1  ,0  ,64),--Canada
('20200324',64 ,null,258 ,8  ,0  ,64),--Ecuador
('20200324',137,null,119 ,2  ,0  ,64),--Mexico
('20200324',28 ,null,642 ,14 ,0  ,64),--Brasil
('20200324',9  ,null,41  ,0  ,0  ,64),--Argentina
('20200324',44 ,null,114 ,0  ,0  ,64),--Chile
('20200324',62 ,null,173 ,1  ,0  ,64),--Dominican Republic
('20200324',46 ,null,81  ,1  ,0  ,64),--Colombia
('20200324',168,null,77  ,0  ,0  ,64),--Peru
('20200324',50 ,null,41  ,0  ,0  ,64),--Costa Rica
('20200324',167,null,0   ,0  ,0  ,64),--Paraguay
('20200324',165,null,100 ,3  ,0  ,64),--Panama
('20200324',25 ,null,3   ,0  ,0  ,64),--Bolivia
('20200324',109,null,0   ,0  ,0  ,64),--Jamaica
('20200324',96 ,null,4   ,0  ,0  ,64),--Honduras
('20200324',93 ,null,0   ,0  ,0  ,64),--Guyana
('20200324',203,null,0   ,0  ,0  ,64),--St. Vicent Grenadines
('20200324',52 ,null,24  ,0  ,0  ,64),--Cuba
('20200324',232,null,0   ,0  ,0  ,64),--Venezuela
('20200324',8  ,null,0   ,0  ,0  ,64),--Antigua and barbuda
('20200324',216,null,1   ,0  ,0  ,64),--Trinidad and Tobago
('20200324',227,null,27  ,0  ,0  ,64),--Uruguay
('20200324',182,null,1   ,0  ,0  ,64),--Saint lucia
('20200324',15 ,null,0   ,0  ,0  ,64),--Bahamas
('20200324',90 ,null,2   ,0  ,0  ,64),--Guatemala
('20200324',205,null,0   ,0  ,0  ,64),--suriname
('20200324',18,null ,12  ,0  ,0  ,64),--Barbados
('20200324',143,null,0   ,0  ,0  ,64),--Montserrat
('20200324',66 ,null,2   ,0  ,0  ,64),--El Salvador
('20200324',153,null,0   ,0  ,0  ,64),--Nicaragua
('20200324',94 ,null,4   ,0  ,0  ,64),--Haiti
('20200324',87 ,null,0   ,0  ,0  ,64),--Grenada
('20200324',21 ,null,1   ,0  ,0  ,64),--Belize
('20200324',60 ,null,1   ,0  ,0  ,64),--Dominica

('20200324',88 ,null,6   ,0  ,0  ,64),--Guadeloupe
('20200324',133,null,21  ,0  ,0  ,64),--Martinique
('20200324',77 ,null,2   ,0  ,0  ,64),--French Guiana
('20200324',172,null,8   ,1  ,0  ,64),--Puerto rico
('20200324',183,null,3   ,0  ,0  ,64),--Saint Martin
('20200324',178,null,0   ,0  ,0  ,64),--Saint Barthelemy
('20200324',40 ,null,2   ,0  ,0  ,64),--Cayman Islands
('20200324',53 ,null,1   ,0  ,0  ,64),--Cura�ao
('20200324',222,null,11  ,0  ,0  ,64),--US Virgin Islands
('20200324',11 ,null,1   ,0  ,0  ,64),--Aruba
('20200324',194,null,1   ,0  ,0  ,64),--Sint Maarten
('20200324',23 ,null,4   ,0  ,0  ,64),--Bermuda
('20200324',220,null,1   ,0  ,0  ,64),--Turks and Caicos

('20200324',3  ,null,30  ,0  ,0  ,64),--Algeria
('20200324',189,null,12  ,0  ,0  ,64),--Senegal 
('20200324',155,null,0   ,0  ,0  ,64),--Nigeria
('20200324',36, null,32  ,0  ,0  ,64),--Cameroon
('20200324',199,null,128 ,0  ,0  ,64),--South Africa
('20200324',213,null,2   ,0  ,0  ,64),--Togo
('20200324',32 ,null,24  ,0  ,0  ,64),--Burkina Faso
('20200324',63 ,null,6   ,0  ,0  ,64),--DRC Congo
('20200324',56 ,null,0   ,0  ,0  ,64),--Cote Divore
('20200324',71 ,null,0   ,0  ,0  ,64),--Ethiopia
('20200324',79 ,null,0   ,0  ,0  ,64),--Gabon
('20200324',83 ,null,3   ,2  ,0  ,64),--Ghana
('20200324',91 ,null,2   ,0  ,0  ,64),--Guinea
('20200324',113,null,1   ,0  ,0  ,64),--Kenya
('20200324',147,null,0   ,0  ,0  ,64),--Namibia
('20200324',41 ,null,0   ,0  ,0  ,64),--Central African Republic
('20200324',48 ,null,0   ,0  ,0  ,64),--Congo
('20200324',67 ,null,0   ,0  ,0  ,64),--Equatorial Guinea
('20200324',70 ,null,0   ,0  ,0  ,64),--Eswatini
('20200324',134,null,0   ,0  ,0  ,64),--Mauritania
('20200324',176,null,19  ,0  ,0  ,64),--Rwanda
('20200324',191,null,0   ,0  ,0  ,64),--Seychelles
('20200324',22 ,null,3   ,0  ,0  ,64),--Benin
('20200324',121,null,0   ,0  ,0  ,64),--Liberia
('20200324',210,null,0   ,0  ,0  ,64),--Tanzania
('20200324',135,null,24  ,0  ,0  ,64),--Mauritius
('20200324',237,null,0   ,0  ,0  ,64),--Zambia
('20200324',80 ,null,0   ,0  ,0  ,64),--Gambia
('20200324',42 ,null,2   ,0  ,0  ,64),--Chad
('20200324',154,null,1   ,0  ,0  ,64),--Niger
('20200324',34 ,null,0   ,0  ,0  ,64),--Cabo Verde
('20200324',238,null,0   ,1  ,0  ,64),--Zimbabwe
('20200324',126,null,1   ,0  ,0  ,64),--Madagascar
('20200324',6  ,null,0   ,0  ,0  ,64),--Angola
('20200324',68 ,null,0   ,0  ,0  ,64),--Eritrea
('20200324',224,null,8   ,0  ,0  ,64),--Uganda
('20200324',145,null,0   ,0  ,0  ,64),--Mozambique

('20200324',136,null,10  ,0  ,0  ,64),--Mayotte
('20200324',177,null,7   ,0  ,0  ,64),--Reunion

('20200324',58 ,null,0   ,0  ,0  ,64)--Diamond Princess




INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200325',45 ,null,101 ,5  ,0  ,65), --China
('20200325',181,null,100 ,6  ,0  ,65),--Republic of korea
('20200325',110,null,65  ,1  ,0  ,65),--Japan
('20200325',193,null,51  ,0  ,0  ,65),--Singapore
('20200325',12 ,null,543 ,1  ,0  ,65),--Australia
('20200325',128,null,106 ,0  ,0  ,65),--Malaysia
('20200325',233,null,11  ,0  ,0  ,65),--Vietnam
('20200325',169,null,90  ,2  ,0  ,65),--Philippines
('20200325',152,null,87  ,0  ,0  ,65),--New Zealand
('20200325',35 ,null,4   ,0  ,0  ,65),--Cambodia
('20200325',30 ,null,13  ,0  ,0  ,65),--Brunei
('20200325',141,null,0   ,0  ,0  ,65),--Mongolia
('20200325',74 ,null,1   ,0  ,0  ,65),--Fiji
('20200325',166,null,0   ,0  ,0  ,65),--Papua new Guinea
('20200325',117,null,2   ,0  ,0  ,65),--Laos

('20200325',78 ,null,7   ,0  ,0  ,65),--French Polynesia
('20200325',89 ,null,3   ,0  ,0  ,65),--Guam
('20200325',151,null,2   ,0  ,0  ,65),--New Caledonia

('20200325',107,null,5249,743,0  ,65),--Italy
('20200325',76 ,null,2410,240,0  ,65),--France
('20200325',82 ,null,2342,23 ,0  ,65),--Germany
('20200325',200,null,6584,514,0  ,65),--Spain
('20200325',225,null,1427,87 ,0  ,65),--UK (United Kingdom)
('20200325',207,null,774 ,20 ,0  ,65),--Switzerland
('20200325',160,null,195 ,2  ,0  ,65),--Norway
('20200325',150,null,811 ,63 ,0  ,65),--Netherlands
('20200325',13 ,null,796 ,5  ,0  ,65),--Austria
('20200325',206,null,256 ,11 ,0  ,65),--Sweden
('20200325',99 ,null,60  ,0  ,0  ,65),--Iceland
('20200325',20 ,null,526 ,34 ,0  ,65),--Belgium
('20200325',186,null,0   ,1  ,0  ,65),--San Marino
('20200325',106,null,932 ,4  ,0  ,65),--Israel
('20200325',57 ,null,131 ,8  ,0  ,65),--Denmark
('20200325',51 ,null,76  ,1  ,0  ,65),--Croatia

('20200325',85 ,null,48  ,3  ,0  ,65),--Greece
('20200325',75 ,null,92  ,0  ,0  ,65),--Finland
('20200325',171,null,302 ,10 ,0  ,65),--Portugal
('20200325',19 ,null,0   ,0  ,0  ,65),--Belarus
('20200325',55 ,null,158 ,2  ,0  ,65),--Czechia
('20200325',174,null,186 ,4  ,0  ,65),--Romania
('20200325',14 ,null,15  ,0  ,0  ,65),--Azerbaijan
('20200325',81 ,null,6   ,0  ,0  ,65),--Georgia
('20200325',175,null,220 ,0  ,0  ,65),--Russia
('20200325',26 ,null,33  ,1  ,0  ,65),--Bosnia and Herzegovina
('20200325',2  ,null,23  ,1  ,0  ,65),--Albania
('20200325',69 ,null,17  ,0  ,0  ,65),--Estonia
('20200325',98 ,null,39  ,2  ,0  ,65),--Hungary
('20200325',104,null,204 ,1  ,0  ,65),--Ireland
('20200325',5  ,null,24  ,0  ,0  ,65),--Andorra
('20200325',10 ,null,30  ,0  ,0  ,65),--Armenia
('20200325',118,null,17  ,0  ,0  ,65),--Latvia
('20200325',123,null,30  ,1  ,0  ,65),--Lithuania
('20200325',124,null,224 ,0  ,0  ,65),--Luxembourg
('20200325',140,null,0   ,0  ,0  ,65),--Monaco
('20200325',158,null,12  ,0  ,0  ,65),--North Macedonia
('20200325',170,null,152 ,2  ,0  ,65),--Poland
('20200325',196,null,38  ,2  ,0  ,65),--Slovenia
('20200325',226,null,29  ,1  ,0  ,65),--Ukraine
('20200325',122,null,1   ,0  ,0  ,65),--Liechtenstein
('20200325',95 ,null,0   ,0  ,0  ,65),--Holy see
('20200325',195,null,13  ,0  ,0  ,65),--Slovakia
('20200325',31 ,null,19  ,0  ,0  ,65),--Bulgaria
('20200325',131,null,13  ,0  ,0  ,65),--Malta
('20200325',139,null,16  ,0  ,0  ,65),--Moldova
('20200325',54 ,null,8   ,3  ,0  ,65),--Cyprus
('20200325',190,null,54  ,1  ,0  ,65),--Serbia
('20200325',218,null,343 ,7  ,0  ,65),--Turkey
('20200325',112,null,16  ,0  ,0  ,65),--Kazakhstan
('20200325',229,null,4   ,0  ,0  ,65),--Uzbekistan
('20200325',142,null,7   ,0  ,0  ,65),--Montenegro
('20200325',116,null,26  ,0  ,0  ,65),--Kyrgyzstan

 
('20200325',241,null,2   ,0  ,0  ,65),--Kosovo
('20200325',72 ,null,4   ,0  ,0  ,65),--Faeroe Islands
('20200325',239,null,3   ,0  ,0  ,65),--Guernsey
('20200325',240,null,0   ,0  ,0  ,65),--Jersey
('20200325',84 ,null,0   ,0  ,0  ,65),--Gibraltar
('20200325',86 ,null,0   ,0  ,0  ,65),--Greenland
('20200325',105,null,10  ,0  ,0  ,65),--Isle of man

('20200325',211,null,107 ,0  ,0  ,65),--Thailand
('20200325',100,null,128 ,0  ,0  ,65),--India
('20200325',101,null,107 ,6  ,0  ,65),--Indonesia
('20200325',149,null,0   ,0  ,0  ,65),--Nepal
('20200325',201,null,5   ,0  ,0  ,65),--Sri Lanka
('20200325',129,null,0   ,0  ,0  ,65),--Maldives
('20200325',17 ,null,6   ,1  ,0  ,65),--Bangladesh
('20200325',24 ,null,0   ,0  ,0  ,65),--Bhutan
('20200325',212,null,0   ,0  ,0  ,65),--Timor-leste
('20200325',146,null,1   ,0  ,0  ,65),--Myanmar

('20200325',102,null,1762,122,0  ,65),--Iran
('20200325',115,null,4   ,0  ,0  ,65),--Kuwait
('20200325',16 ,null,15  ,1  ,0  ,65),--Bahrain
('20200325',103,null,50  ,4  ,0  ,65),--Iraq
('20200325',223,null,50  ,0  ,0  ,65),--UAE (United Arab Emirates)
('20200325',161,null,12  ,0  ,0  ,65),--Oman
('20200325',119,null,37  ,0  ,0  ,65),--lebanon
('20200325',173,null,25  ,0  ,0  ,65),--Qatar
('20200325',162,null,104 ,1  ,0  ,65),--Pakistan
('20200325',65 ,null,36  ,1  ,0  ,65),--Egypt
('20200325',144,null,27  ,1  ,0  ,65),--Morocco
('20200325',188,null,205 ,1  ,0  ,65),--Saudi Arabia
('20200325',1  ,null,32  ,0  ,0  ,65),--Afghanistan
('20200325',111,null,26  ,0  ,0  ,65),--Jordan
('20200325',217,null,25  ,0  ,0  ,65),--Tunisia
('20200325',204,null,1   ,0  ,0  ,65),--Sudan
('20200325',198,null,0   ,0  ,0  ,65),--Somalia
('20200325',59 ,null,0   ,0  ,0  ,65),--Djibouti
('20200325',242,null,0   ,0  ,0  ,65),--Syria
('20200325',243,null,1   ,0  ,0  ,65),--Libya

('20200325',164,null,1   ,0  ,0  ,65),--Palestine

('20200325',228,null,9750,202,0 ,65),--USA
('20200325',37 ,null,307 ,5  ,0  ,65),--Canada
('20200325',64 ,null,259 ,12 ,0  ,65),--Ecuador
('20200325',137,null,0   ,0  ,0  ,65),--Mexico
('20200325',28 ,null,655 ,21 ,0  ,65),--Brasil
('20200325',9  ,null,35  ,0  ,0  ,65),--Argentina
('20200325',44 ,null,176 ,1  ,0  ,65),--Chile
('20200325',62 ,null,67  ,3  ,0  ,65),--Dominican Republic
('20200325',46 ,null,29  ,0  ,0  ,65),--Colombia
('20200325',168,null,21  ,3  ,0  ,65),--Peru
('20200325',50 ,null,19  ,0  ,0  ,65),--Costa Rica
('20200325',167,null,5   ,1  ,0  ,65),--Paraguay
('20200325',165,null,0   ,0  ,0  ,65),--Panama
('20200325',25 ,null,1   ,0  ,0  ,65),--Bolivia
('20200325',109,null,2   ,0  ,0  ,65),--Jamaica
('20200325',96 ,null,0   ,0  ,0  ,65),--Honduras
('20200325',93 ,null,0   ,0  ,0  ,65),--Guyana
('20200325',203,null,0   ,0  ,0  ,65),--St. Vicent Grenadines
('20200325',52 ,null,8   ,0  ,0  ,65),--Cuba
('20200325',232,null,7   ,0  ,0  ,65),--Venezuela
('20200325',8  ,null,2   ,0  ,0  ,65),--Antigua and barbuda
('20200325',216,null,6   ,0  ,0  ,65),--Trinidad and Tobago
('20200325',227,null,0   ,0  ,0  ,65),--Uruguay
('20200325',182,null,0   ,0  ,0  ,65),--Saint lucia
('20200325',15 ,null,0   ,0  ,0  ,65),--Bahamas
('20200325',90 ,null,1   ,0  ,0  ,65),--Guatemala
('20200325',205,null,4   ,0  ,0  ,65),--suriname
('20200325',18,null ,1   ,0  ,0  ,65),--Barbados
('20200325',143,null,0   ,0  ,0  ,65),--Montserrat
('20200325',66 ,null,2   ,0  ,0  ,65),--El Salvador
('20200325',153,null,0   ,0  ,0  ,65),--Nicaragua
('20200325',94 ,null,1   ,0  ,0  ,65),--Haiti
('20200325',87 ,null,0   ,0  ,0  ,65),--Grenada
('20200325',21 ,null,0   ,0  ,0  ,65),--Belize
('20200325',60 ,null,1   ,0  ,0  ,65),--Dominica

('20200325',88 ,null,11  ,0  ,0  ,65),--Guadeloupe
('20200325',133,null,4   ,0  ,0  ,65),--Martinique
('20200325',77 ,null,3   ,0  ,0  ,65),--French Guiana
('20200325',172,null,8   ,0  ,0  ,65),--Puerto rico
('20200325',183,null,0   ,0  ,0  ,65),--Saint Martin
('20200325',178,null,0   ,0  ,0  ,65),--Saint Barthelemy
('20200325',40 ,null,0   ,0  ,0  ,65),--Cayman Islands
('20200325',53 ,null,2   ,0  ,0  ,65),--Cura�ao
('20200325',222,null,0   ,0  ,0  ,65),--US Virgin Islands
('20200325',11 ,null,3   ,0  ,0  ,65),--Aruba
('20200325',194,null,0   ,0  ,0  ,65),--Sint Maarten
('20200325',23 ,null,0   ,0  ,0  ,65),--Bermuda
('20200325',220,null,0   ,0  ,0  ,65),--Turks and Caicos

('20200325',3  ,null,33  ,0  ,0  ,65),--Algeria
('20200325',189,null,7   ,0  ,0  ,65),--Senegal 
('20200325',155,null,0   ,0  ,0  ,65),--Nigeria
('20200325',36, null,0   ,1  ,0  ,65),--Cameroon
('20200325',199,null,152 ,0  ,0  ,65),--South Africa
('20200325',213,null,0   ,0  ,0  ,65),--Togo
('20200325',32 ,null,15  ,0  ,0  ,65),--Burkina Faso
('20200325',63 ,null,9   ,0  ,0  ,65),--DRC Congo
('20200325',56 ,null,47  ,0  ,0  ,65),--Cote Divore
('20200325',71 ,null,0   ,0  ,0  ,65),--Ethiopia
('20200325',79 ,null,0   ,0  ,0  ,65),--Gabon
('20200325',83 ,null,26  ,0  ,0  ,65),--Ghana
('20200325',91 ,null,0   ,0  ,0  ,65),--Guinea
('20200325',113,null,0   ,0  ,0  ,65),--Kenya
('20200325',147,null,1   ,0  ,0  ,65),--Namibia
('20200325',41 ,null,0   ,0  ,0  ,65),--Central African Republic
('20200325',48 ,null,0   ,0  ,0  ,65),--Congo
('20200325',67 ,null,0   ,0  ,0  ,65),--Equatorial Guinea
('20200325',70 ,null,0   ,0  ,0  ,65),--Eswatini
('20200325',134,null,0   ,0  ,0  ,65),--Mauritania
('20200325',176,null,0   ,0  ,0  ,65),--Rwanda
('20200325',191,null,0   ,0  ,0  ,65),--Seychelles
('20200325',22 ,null,0   ,0  ,0  ,65),--Benin
('20200325',121,null,0   ,0  ,0  ,65),--Liberia
('20200325',210,null,0   ,0  ,0  ,65),--Tanzania
('20200325',135,null,0   ,0  ,0  ,65),--Mauritius
('20200325',237,null,0   ,0  ,0  ,65),--Zambia
('20200325',80 ,null,1   ,0  ,0  ,65),--Gambia
('20200325',42 ,null,0   ,0  ,0  ,65),--Chad
('20200325',154,null,0   ,0  ,0  ,65),--Niger
('20200325',34 ,null,0   ,0  ,0  ,65),--Cabo Verde
('20200325',238,null,0   ,0  ,0  ,65),--Zimbabwe
('20200325',126,null,0   ,0  ,0  ,65),--Madagascar
('20200325',6  ,null,0   ,0  ,0  ,65),--Angola
('20200325',68 ,null,0   ,0  ,0  ,65),--Eritrea
('20200325',224,null,0   ,0  ,0  ,65),--Uganda
('20200325',145,null,2   ,0  ,0  ,65),--Mozambique

('20200325',136,null,6   ,0  ,0  ,65),--Mayotte
('20200325',177,null,12  ,0  ,0  ,65),--Reunion

('20200325',58 ,null,0   ,0  ,0  ,65)--Diamond Princess





INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200326',45 ,null,113 ,6  ,0  ,66), --China
('20200326',181,null,105 ,5  ,0  ,66),--Republic of korea
('20200326',110,null,98  ,2  ,0  ,66),--Japan
('20200326',193,null,73  ,0  ,0  ,66),--Singapore
('20200326',12 ,null,547 ,3  ,0  ,66),--Australia
('20200326',128,null,172 ,3  ,0  ,66),--Malaysia
('20200326',233,null,7   ,0  ,0  ,66),--Vietnam
('20200326',169,null,84  ,3  ,0  ,66),--Philippines
('20200326',152,null,73  ,0  ,0  ,66),--New Zealand
('20200326',35 ,null,5   ,0  ,0  ,66),--Cambodia
('20200326',30 ,null,5   ,0  ,0  ,66),--Brunei
('20200326',141,null,0   ,0  ,0  ,66),--Mongolia
('20200326',74 ,null,1   ,0  ,0  ,66),--Fiji
('20200326',166,null,0   ,0  ,0  ,66),--Papua new Guinea
('20200326',117,null,1   ,0  ,0  ,66),--Laos

('20200326',78 ,null,0   ,0  ,0  ,66),--French Polynesia
('20200326',89 ,null,5   ,0  ,0  ,66),--Guam
('20200326',151,null,4   ,0  ,0  ,66),--New Caledonia

('20200326',107,null,5210,685,0  ,66),--Italy
('20200326',76 ,null,2895,231,0  ,66),--France
('20200326',82 ,null,4954,49 ,0  ,66),--Germany
('20200326',200,null,7937,738,0  ,66),--Spain
('20200326',225,null,1452,41 ,0  ,66),--UK (United Kingdom)
('20200326',207,null,925 ,17 ,0  ,66),--Switzerland
('20200326',160,null,350 ,2  ,0  ,66),--Norway
('20200326',150,null,852 ,80 ,0  ,66),--Netherlands
('20200326',13 ,null,606 ,4  ,0  ,66),--Austria
('20200326',206,null,238 ,6  ,0  ,66),--Sweden
('20200326',99 ,null,89  ,0  ,0  ,66),--Iceland
('20200326',20 ,null,668 ,56 ,0  ,66),--Belgium
('20200326',186,null,21  ,0  ,0  ,66),--San Marino
('20200326',106,null,199 ,0  ,0  ,66),--Israel
('20200326',57 ,null,133 ,2  ,0  ,66),--Denmark
('20200326',51 ,null,36  ,0  ,0  ,66),--Croatia

('20200326',85 ,null,78  ,2  ,0  ,66),--Greece
('20200326',75 ,null,88  ,2  ,0  ,66),--Finland
('20200326',171,null,633 ,10 ,0  ,66),--Portugal
('20200326',19 ,null,5   ,0  ,0  ,66),--Belarus
('20200326',55 ,null,260 ,3  ,0  ,66),--Czechia
('20200326',174,null,144 ,2  ,0  ,66),--Romania
('20200326',14 ,null,6   ,1  ,0  ,66),--Azerbaijan
('20200326',81 ,null,4   ,0  ,0  ,66),--Georgia
('20200326',175,null,182 ,2  ,0  ,66),--Russia
('20200326',26 ,null,9   ,1  ,0  ,66),--Bosnia and Herzegovina
('20200326',2  ,null,28  ,0  ,0  ,66),--Albania
('20200326',69 ,null,35  ,1  ,0  ,66),--Estonia
('20200326',98 ,null,35  ,0  ,0  ,66),--Hungary
('20200326',104,null,235 ,2  ,0  ,66),--Ireland
('20200326',5  ,null,25  ,2  ,0  ,66),--Andorra
('20200326',10 ,null,25  ,0  ,0  ,66),--Armenia
('20200326',118,null,24  ,0  ,0  ,66),--Latvia
('20200326',123,null,65  ,2  ,0  ,66),--Lithuania
('20200326',124,null,234 ,0  ,0  ,66),--Luxembourg
('20200326',140,null,0   ,0  ,0  ,66),--Monaco
('20200326',158,null,29  ,0  ,0  ,66),--North Macedonia
('20200326',170,null,150 ,4  ,0  ,66),--Poland
('20200326',196,null,48  ,1  ,0  ,66),--Slovenia
('20200326',226,null,43  ,1  ,0  ,66),--Ukraine
('20200326',122,null,0   ,0  ,0  ,66),--Liechtenstein
('20200326',95 ,null,3   ,0  ,0  ,66),--Holy see
('20200326',195,null,12  ,0  ,0  ,66),--Slovakia
('20200326',31 ,null,22  ,0  ,0  ,66),--Bulgaria
('20200326',131,null,9   ,0  ,0  ,66),--Malta
('20200326',139,null,24  ,0  ,0  ,66),--Moldova
('20200326',54 ,null,8   ,0  ,0  ,66),--Cyprus
('20200326',190,null,81  ,1  ,0  ,66),--Serbia
('20200326',218,null,561 ,15 ,0  ,66),--Turkey
('20200326',112,null,18  ,0  ,0  ,66),--Kazakhstan
('20200326',229,null,15  ,0  ,0  ,66),--Uzbekistan
('20200326',142,null,23  ,1  ,0  ,66),--Montenegro
('20200326',116,null,2   ,0  ,0  ,66),--Kyrgyzstan

 
('20200326',241,null,8   ,0  ,0  ,66),--Kosovo
('20200326',72 ,null,10  ,0  ,0  ,66),--Faeroe Islands
('20200326',239,null,7   ,0  ,0  ,66),--Guernsey
('20200326',240,null,2   ,0  ,0  ,66),--Jersey
('20200326',84 ,null,11  ,0  ,0  ,66),--Gibraltar
('20200326',86 ,null,1   ,0  ,0  ,66),--Greenland
('20200326',105,null,0   ,0  ,0  ,66),--Isle of man

('20200326',211,null,0   ,0  ,0  ,66),--Thailand
('20200326',100,null,87  ,4  ,0  ,66),--India
('20200326',101,null,104 ,3  ,0  ,66),--Indonesia
('20200326',149,null,1   ,0  ,0  ,66),--Nepal
('20200326',201,null,0   ,0  ,0  ,66),--Sri Lanka
('20200326',129,null,0   ,0  ,0  ,66),--Maldives
('20200326',17 ,null,0   ,0  ,0  ,66),--Bangladesh
('20200326',24 ,null,0   ,0  ,0  ,66),--Bhutan
('20200326',212,null,0   ,0  ,0  ,66),--Timor-leste
('20200326',146,null,0   ,0  ,0  ,66),--Myanmar

('20200326',102,null,2206,143,0  ,66),--Iran
('20200326',115,null,13  ,0  ,0  ,66),--Kuwait
('20200326',16 ,null,27  ,1  ,0  ,66),--Bahrain
('20200326',103,null,30  ,2  ,0  ,66),--Iraq
('20200326',223,null,85  ,0  ,0  ,66),--UAE (United Arab Emirates)
('20200326',161,null,0   ,0  ,0  ,66),--Oman
('20200326',119,null,29  ,0  ,0  ,66),--lebanon
('20200326',173,null,11  ,0  ,0  ,66),--Qatar
('20200326',162,null,66  ,1  ,0  ,66),--Pakistan
('20200326',65 ,null,54  ,1  ,0  ,66),--Egypt
('20200326',144,null,55  ,1  ,0  ,66),--Morocco
('20200326',188,null,133 ,1  ,0  ,66),--Saudi Arabia
('20200326',1  ,null,6   ,1  ,0  ,66),--Afghanistan
('20200326',111,null,19  ,0  ,0  ,66),--Jordan
('20200326',217,null,59  ,2  ,0  ,66),--Tunisia
('20200326',204,null,0   ,0  ,0  ,66),--Sudan
('20200326',198,null,1   ,0  ,0  ,66),--Somalia
('20200326',59 ,null,9   ,0  ,0  ,66),--Djibouti
('20200326',242,null,4   ,0  ,0  ,66),--Syria
('20200326',243,null,0   ,0  ,0  ,66),--Libya

('20200326',164,null,4   ,1  ,0  ,66),--Palestine

('20200326',228,null,11656,211,0 ,66),--USA
('20200326',37 ,null,1670,10 ,0  ,66),--Canada
('20200326',64 ,null,162 ,2  ,0  ,66),--Ecuador
('20200326',137,null,108 ,1  ,0  ,66),--Mexico
('20200326',28 ,null,232 ,11 ,0  ,66),--Brasil
('20200326',9  ,null,86  ,2  ,0  ,66),--Argentina
('20200326',44 ,null,220 ,1  ,0  ,66),--Chile
('20200326',62 ,null,80  ,4  ,0  ,66),--Dominican Republic
('20200326',46 ,null,164 ,1  ,0  ,66),--Colombia
('20200326',168,null,64  ,4  ,0  ,66),--Peru
('20200326',50 ,null,24  ,0  ,0  ,66),--Costa Rica
('20200326',167,null,14  ,1  ,0  ,66),--Paraguay
('20200326',165,null,213 ,2  ,0  ,66),--Panama
('20200326',25 ,null,11  ,0  ,0  ,66),--Bolivia
('20200326',109,null,5   ,0  ,0  ,66),--Jamaica
('20200326',96 ,null,22  ,0  ,0  ,66),--Honduras
('20200326',93 ,null,0   ,0  ,0  ,66),--Guyana
('20200326',203,null,0   ,0  ,0  ,66),--St. Vicent Grenadines
('20200326',52 ,null,9   ,0  ,0  ,66),--Cuba
('20200326',232,null,14  ,0  ,0  ,66),--Venezuela
('20200326',8  ,null,0   ,0  ,0  ,66),--Antigua and barbuda
('20200326',216,null,3   ,1  ,0  ,66),--Trinidad and Tobago
('20200326',227,null,0   ,0  ,0  ,66),--Uruguay
('20200326',182,null,0   ,0  ,0  ,66),--Saint lucia
('20200326',15 ,null,1   ,0  ,0  ,66),--Bahamas
('20200326',90 ,null,3   ,0  ,0  ,66),--Guatemala
('20200326',205,null,1   ,0  ,0  ,66),--suriname
('20200326',18,null ,0   ,0  ,0  ,66),--Barbados
('20200326',143,null,1   ,0  ,0  ,66),--Montserrat
('20200326',66 ,null,8   ,0  ,0  ,66),--El Salvador
('20200326',153,null,0   ,0  ,0  ,66),--Nicaragua
('20200326',94 ,null,1   ,0  ,0  ,66),--Haiti
('20200326',87 ,null,0   ,0  ,0  ,66),--Grenada
('20200326',21 ,null,1   ,0  ,0  ,66),--Belize
('20200326',60 ,null,5   ,0  ,0  ,66),--Dominica
('20200326',180,null,2   ,0  ,0  ,66),--saint kitts & nevis

('20200326',88 ,null,3   ,0  ,0  ,66),--Guadeloupe
('20200326',133,null,9   ,1  ,0  ,66),--Martinique
('20200326',77 ,null,5   ,0  ,0  ,66),--French Guiana
('20200326',172,null,12  ,0  ,0  ,66),--Puerto rico
('20200326',183,null,3   ,0  ,0  ,66),--Saint Martin
('20200326',178,null,0   ,0  ,0  ,66),--Saint Barthelemy
('20200326',40 ,null,3   ,0  ,0  ,66),--Cayman Islands
('20200326',53 ,null,0   ,0  ,0  ,66),--Cura�ao
('20200326',222,null,0   ,0  ,0  ,66),--US Virgin Islands
('20200326',29 ,null,7   ,0  ,0  ,66),--Aruba
('20200326',194,null,0   ,0  ,0  ,66),--Sint Maarten
('20200326',23 ,null,1   ,0  ,0  ,66),--Bermuda
('20200326',220,null,0   ,0  ,0  ,66),--Turks and Caicos

('20200326',3  ,null,0   ,0  ,0  ,66),--Algeria
('20200326',189,null,13  ,0  ,0  ,66),--Senegal 
('20200326',155,null,4   ,1  ,0  ,66),--Nigeria
('20200326',36, null,0   ,0  ,0  ,66),--Cameroon
('20200326',199,null,155 ,0  ,0  ,66),--South Africa
('20200326',213,null,3   ,0  ,0  ,66),--Togo
('20200326',32 ,null,32  ,0  ,0  ,66),--Burkina Faso
('20200326',63 ,null,6   ,1  ,0  ,66),--DRC Congo
('20200326',56 ,null,8   ,0  ,0  ,66),--Cote Divore
('20200326',71 ,null,0   ,0  ,0  ,66),--Ethiopia
('20200326',79 ,null,0   ,0  ,0  ,66),--Gabon
('20200326',83 ,null,15  ,0  ,0  ,66),--Ghana
('20200326',91 ,null,0   ,0  ,0  ,66),--Guinea
('20200326',113,null,0   ,0  ,0  ,66),--Kenya
('20200326',147,null,0   ,0  ,0  ,66),--Namibia
('20200326',41 ,null,1   ,0  ,0  ,66),--Central African Republic
('20200326',48 ,null,0   ,0  ,0  ,66),--Congo
('20200326',67 ,null,0   ,0  ,0  ,66),--Equatorial Guinea
('20200326',70 ,null,0   ,0  ,0  ,66),--Eswatini
('20200326',134,null,0   ,0  ,0  ,66),--Mauritania
('20200326',176,null,1   ,0  ,0  ,66),--Rwanda
('20200326',191,null,0   ,0  ,0  ,66),--Seychelles
('20200326',22 ,null,0   ,0  ,0  ,66),--Benin
('20200326',121,null,0   ,0  ,0  ,66),--Liberia
('20200326',210,null,1   ,0  ,0  ,66),--Tanzania
('20200326',135,null,5   ,0  ,0  ,66),--Mauritius
('20200326',237,null,0   ,0  ,0  ,66),--Zambia
('20200326',80 ,null,0   ,0  ,0  ,66),--Gambia
('20200326',42 ,null,0   ,0  ,0  ,66),--Chad
('20200326',154,null,0   ,0  ,0  ,66),--Niger
('20200326',34 ,null,0   ,0  ,0  ,66),--Cabo Verde
('20200326',238,null,0   ,0  ,0  ,66),--Zimbabwe
('20200326',126,null,0   ,0  ,0  ,66),--Madagascar
('20200326',6  ,null,0   ,0  ,0  ,66),--Angola
('20200326',68 ,null,3   ,0  ,0  ,66),--Eritrea
('20200326',224,null,5   ,0  ,0  ,66),--Uganda
('20200326',145,null,2   ,0  ,0  ,66),--Mozambique
('20200326',92 ,null,2   ,0  ,0  ,66),--Guinea-bissau
('20200326',130,null,2   ,0  ,0  ,66),--Mali

('20200326',136,null,5   ,0  ,0  ,66),--Mayotte
('20200326',177,null,11  ,0  ,0  ,66),--Reunion

('20200326',58 ,null,0   ,0  ,0  ,66)--Diamond Princess





INSERT Report(Date,CountryId,RegionId,Cases,Deaths,Recovered,id)
VALUES
('20200327',45 ,null,117 ,5  ,0  ,67), --China
('20200327',181,null,91  ,8  ,0  ,67),--Republic of korea
('20200327',110,null,97  ,1  ,0  ,67),--Japan
('20200327',193,null,52  ,0  ,0  ,67),--Singapore
('20200327',12 ,null,186 ,2  ,0  ,67),--Australia
('20200327',128,null,235 ,4  ,0  ,67),--Malaysia
('20200327',233,null,12  ,0  ,0  ,67),--Vietnam
('20200327',169,null,71  ,7  ,0  ,67),--Philippines
('20200327',152,null,76  ,0  ,0  ,67),--New Zealand
('20200327',35 ,null,2   ,0  ,0  ,67),--Cambodia
('20200327',30 ,null,5   ,0  ,0  ,67),--Brunei
('20200327',141,null,1   ,0  ,0  ,67),--Mongolia
('20200327',74 ,null,0   ,0  ,0  ,67),--Fiji
('20200327',166,null,0   ,0  ,0  ,67),--Papua new Guinea
('20200327',117,null,3   ,0  ,0  ,67),--Laos

('20200327',78 ,null,5   ,0  ,0  ,67),--French Polynesia
('20200327',89 ,null,8   ,0  ,0  ,67),--Guam
('20200327',151,null,0   ,0  ,0  ,67),--New Caledonia

('20200327',107,null,6153,660,0  ,67),--Italy
('20200327',76 ,null,3866,364,0  ,67),--France
('20200327',82 ,null,5780,55 ,0  ,67),--Germany
('20200327',200,null,8578,655,0  ,67),--Spain
('20200327',225,null,2129,115,0  ,67),--UK (United Kingdom)
('20200327',207,null,1000,58 ,0  ,67),--Switzerland
('20200327',160,null,240 ,2  ,0  ,67),--Norway
('20200327',150,null,1019,78 ,0  ,67),--Netherlands
('20200327',13 ,null,1141,18 ,0  ,67),--Austria
('20200327',206,null,296 ,24 ,0  ,67),--Sweden
('20200327',99 ,null,65  ,0  ,0  ,67),--Iceland
('20200327',20 ,null,1298,42 ,0  ,67),--Belgium
('20200327',186,null,10  ,0  ,0  ,67),--San Marino
('20200327',106,null,666 ,5  ,0  ,67),--Israel
('20200327',57 ,null,153 ,7  ,0  ,67),--Denmark
('20200327',51 ,null,77  ,1  ,0  ,67),--Croatia
('20200327',218,null,1196,16 ,0  ,67),--Turkey

('20200327',85 ,null,71  ,4  ,0  ,67),--Greece
('20200327',75 ,null,78  ,1  ,0  ,67),--Finland
('20200327',171,null,549 ,16 ,0  ,67),--Portugal
('20200327',19 ,null,0   ,0  ,0  ,67),--Belarus
('20200327',55 ,null,408 ,3  ,0  ,67),--Czechia
('20200327',174,null,123 ,4  ,0  ,67),--Romania
('20200327',14 ,null,29  ,1  ,0  ,67),--Azerbaijan
('20200327',81 ,null,4   ,0  ,0  ,67),--Georgia
('20200327',175,null,196 ,1  ,0  ,67),--Russia
('20200327',26 ,null,40  ,0  ,0  ,67),--Bosnia and Herzegovina
('20200327',2  ,null,12  ,3  ,0  ,67),--Albania
('20200327',69 ,null,134 ,0  ,0  ,67),--Estonia
('20200327',98 ,null,39  ,0  ,0  ,67),--Hungary
('20200327',104,null,255 ,10 ,0  ,67),--Ireland
('20200327',5  ,null,18  ,0  ,0  ,67),--Andorra
('20200327',10 ,null,39  ,1  ,0  ,67),--Armenia
('20200327',118,null,23  ,0  ,0  ,67),--Latvia
('20200327',123,null,25  ,0  ,0  ,67),--Lithuania
('20200327',124,null,120 ,1  ,0  ,67),--Luxembourg
('20200327',140,null,0   ,0  ,0  ,67),--Monaco
('20200327',158,null,24  ,1  ,0  ,67),--North Macedonia
('20200327',170,null,170 ,2  ,0  ,67),--Poland
('20200327',196,null,49  ,1  ,0  ,67),--Slovenia
('20200327',226,null,62  ,0  ,0  ,67),--Ukraine
('20200327',122,null,5   ,0  ,0  ,67),--Liechtenstein
('20200327',95 ,null,0   ,0  ,0  ,67),--Holy see
('20200327',195,null,0   ,0  ,0  ,67),--Slovakia
('20200327',31 ,null,22  ,0  ,0  ,67),--Bulgaria
('20200327',131,null,5   ,0  ,0  ,67),--Malta
('20200327',139,null,28  ,1  ,0  ,67),--Moldova
('20200327',54 ,null,14  ,0  ,0  ,67),--Cyprus
('20200327',190,null,73  ,3  ,0  ,67),--Serbia
('20200327',112,null,28  ,0  ,0  ,67),--Kazakhstan
('20200327',229,null,18  ,0  ,0  ,67),--Uzbekistan
('20200327',142,null,15  ,0  ,0  ,67),--Montenegro
('20200327',116,null,14  ,0  ,0  ,67),--Kyrgyzstan

 
('20200327',241,null,8   ,0  ,0  ,67),--Kosovo
('20200327',72 ,null,8   ,0  ,0  ,67),--Faeroe Islands
('20200327',239,null,4   ,0  ,0  ,67),--Guernsey
('20200327',240,null,14  ,1  ,0  ,67),--Jersey
('20200327',84 ,null,9   ,0  ,0  ,67),--Gibraltar
('20200327',86 ,null,1   ,0  ,0  ,67),--Greenland
('20200327',105,null,3   ,0  ,0  ,67),--Isle of man

('20200327',211,null,202 ,1  ,0  ,67),--Thailand
('20200327',100,null,75  ,4  ,0  ,67),--India
('20200327',101,null,103 ,20 ,0  ,67),--Indonesia
('20200327',149,null,0   ,0  ,0  ,67),--Nepal
('20200327',201,null,4   ,0  ,0  ,67),--Sri Lanka
('20200327',129,null,0   ,0  ,0  ,67),--Maldives
('20200327',17 ,null,9   ,1  ,0  ,67),--Bangladesh
('20200327',24 ,null,1   ,0  ,0  ,67),--Bhutan
('20200327',212,null,0   ,0  ,0  ,67),--Timor-leste
('20200327',146,null,2   ,0  ,0  ,67),--Myanmar

('20200327',102,null,2389,157,0  ,67),--Iran
('20200327',115,null,0   ,0  ,0  ,67),--Kuwait
('20200327',16 ,null,39  ,0  ,0  ,67),--Bahrain
('20200327',103,null,36  ,7  ,0  ,67),--Iraq
('20200327',223,null,0   ,0  ,0  ,67),--UAE (United Arab Emirates)
('20200327',161,null,10  ,0  ,0  ,67),--Oman
('20200327',119,null,35  ,2  ,0  ,67),--lebanon
('20200327',173,null,12  ,0  ,0  ,67),--Qatar
('20200327',162,null,0   ,0  ,0  ,67),--Pakistan
('20200327',65 ,null,39  ,3  ,0  ,67),--Egypt
('20200327',144,null,50  ,4  ,0  ,67),--Morocco
('20200327',188,null,112 ,1  ,0  ,67),--Saudi Arabia
('20200327',1  ,null,0   ,0  ,0  ,67),--Afghanistan
('20200327',111,null,40  ,0  ,0  ,67),--Jordan
('20200327',217,null,24  ,0  ,0  ,67),--Tunisia
('20200327',204,null,0   ,0  ,0  ,67),--Sudan
('20200327',198,null,1   ,0  ,0  ,67),--Somalia
('20200327',59 ,null,0   ,0  ,0  ,67),--Djibouti
('20200327',242,null,0   ,0  ,0  ,67),--Syria
('20200327',243,null,0   ,0  ,0  ,67),--Libya

('20200327',164,null,20  ,0  ,0  ,67),--Palestine

('20200327',228,null,4764,107,0 ,67),--USA
('20200327',37 ,null,146 ,0  ,0  ,67),--Canada
('20200327',64 ,null,0   ,0  ,0  ,67),--Ecuador
('20200327',137,null,0   ,0  ,0  ,67),--Mexico
('20200327',28 ,null,0   ,0  ,0  ,67),--Brasil
('20200327',9  ,null,115 ,2  ,0  ,67),--Argentina
('20200327',44 ,null,164 ,1  ,0  ,67),--Chile
('20200327',62 ,null,96  ,0  ,0  ,67),--Dominican Republic
('20200327',46 ,null,0   ,0  ,0  ,67),--Colombia
('20200327',168,null,100 ,0  ,0  ,67),--Peru
('20200327',50 ,null,0   ,0  ,0  ,67),--Costa Rica
('20200327',167,null,0   ,0  ,0  ,67),--Paraguay
('20200327',165,null,0   ,0  ,0  ,67),--Panama
('20200327',25 ,null,0   ,0  ,0  ,67),--Bolivia
('20200327',109,null,0   ,0  ,0  ,67),--Jamaica
('20200327',96 ,null,0   ,1  ,0  ,67),--Honduras
('20200327',93 ,null,0   ,0  ,0  ,67),--Guyana
('20200327',203,null,0   ,0  ,0  ,67),--St. Vicent Grenadines
('20200327',52 ,null,10  ,0  ,0  ,67),--Cuba
('20200327',232,null,0   ,0  ,0  ,67),--Venezuela
('20200327',8  ,null,0   ,0  ,0  ,67),--Antigua and barbuda
('20200327',216,null,1   ,0  ,0  ,67),--Trinidad and Tobago
('20200327',227,null,0   ,0  ,0  ,67),--Uruguay
('20200327',182,null,0   ,0  ,0  ,67),--Saint lucia
('20200327',15 ,null,0   ,0  ,0  ,67),--Bahamas
('20200327',90 ,null,0   ,0  ,0  ,67),--Guatemala
('20200327',205,null,0   ,0  ,0  ,67),--suriname
('20200327',18,null ,0   ,0  ,0  ,67),--Barbados
('20200327',143,null,0   ,0  ,0  ,67),--Montserrat
('20200327',66 ,null,0   ,0  ,0  ,67),--El Salvador
('20200327',153,null,0   ,0  ,0  ,67),--Nicaragua
('20200327',94 ,null,0   ,0  ,0  ,67),--Haiti
('20200327',87 ,null,6   ,0  ,0  ,67),--Grenada
('20200327',21 ,null,0   ,0  ,0  ,67),--Belize
('20200327',60 ,null,4   ,0  ,0  ,67),--Dominica
('20200327',180,null,0   ,0  ,0  ,67),--saint kitts & nevis

('20200327',88 ,null,0   ,0  ,0  ,67),--Guadeloupe
('20200327',133,null,0   ,0  ,0  ,67),--Martinique
('20200327',77 ,null,0   ,0  ,0  ,67),--French Guiana
('20200327',172,null,13  ,0  ,0  ,67),--Puerto rico
('20200327',183,null,0   ,0  ,0  ,67),--Saint Martin
('20200327',178,null,0   ,0  ,0  ,67),--Saint Barthelemy
('20200327',40 ,null,0   ,0  ,0  ,67),--Cayman Islands
('20200327',53 ,null,1   ,0  ,0  ,67),--Cura�ao
('20200327',222,null,0   ,0  ,0  ,67),--US Virgin Islands
('20200327',11 ,null,0   ,0  ,0  ,67),--Aruba
('20200327',194,null,0   ,0  ,0  ,67),--Sint Maarten
('20200327',23 ,null,0   ,0  ,0  ,67),--Bermuda
('20200327',220,null,1   ,0  ,0  ,67),--Turks and Caicos
('20200327',7  ,null,2   ,0  ,0  ,67),--Anguilla
('20200327',29 ,null,2   ,0  ,0  ,67),--British Virgin Islands



('20200327',3  ,null,41  ,4  ,0  ,67),--Algeria
('20200327',189,null,6   ,0  ,0  ,67),--Senegal 
('20200327',155,null,19  ,0  ,0  ,67),--Nigeria
('20200327',36, null,5   ,0  ,0  ,67),--Cameroon
('20200327',199,null,218 ,0  ,0  ,67),--South Africa
('20200327',213,null,1   ,0  ,0  ,67),--Togo
('20200327',32 ,null,0   ,0  ,0  ,67),--Burkina Faso
('20200327',63 ,null,3   ,1  ,0  ,67),--DRC Congo
('20200327',56 ,null,0   ,0  ,0  ,67),--Cote Divore
('20200327',71 ,null,0   ,0  ,0  ,67),--Ethiopia
('20200327',79 ,null,0   ,0  ,0  ,67),--Gabon
('20200327',83 ,null,64  ,1  ,0  ,67),--Ghana
('20200327',91 ,null,1   ,0  ,0  ,67),--Guinea
('20200327',113,null,0   ,0  ,0  ,67),--Kenya
('20200327',147,null,3   ,0  ,0  ,67),--Namibia
('20200327',41 ,null,0   ,0  ,0  ,67),--Central African Republic
('20200327',48 ,null,0   ,0  ,0  ,67),--Congo
('20200327',67 ,null,0   ,0  ,0  ,67),--Equatorial Guinea
('20200327',70 ,null,2   ,0  ,0  ,67),--Eswatini
('20200327',134,null,1   ,0  ,0  ,67),--Mauritania
('20200327',176,null,9   ,0  ,0  ,67),--Rwanda
('20200327',191,null,0   ,0  ,0  ,67),--Seychelles
('20200327',22 ,null,1   ,0  ,0  ,67),--Benin
('20200327',121,null,0   ,0  ,0  ,67),--Liberia
('20200327',210,null,0   ,0  ,0  ,67),--Tanzania
('20200327',135,null,34  ,0  ,0  ,67),--Mauritius
('20200327',237,null,0   ,0  ,0  ,67),--Zambia
('20200327',80 ,null,0   ,0  ,0  ,67),--Gambia
('20200327',42 ,null,2   ,0  ,0  ,67),--Chad
('20200327',154,null,8   ,1  ,0  ,67),--Niger
('20200327',34 ,null,0   ,0  ,0  ,67),--Cabo Verde
('20200327',238,null,1   ,0  ,0  ,67),--Zimbabwe
('20200327',126,null,5   ,0  ,0  ,67),--Madagascar
('20200327',6  ,null,0   ,0  ,0  ,67),--Angola
('20200327',68 ,null,2   ,0  ,0  ,67),--Eritrea
('20200327',224,null,0   ,0  ,0  ,67),--Uganda
('20200327',145,null,0   ,0  ,0  ,67),--Mozambique
('20200327',92 ,null,0   ,0  ,0  ,67),--Guinea-bissau
('20200327',130,null,0   ,0  ,0  ,67),--Mali

('20200327',136,null,15  ,0  ,0  ,67),--Mayotte
('20200327',177,null,41  ,0  ,0  ,67),--Reunion

('20200327',58 ,null,0   ,0  ,0  ,67)--Diamond Princess
